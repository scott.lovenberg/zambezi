/* ========================================================================== **
 *                               smb2_treeconn.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: SMB2/SMB3 Tree Connect and Disconnect
 *
 * Copyright (C) 2021 by Christopher R. Hertel
 * $Id: smb2_treeconn.c; 2021-03-10 17:04:48 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>         /* For debugging; see assert(3).            */

#include "smb_endian.h"     /* Convert between host and SMB byte order. */
#include "smb2_header.h"    /* SMB2 Header header.                      */
#include "smb2_treeconn.h"  /* Module Header. */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *
 *  TC_PATH_OFFSET      - The default offset, from the start of the SMB2
 *                        header, at which the Share Path will normally be
 *                        found.  In dialects prior to 3.1.1, the Share Path
 *                        should always be at the beginning of the Buffer
 *                        field.
 *  TC_PATH_OFFSET_EXT  - In SMBv3.1.1 and above, there *may* be a Context
 *                        Header structure at the start of the Buffer.  This
 *                        will be the case IFF the
 *                        SMB2_TREE_CONNECT_FLAG_EXTENSION_PRESENT flag is
 *                        set in the Flags field.  The Context Header
 *                        consumes an additional 16 bytes at the start of
 *                        the Buffer.  [MS-SMB2; 2.2.9.1]
 */

#define TC_PATH_OFFSET      (SMB2_HEADER_SIZE + SMB2_TREECONN_REQ_SIZE)
#define TC_PATH_OFFSET_EXT  (TC_PATH_OFFSET + 16)


/* -------------------------------------------------------------------------- **
 * Static Functions
 */

static inline void filterTCResp( uint16_t           const dialect,
                                 smb2_TreeConnResp *const tConnResp )
  /* ------------------------------------------------------------------------ **
   * Bit filter for three flags fields in the Tree Connect Response message.
   *
   *  Input:  dialect   - The dialect Id to use when filtering the flags.
   *          tConnResp - A pointer to an smb2_TreeConnResp structure.
   *
   *  Output: <none>
   *
   *  Notes:  This function is used by smb2_parseTreeConnResp() and
   *          smb2_packTreeConnResp().  It performs bit filtering based on
   *          the given dialect, disabling (clearing) any flag bits that
   *          not defined for that dialect.
   *
   *          If the given dialect is SMB2_VNONE, SMB2_VWILD, or any
   *          unrecognized dialect, then no filtering is performed and the
   *          contents of <tConnResp> are returned unaltered.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  static const
  uint32_t capMask = ~(uint32_t)(SMB2_SHARE_CAP_CONTINUOUS_AVAILABILITY
                               | SMB2_SHARE_CAP_SCALEOUT
                               | SMB2_SHARE_CAP_CLUSTER
                               | SMB2_SHARE_CAP_ASYMMETRIC
                               | SMB2_SHARE_CAP_REDIRECT_TO_OWNER);
  switch( dialect )
    {
    case SMB2_V0202:
      /* Discard ShareFlags that are invalid for 2.0.2. */
      tConnResp->ShareFlags   &= ~(uint32_t)(SMB2_SHAREFLAG_ENABLE_HASH_V1
                                           | SMB2_SHAREFLAG_ENABLE_HASH_V2
                                           | SMB2_SHAREFLAG_ENCRYPT_DATA
                                           | SMB2_SHAREFLAG_IDENTITY_REMOTING);
      tConnResp->Capabilities &= capMask;
      break;
    case SMB2_V0210:
      /* Discard ShareFlags that are invalid for 2.1.0. */
      tConnResp->ShareFlags   &= ~(uint32_t)(SMB2_SHAREFLAG_ENABLE_HASH_V2
                                           | SMB2_SHAREFLAG_ENCRYPT_DATA
                                           | SMB2_SHAREFLAG_IDENTITY_REMOTING);
      tConnResp->Capabilities &= capMask;
      break;
    case SMB2_V0300:
      /* Discard SareFlags that are invalid for 3.0.0.
       * Remoting was added in 3.1.1.
       */
      tConnResp->ShareFlags   &= ~(uint32_t)SMB2_SHAREFLAG_IDENTITY_REMOTING;
      /* CAP_ASYMMETRIC was not introduced until 3.0.2. */
      tConnResp->Capabilities &= ~(uint32_t)(SMB2_SHARE_CAP_ASYMMETRIC
                                           | SMB2_SHARE_CAP_REDIRECT_TO_OWNER);
      break;
    case SMB2_V0302:
      /* Remoting was added in 3.1.1. */
      tConnResp->ShareFlags   &= ~(uint32_t)SMB2_SHAREFLAG_IDENTITY_REMOTING;
      /* CAP_REDIRECT_TO_OWNER was not introduced until 3.1.1. */
      tConnResp->Capabilities &= ~(uint32_t)SMB2_SHARE_CAP_REDIRECT_TO_OWNER;
      break;
    case SMB2_V0311:
      /* No exclusions; just apply the known flag bitmasks (below). */
      break;
    default:
      /* Apply *no* filters if an unknown or unspecific dialect was given.  */
      return;
    }

  /* Apply defined flag bitmasks. */
  tConnResp->ShareType    &= SMB2_SHARE_TYPE_MASK;  /* There are only 3 types.*/
  tConnResp->ShareFlags   &= SMB2_SHAREFLAG_MASK;   /* Mask out unknown flags.*/
  tConnResp->Capabilities &= SMB2_SHARE_CAP_MASK;   /* Mask out unknown caps. */
  return;
  } /* filterTCResp */


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb2_parseTreeConnReq( uint16_t          const dialect,
                           uint8_t          *const msg,
                           size_t            const msgLen,
                           smb2_TreeConnReq *const tConnReq )
  /** Parse an SMB2 Tree Connect Request message.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        The 3.1.1 dialect introduces a \c Flags field.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Tree Connect Request
   *                        message, at least \c #SMB2_TREECONN_REQ_SIZE
   *                        bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  tConnReq  A (constant) pointer to an \c #smb2_TreeConnReq
   *                        structure into which the request message will be
   *                        parsed.
   *
   * @returns   On success, a positive value indicating the number of bytes
   *            of \p msg that have been parsed.  This is the size of the
   *            fixed portion of the SMB2 Tree Connect Request message (8).\n
   *            On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value (9).
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of an SMB2/3
   *                  Tree Connect Request.  A minimum of
   *                  \c #SMB2_TREECONN_REQ_SIZE bytes are required.
   *
   * \b Notes
   *  - Regarding \p msgLen:
   *    + If \p msgLen is less than \c #SMB2_TREECONN_REQ_SIZE, an error is
   *      returned and the contents of \p tConnReq are unchanged.
   *    + If \p msgLen is equal to \c #SMB2_TREECONN_REQ_SIZE, then
   *      \p tConnReq->Buffer will be NULL.
   *    + If \p msgLen is greater than the size of the fixed portion of the
   *      SMB2 Tree Connect message body, then \p tConnReq->Buffer will point
   *      to the byte immediately following the fixed portion of the message:
   *      \code &msg[SMB2_TREECONN_REQ_SIZE]; \endcode
   *  - The location of the share path is given by \p tConnReq->PathOffset,
   *    but this value is relative to the start of the packet header.  The
   *    path offset relative to \p tConnReq->Buffer is given by:
   *    \code
   *    tConnReq->PathOffset - (SMB2_HEADER_SIZE + SMB2_TREECONN_REQ_SIZE);
   *    \endcode
   *  - In dialects prior to SMBv3.1.1, the share path would typically be at
   *    the start of the \c Buffer field (the \c Buffer containing no other
   *    data).  In SMBv3.1.1 and above, the \p tConnReq->Buffer may contain
   *    additional fields, as specified in [MS-SMB2; 2.2.9.1].  The share
   *    path will then start 16 bytes beyond the start of the Buffer.  Even
   *    though these offsets are known and (probably) consistent, it is best
   *    to use (or, at least, verify) the \c PathOffset value.
   *    @see <a
   *      href="@msdocs/ms-smb2/9ca7328b-b6ca-41a7-9773-0fa237261b76">[MS-SMB2;
   *      2.2.9.1]: SMB2 TREE_CONNECT Request Extension</a>
   *  - For dialects prior to SMBv3.1.1, the \c Reserved field is copied
   *    verbatim into the \p tConnReq->Reserved field.  For 3.1.1 and above,
   *    the value is read as a 16-bit unsigned integer in SMB order.  No
   *    filters are applied to the \c Flags.  Undefined bits \e should be
   *    ignored, as usual.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != tConnReq );
  /* Check buffer size. */
  if( msgLen < SMB2_TREECONN_REQ_SIZE )
    return( -SMB2_TREECONN_REQ_SIZE );
  /* Test StructureSize. */
  tConnReq->StructureSize = smbget16( msg );
  if( tConnReq->StructureSize != SMB2_TREECONN_REQ_STRUCT_SIZE )
    return( -1 );

  /* Extract the Flags/Reserved field. */
  if( dialect < SMB2_V0311 )
    (void)memcpy( tConnReq->Reserved, &msg[2], 2 );
  else
    tConnReq->Flags = smbget16( &msg[2] );

  tConnReq->PathOffset = smbget16( &msg[4] );
  tConnReq->PathLength = smbget16( &msg[6] );
  tConnReq->Buffer     = NULL;
  if( msgLen > SMB2_TREECONN_REQ_SIZE )
    tConnReq->Buffer  = &msg[SMB2_TREECONN_REQ_SIZE];
  return( SMB2_TREECONN_REQ_SIZE );
  } /* smb2_parseTreeConnReq */

int smb2_packTreeConnReq( uint16_t          const dialect,
                          smb2_TreeConnReq *const tConnReq,
                          uint8_t          *const bufr,
                          uint32_t          const bSize )
  /** Pack an SMB2 Tree Connect Request Message.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        The \c #smb2_TreeConnReq::Flags field was
   *                        introduced with SMBv3.1.1.
   * @param[in]   tConnReq  A pointer to an \c #smb2_TreeConnReq structure
   *                        containing the values to be used to compose the
   *                        wire format message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns
   *  The number of bytes of \p bufr that were used to store the message.
   *
   * \b Notes
   *  - If \c #smb2_TreeConnReq::PathOffset is given as zero, a default value
   *    will be used.
   *    + If the dialect is pre-3.1.1, or if the dialect is 3.1.1 or above
   *      and the \c #SMB2_TREE_CONNECT_FLAG_EXTENSION_PRESENT flag is
   *      \em not set, then the default PathOffset is:
   *      \code (SMB2_HEADER_SIZE + SMB2_TREECONN_REQ_SIZE) \endcode
   *      \b See: <a
   *      href="@msdocs/ms-smb2/832d2130-22e8-4afb-aafd-b30bb0901798">[MS-SMB2;
   *      2.2.9]: SMB2 TREE_CONNECT Request</a>
   *    + If the dialect is 3.1.1 or above and the
   *      \c #SMB2_TREE_CONNECT_FLAG_EXTENSION_PRESENT flag \em is set,
   *      then the default PathOffset is:
   *      \code (16 + SMB2_HEADER_SIZE + SMB2_TREECONN_REQ_SIZE) \endcode
   *      \b See: <a
   *      href="@msdocs/ms-smb2/9ca7328b-b6ca-41a7-9773-0fa237261b76">[MS-SMB2;
   *      2.2.9.1]: SMB2 TREE_CONNECT Request Extension</a>
   *  - No filters are applied to the \c #smb2_TreeConnReq->Flags field when
   *    composing the outgoing message.
   */
  {
  uint8_t *p            = bufr;
  uint16_t pOff_default = TC_PATH_OFFSET;

  /* Sanity chequers. */
  assert( NULL != bufr );
  assert( SMB2_TREECONN_REQ_SIZE <= bSize );

  /* Packit. */
  p = smbset16( p, tConnReq->StructureSize );
  if( dialect < SMB2_V0311 )
    p = smbcpmem( p, tConnReq->Reserved, 2 ); /* Raw copy Reserved bytes.     */
  else
    {
    p = smbset16( p, tConnReq->Flags );       /* Flag bytes in smb order.     */
    if( SMB2_TREE_CONNECT_FLAG_EXTENSION_PRESENT & tConnReq->Flags )
      pOff_default = TC_PATH_OFFSET_EXT;      /* Change default path offset.  */
    }
  p = smbset16( p, tConnReq->PathOffset ? tConnReq->PathOffset : pOff_default );
  p = smbset16( p, tConnReq->PathLength );

  return( SMB2_TREECONN_REQ_SIZE );
  } /* smb2_packTreeConnReq */

int smb2_parseTreeConnResp( uint16_t           const dialect,
                            uint8_t           *const msg,
                            size_t             const msgLen,
                            smb2_TreeConnResp *const tConnResp )
  /** Parse an SMB2 Tree Connect Response message.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        Bitmasks may be applied to the numerous flags
   *                        fields in this message.  See the notes, below.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Tree Connect Response
   *                        message, at least \c #SMB2_TREECONN_RESP_SIZE
   *                        bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  tConnResp A (constant) pointer to an \c #smb2_TreeConnResp
   *                        structure into which the request message will be
   *                        parsed.
   *
   * @returns   On success, a positive value indicating the number of bytes
   *            of \p msg that have been parsed.  This is the size of the
   *            fixed portion of the SMB2 Tree Connect Response message (16).
   *            The Tree Connect Response message does not include a dynamic
   *            portion.
   *            \n On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value (16).
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of an SMB2/3
   *                  Tree Connect Response.  A minimum of
   *                  \c #SMB2_TREECONN_RESP_SIZE bytes are required.
   *
   * \b Notes
   *  - There are three flags fields in the Tree Connect Response:
   *    + \c #smb2_TreeConnResp::ShareType
   *    + \c #smb2_TreeConnResp::ShareFlags
   *    + \c #smb2_TreeConnResp::Capabilities
   *    .
   *    The latter two of these each include flags introduced with different
   *    dialect revisions.
   *    @see \c #SMB2_tcShareFlags
   *    @see \c #SMB2_tcShareCaps
   *  - This function will attempt to filter the flags fields so that only
   *    flags that are valid for the given \p dialect are returned.  The
   *    rules are applied as follows:
   *    + If the given dialect is \c #SMB2_VNONE, \c #SMB2_VWILD, or any
   *      dialect identifier value that is unrecognized by this module,
   *      then \e no bitmask filters are applied and the flags fields are
   *      all returned with wire values (in host byte order).
   *    + Otherwise, only the bits defined for the given \p dialect are
   *      returned.  All others will be cleared.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != tConnResp );
  /* Check buffer size. */
  if( msgLen < SMB2_TREECONN_RESP_SIZE )
    return( -SMB2_TREECONN_RESP_SIZE );
  /* Test StructureSize. */
  tConnResp->StructureSize = smbget16( msg );
  if( tConnResp->StructureSize != SMB2_TREECONN_RESP_STRUCT_SIZE )
    return( -1 );

  /* Parse the rest. */
  tConnResp->ShareType      = msg[2];
  tConnResp->Reserved       = msg[3];
  tConnResp->ShareFlags     = smbget32( &msg[4] );
  tConnResp->Capabilities   = smbget32( &msg[8] );
  tConnResp->MaximalAccess  = smbget32( &msg[12] );

  /* Filter flags fields, then done. */
  filterTCResp( dialect, tConnResp );
  return( SMB2_TREECONN_RESP_SIZE );
  } /* smb2_parseTreeConnResp */

int smb2_packTreeConnResp( uint16_t           const dialect,
                           smb2_TreeConnResp *const tConnResp,
                           uint8_t           *const bufr,
                           uint32_t           const bSize )
  /** Compose an SMB2 Tree Connect Response message.
   *
   * @param[in]   dialect   The dialect used when composing the message.
   *                        Bitmasks may be applied to the numerous flags
   *                        fields in this message.  See the notes, below.
   * @param[out]  tConnResp a pointer to an \c #smb2_TreeConnResp structure
   *                        containing the values to be used to compose the
   *                        wire format message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   On success, the number of bytes of \p bufr that were used to
   *            store the wire form message.  This will always be
   *            \c #SMB2_TREECONN_RESP_SIZE (16).
   *
   * \b Notes
   *  - As with \c smb2_parseTreeConnResp(), this function may apply a
   *    set of filters to the flags fields provided via \p tConnResp.
   *    + If \p dialect is is \c #SMB2_VNONE, \c #SMB2_VWILD, or any
   *      dialect value that is unrecognized by this module, the filters
   *      will be skipped.  Otherwise, the filter will unset any bits that
   *      are not valid for the given dialect.
   */
  {
  uint8_t *p = bufr;

  /* Sanity chex. */
  assert( NULL != bufr );
  assert( SMB2_TREECONN_RESP_STRUCT_SIZE <= bSize );

  /* Apply flag filters. */
  filterTCResp( dialect, tConnResp );

  /* Pack 'em up. */
  p = smbset16( p, tConnResp->StructureSize );
  p = smbset8(  p, tConnResp->ShareType );
  p = smbset8(  p, tConnResp->Reserved  );
  p = smbset32( p, tConnResp->ShareFlags );
  p = smbset32( p, tConnResp->Capabilities );
  p = smbset32( p, tConnResp->MaximalAccess );
  return( SMB2_TREECONN_RESP_SIZE );
  } /* smb2_packTreeConnResp */

int smb2_parseTreeDiscReq( uint16_t          const dialect,
                           uint8_t          *const msg,
                           size_t            const msgLen,
                           smb2_TreeDiscReq *const tDiscReq )
  /** Parse an SMB2 Tree Disconnect Request message.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Tree Disconnect Request
   *                        message, at least 4 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  tDiscReq  A (constant) pointer to an #smb2_TreeDiscReq
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 4.
   *
   * @par Errors
   *  \b See \c smb2_parseBaseMsg()
   *
   * \b Notes
   *  - This function is a wrapper for \c smb2_parseBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_parseBaseMsg( dialect, msg, msgLen, tDiscReq ) );
  } /* smb2_parseTreeDiscReq */

int smb2_packTreeDiscReq( uint16_t          const dialect,
                          smb2_TreeDiscReq *const tDiscReq,
                          uint8_t          *const bufr,
                          uint32_t          const bSize )
  /** Pack an SMB2 Tree Disconnect Request message.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   tDiscReq  Either NULL, in which case appropriate default
   *                        values are used, or a pointer to an
   *                        \c #smb2_TreeDiscReq structure containing the
   *                        values to be used to compose the wire format
   *                        message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 4).
   *
   * \b Notes
   *  - This function is a wrapper for \c smb2_packBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *  - Passing NULL for \p tDiscReq is recommended.  The body of the
   *    Flush Request message is pre-defined.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_packBaseMsg( dialect, tDiscReq, bufr, bSize ) );
  } /* smb2_packTreeDiscReq */

int smb2_parseTreeDiscResp( uint16_t           const dialect,
                            uint8_t           *const msg,
                            size_t             const msgLen,
                            smb2_TreeDiscResp *const tDiscResp )
  /** Parse an SMB2 Tree Disconnect Response message.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Tree Disconnect Response
   *                        message, at least 4 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  tDiscResp A (constant) pointer to an #smb2_TreeDiscResp
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 4.
   *
   * @par Errors
   *  \b See \c smb2_parseBaseMsg()
   *
   * \b Notes
   *  - This function is a wrapper for \c smb2_parseBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_parseBaseMsg( dialect, msg, msgLen, tDiscResp ) );
  } /* smb2_parseTreeDiscResp */

int smb2_packTreeDiscResp( uint16_t           const dialect,
                           smb2_TreeDiscResp *const tDiscResp,
                           uint8_t           *const bufr,
                           uint32_t           const bSize )
  /** Pack an SMB2 Tree Disconnect Response message.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   tDiscResp Either NULL, in which case appropriate default
   *                        values are used, or a pointer to an
   *                        \c #smb2_TreeDiscResp structure containing the
   *                        values to be used to compose the wire format
   *                        message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 4).
   *
   * \b Notes
   *  - This function is a wrapper for \c smb2_packBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *  - Passing NULL for \p tDiscResp is recommended.  The body of the
   *    Flush Request message is pre-defined.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_packBaseMsg( dialect, tDiscResp, bufr, bSize ) );
  } /* smb2_packTreeDiscResp */


/* =============================== Gesundheit =============================== */
