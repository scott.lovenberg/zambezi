#ifndef SMB_ENDIAN_H
#define SMB_ENDIAN_H
/* ========================================================================== **
 *                                smb_endian.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Conversion to and from SMB (Little Endian) byte order.
 *
 * Copyright (C) 2019 by Christopher R. Hertel
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 *  Some platforms provide similar tools for marshalling and unmarshalling
 *  integer values.  For example, see byteorder(9) on BSD platforms.
 *  Unfortunately, the various implementations are neither consistent nor
 *  universal.  We are best served by writhing [sic.] our own code.
 *
 * ========================================================================== **
 *//**
 * @file      smb_endian.h
 * @author    Christopher R. Hertel
 * @date      9 Apr 2019
 * @version   \$Id: smb_endian.h; 2020-11-26 17:13:52 -0600; crh$
 * @copyright Copyright (C) 2019 by Christopher R. Hertel
 *
 * @brief     Static inline functions for [un]marshalling of integers.
 * @see       On BSD, see \c byteorder(9).  On Linux, see \c endian(3).
 * @see       In the Samba source tree, see \c samba/lib/util/byteorder.h.
 *
 * @details   SMB, with one awkward exception, uses little endian byte
 *            order on the wire.  This module provides inline functions
 *            that move integer values in host byte order to and from
 *            byte arrays.  Those byte arrays will likely be the
 *            input/output buffers used to send and receive SMB messages.
 *
 * @attention This header file uses a simple trick borrowed from some of
 *            the referenced example code.  The functions are defined (not
 *            just declared) in the header.  They are defined as both
 *            \c static and \c inline, so they work sort of like macros.
 *
 * \b Notes
 *  - SMB byte order is little endian, which means that the lowest-order
 *    byte (the little end) is stored first in memory.  So, for example,
 *    \code (uint32_t)0x01020304 \endcode is stored as
 *    \code [0x04, 0x03, 0x02, 0x01] \endcode
 *  - The one awkward exception is the message framing header, which is in
 *    network byte order (big endian).  The framing header is used in both
 *    NBT and naked TCP transport.
 */

#include <stdint.h>     /* Standard integer types.  */
#include <string.h>     /* For memcpy(3), et al.    */


/* -------------------------------------------------------------------------- **
 * Doxygen Fudge
 *
 *  The following ifdef block forces Doxygen to produce documentation
 *  for the static functions defined in this header file, even though
 *  EXTRACT_STATIC is set to NO in the Doxyfile.  Note, in particular,
 *  that these declarations are neither static nor inline, which is
 *  incorrect.  Doxygen don't care.
 */

#ifdef DOXY_TEXT
uint16_t smbget16( const uint8_t *const ptr );
uint32_t smbget32( const uint8_t *const ptr );
uint64_t smbget64( const uint8_t *const ptr );
uint16_t nbtget16( const uint8_t *const ptr );
uint32_t nbtget32( const uint8_t *const ptr );
uint8_t *smbset8( uint8_t *ptr, const uint8_t val );
uint8_t *smbset16( uint8_t *ptr, const uint16_t val );
uint8_t *smbset32( uint8_t *ptr, const uint32_t val );
uint8_t *smbset64( uint8_t *ptr, const uint64_t val );
int smbget16array( uint16_t       *const dst,
                   uint16_t const *const src,
                   uint16_t const        count );
int smbset16array( uint16_t       *const dst,
                   uint16_t const *const src,
                   uint16_t const        count );
uint8_t *nbtset16( uint8_t *ptr, const uint16_t val );
uint8_t *nbtset32( uint8_t *ptr, const uint32_t val );
uint8_t *smbcpmem( void *dst, const void *src, size_t len );
uint8_t *smbmset( void *dst, const uint8_t c, size_t len );
#endif /* DOXY_TEXT */


/* -------------------------------------------------------------------------- **
 * Macros:
 */

/**
 * @def     smbzero( P, N )
 * @param   S   Pointer to the memory area to be zeroed.
 * @param   N   The number of bytes at \p S to be zeroed.
 * @brief   Zero a memory area of \p N bytes.
 * @returns \c void
 * @details
 *  The \c bzero(3) function is simple, useful, and deprecated.\n
 *  This macro is derived from a recommendation given in older editions
 *  of the POSIX manual for a portable replacement for \c bzero(3).
 *
 * @see <a href=
 *  "https://pubs.opengroup.org/onlinepubs/009695399/functions/bzero.html">
 *  POSIX.1-2004:bzero(3)</a>
 */
#define smbzero( S, N ) ( memset( (S), 0x00, (N) ), (void)0 )


/* -------------------------------------------------------------------------- **
 * Static inline functions:
 */

/* Read integer values in SMB byte order from an input buffer.
 */

static inline uint16_t smbget16( const uint8_t *const ptr )
  /** Read a two-byte unsigned integer value from a buffer.
   *
   * @param[in] ptr   A pointer to an array of (at least) two bytes.
   *                  These two bytes will be read as a 16-bit unsigned
   *                  integer in SMB byte order.
   *
   * @returns   An unsigned 16-bit integer.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  return( (uint16_t)((ptr[1] << 8) | ptr[0]) );
  } /* smbget16 */

static inline uint32_t smbget32( const uint8_t *const ptr )
  /** Read a four-byte unsigned integer value from a buffer.
   *
   * @param[in] ptr   A pointer to an array of (at least) four bytes.
   *                  The four bytes will be read as a 32-bit unsigned
   *                  integer in SMB byte order.
   *
   * @returns   An unsigned 32-bit integer.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  return( (uint32_t)(ptr[3] << 24 | ptr[2] << 16 | ptr[1] << 8 | ptr[0]) );
  } /* smbget32 */

static inline uint64_t smbget64( const uint8_t *const ptr )
  /** Read an eight-byte unsigned integer value from a buffer.
   *
   * @param[in] ptr   A pointer to an array of (at least) eight bytes.
   *                  The eight bytes will be read as a 64-bit unsigned
   *                  integer in SMB byte order.
   *
   * @returns   An unsigned 64-bit integer.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  register uint64_t tmp;

  tmp = ptr[7];
  tmp = (tmp << 8) | ptr[6];
  tmp = (tmp << 8) | ptr[5];
  tmp = (tmp << 8) | ptr[4];
  tmp = (tmp << 8) | ptr[3];
  tmp = (tmp << 8) | ptr[2];
  tmp = (tmp << 8) | ptr[1];
  tmp = (tmp << 8) | ptr[0];
  return( tmp );
  } /* smbget64 */

/* Write integer values to an output buffer in SMB byte order.
 */

static inline uint8_t *smbset8( uint8_t *ptr, const uint8_t val )
  /** Write a one-byte unsigned integer to an array of bytes.
   *
   * @param[in,out] ptr Pointer to an array of at least one byte.
   * @param[in]     val An unsigned 8-bit integer value, to be copied
   *                    to the location indicated by \p ptr.
   *
   * @returns   As a convenience, this function returns a pointer to the
   *            location in \p ptr immediately following the byte that
   *            was written.  The result probably points to the location
   *            at which the next value will be written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   *  - This function was created for visual consistency.  Fundamentally,
   *    it just copies one byte to another location.  However, it does also
   *    increment the pointer for you, and it has the same interface as the
   *    other \c smbset*() functions.  It looks nice.
   */
  {
  ptr[0] = val;
  return( ptr + 1 );
  } /* smbset8 */

static inline uint8_t *smbset16( uint8_t *ptr, const uint16_t val )
  /** Write a 16-bit unsigned integer as a string of bytes in SMB byte order.
   *
   * @param[in,out] ptr   Pointer to an array of (at least) two bytes.
   * @param[in]     val   An unsigned 16-bit integer value.  This value
   *                      will be written in SMB byte order to the location
   *                      indicated by \p ptr.
   *
   * @returns   A pointer to the location in \p ptr immediately following
   *            the two bytes written.  This is a convenience; the returned
   *            pointer is (probably) the location at which the next value
   *            will be written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  ptr[0] = (uint8_t)( val       & 0xFF);
  ptr[1] = (uint8_t)((val >> 8) & 0xFF);
  return( ptr + 2 );
  } /* smbset16 */

static inline uint8_t *smbset32( uint8_t *ptr, const uint32_t val )
  /** Write a 32-bit unsigned integer as a string of bytes in SMB byte order.
   *
   * @param[in,out] ptr   Pointer to an array of (at least) four bytes.
   * @param[in]     val   An unsigned 32-bit integer value.  This value will
   *                      be written in SMB byte order to the location
   *                      indicated by \p ptr.
   *
   * @returns   A pointer to the location in \p ptr immediately following
   *            the four bytes written.  This is a convenience; the returned
   *            pointer is (probably) the location at which the next value
   *            will be written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  ptr[0] = (uint8_t)( val        & 0xFF);
  ptr[1] = (uint8_t)((val >>  8) & 0xFF);
  ptr[2] = (uint8_t)((val >> 16) & 0xFF);
  ptr[3] = (uint8_t)((val >> 24) & 0xFF);
  return( ptr + 4 );
  } /* smbset32 */

static inline uint8_t *smbset64( uint8_t *ptr, const uint64_t val )
  /** Write a 64-bit unsigned integer as a string of bytes in SMB byte order.
   *
   * @param[in,out] ptr   Pointer to an array of (at least) eight bytes.
   * @param[in]     val   An unsigned 64-bit integer value.  This value will
   *                      be written in SMB byte order to the location
   *                      indicated by \p ptr.
   *
   * @returns   A pointer to the location in \p ptr immediately following
   *            the eight bytes written.  This is a convenience; the
   *            returned pointer is (probably) the location at which the
   *            next value will be written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  ptr[0] = (uint8_t)( val        & 0xFF);
  ptr[1] = (uint8_t)((val >>  8) & 0xFF);
  ptr[2] = (uint8_t)((val >> 16) & 0xFF);
  ptr[3] = (uint8_t)((val >> 24) & 0xFF);
  ptr[4] = (uint8_t)((val >> 32) & 0xFF);
  ptr[5] = (uint8_t)((val >> 40) & 0xFF);
  ptr[6] = (uint8_t)((val >> 48) & 0xFF);
  ptr[7] = (uint8_t)((val >> 56) & 0xFF);
  return( ptr + 8 );
  } /* smbset64 */

/* Get and set arrays of SMB-ordered values.
 */

static inline int smbget16array( uint16_t       *const dst,
                                 uint16_t const *const src,
                                 uint16_t const        count )
  /** Convert an array of 16 bit values from SMB to host byte order.
   *
   * @param[out]  dst   A pointer to an array that will receive the
   *                    translated values.
   * @param[in]   src   A constant pointer to the array of values to be read.
   * @param[in]   count The number of entries in the \p src array.
   *
   * @returns   The size of the input array, in bytes.  The output array is,
   *            of course, the same size.
   *
   * \b Notes
   *  - The \p src and \p dst pointers may safely point to the same location,
   *    in which case the host byte order values will overwrite the wire
   *    format values.
   *  - Both \p src and \p dst must point to arrays with at least \p count
   *    entries.
   */
  {
  register int i;

  for( i = 0; i < count; i++ )
    {
    dst[i] = smbget16( (uint8_t *)(src + i) );
    }
  return( (int)(count * sizeof( uint16_t )) );
  } /* smbget16array */

static inline int smbset16array( uint16_t       *const dst,
                                 uint16_t const *const src,
                                 uint16_t const        count )
  /** Convert an array of 16 bit values from host to SMB byte order.
   *
   * @param[out]  dst   A pointer to an array that will receive the
   *                    translated values.
   * @param[in]   src   A constant pointer to the array of values to be read.
   * @param[in]   count The number of entries in the \p src array.
   *
   * @returns   The size of the input array, in bytes.  The output array is,
   *            of course, the same size.
   *
   * \b Notes
   *  - The \p src and \p dst pointers may safely point to the same location,
   *    in which case the wire format values will overwrite the host byte
   *    order values.
   *  - Both \p src and \p dst must point to arrays with at least \p count
   *    entries.
   */
  {
  register int i;

  for( i = 0; i < count; i++ )
    {
    (void)smbset16( (uint8_t *)(dst + i), src[i] );
    }
  return( (int)(count * sizeof( uint16_t )) );
  } /* smbset16array */

/* Read and write NBT integers, including the SMB framing header.
 */

static inline uint16_t nbtget16( const uint8_t *const ptr )
  /** Read a 16-bit unsigned integer from a buffer in network byte order.
   *
   * @param[in] ptr A pointer to an array of (at least) two bytes.
   *                These two bytes will be read as a 16-bit unsigned
   *                integer in network byte order.
   *
   * @returns   An unsigned 16-bit integer in host byte order.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   *  - In NBT, integers are transferred in network byte order.
   */
  {
  return( (uint16_t)(ptr[0] << 8 | ptr[1]) );
  } /* nbtget16 */

static inline uint32_t nbtget32( const uint8_t *const ptr )
  /** Read a 32-bit unsigned integer from a buffer in network byte order.
   *
   * @param[in] ptr A pointer to an array of (at least) four bytes.
   *                The four bytes will be read as a 32-bit unsigned
   *                integer in network byte order.
   *
   * @returns   An unsigned 32-bit integer in host byte order.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   *  - In NBT, integers are transferred in network byte order.
   *  - In SMB, only the framing header is sent in network byte order
   *    (big endian order).  This is a hold-over from the NBT Session
   *    Service, defined in RFC1001/1002.
   */
  {
  return( (uint32_t)(ptr[0] << 24 | ptr[1] << 16 | ptr[2] << 8 | ptr[3]) );
  } /* nbtget32 */

static inline uint8_t *nbtset16( uint8_t *ptr, const uint16_t val )
  /** Write a 16-bit unsigned integer to a buffer in network byte order.
   *
   * @param[in,out] ptr A pointer to an array of (at least) two bytes,
   *                    into which the integer value will be written.
   * @param[in]     val An integer value, in host byte order, to be
   *                    written to \p ptr in network byte order.
   *
   * @returns   A pointer to the location in \p ptr immediately following
   *            the two bytes that were written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  ptr[0] = (uint8_t)((val >>  8) & 0xFF);
  ptr[1] = (uint8_t)( val        & 0xFF);
  return( ptr + 2 );
  } /* nbtset16 */

static inline uint8_t *nbtset32( uint8_t *ptr, const uint32_t val )
  /** Write a 32-bit unsigned integer to a buffer in network byte order.
   *
   * @param[in,out] ptr A pointer to an array of (at least) four bytes,
   *                    into which the integer value will be written.
   * @param[in]     val An integer value, in host byte order, to be
   *                    written to \p ptr in network byte order.
   *
   * @returns   A pointer to the location in \p ptr immediately following
   *            the four bytes that were written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  ptr[0] = (uint8_t)((val >> 24) & 0xFF);
  ptr[1] = (uint8_t)((val >> 16) & 0xFF);
  ptr[2] = (uint8_t)((val >>  8) & 0xFF);
  ptr[3] = (uint8_t)( val        & 0xFF);
  return( ptr + 4 );
  } /* nbtset32 */

/* Memory copy utility function.
 */

static inline uint8_t *smbcpmem( void *dst, const void *src, size_t len )
  /** Copy the contents of one memory area to another memory area.
   *
   * @param[in] dst A pointer indicating the destination buffer into which
   *                the copy will be made.
   * @param[in] src A pointer to the source buffer from which the copy will
   *                be made.
   * @param[in] len The number of bytes to copy.
   *
   * @returns   A pointer to the byte position immediately following the
   *            copied data.  That is, \p dst + \p len.
   *
   * \b Notes
   *  - The copy is performed using the \c memcpy(3) function.  Therefore,
   *    as with \c memcpy(3), the \p dst and \p src memory areas must not
   *    overlap.
   *  - This function is essentially the same as the GNU \c mempcpy(3)
   *    function, except that it returns a <tt>uint8_t *</tt> pointer.
   *    This function was written only because \c mempcpy(3) is not standard.
   *
   * @see <a href="@posix/functions/memcpy.html">memcpy(3)</a>
   * @see <a href=
   *   "https://www.gnu.org/software/gnulib/manual/html_node/mempcpy.html">GNU
   *   mempcpy(3)</a>
   */
  {
  return( (uint8_t *)memcpy( dst, src, len ) + len );
  } /* smbcpmem */

static inline uint8_t *smbmset( void *dst, const uint8_t c, size_t len )
  /** Set all bytes of the specified memory area to a given value.
   *
   * @param[in] dst A pointer indicating the destintation buffer.
   * @param[in] c   A byte value to be copied into the buffer.
   * @param[in] len The number of bytes of \p dst to fill.
   *
   * @returns   A pointer to the byte immediately following the last byte
   *            written.  That is, \p dst + \p len.
   *
   * \b Notes
   *  - This is just like \c memset(3), except that the pointer returned
   *    indicates the position following the last byte written.  This is
   *    convenient for writing a sequence of values into a buffer.
   */
  {
  return( (uint8_t *)memset( dst, c, len ) + len );
  } /* smbmset */


#ifdef SMB_ENDIAN_UNIT_TEST
/* -------------------------------------------------------------------------- **
 * Unit Tests
 * To run, create a .c file containing the following lines:
 *
 *      #define SMB_ENDIAN_UNIT_TEST
 *      #include "smb_endian.h"
 *
 * Then just compile and run.
 * -------------------------------------------------------------------------- **
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/* A vector of bytes from which we will read uints in little-endian order. */
const uint8_t testbufr[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 };
/* Comparison values. */
const uint8_t  test8  = 0x01;
const uint16_t test16 = 0x0201;
const uint32_t test32 = 0x04030201;
const uint64_t test64 = 0x0807060504030201;

int main( void )
  {
  uint64_t  tmp;
  uint8_t   scratch[8];

  /* 1-byte test. */
  assert( test8 == *testbufr );   /* Should be the first byte of <testbufr>.  */
  (void)smbset8( scratch, (uint8_t)0xA5 );
  (void)smbset8( scratch, test8 );
  assert( *scratch == test8 );

  /* 2-byte tests. */
  tmp = smbget16( testbufr );
  assert( test16 == tmp );
  (void)smbset16( scratch, (uint16_t)tmp );
  assert( *scratch == (uint8_t)(tmp & 0xFF) );
  (void)nbtset16( scratch, test16 );
  tmp = smbget16( scratch );
  assert( test16 != tmp );        /* Should be reversed byte order. */
  (void)smbset16( scratch, (uint16_t)tmp );
  tmp = nbtget16( scratch );
  assert( test16 == tmp );        /* Back to malnor. */

  /* 4-byte tests. */
  tmp = smbget32( testbufr );
  assert( test32 == tmp );
  (void)nbtset32( scratch, (uint32_t)tmp );
  tmp = smbget32( scratch );
  assert( test32 != tmp );        /* Should be reversed byte order. */
  (void)smbset32( scratch, (uint32_t)tmp );
  tmp = nbtget32( scratch );
  assert( test32 == tmp );        /* Back to norlam. */

  /* 8-byte tests. */
  tmp = smbget64( testbufr );
  assert( test64 == tmp );
  (void)smbset64( scratch, tmp );
  assert( 0 == memcmp( scratch, testbufr, 8 ) );

  /* Array conversion tests.  */
  assert( 8 == smbget16array( (uint16_t *)scratch, (uint16_t *)testbufr, 4 ) );
  assert( 8 == smbset16array( (uint16_t *)scratch, (uint16_t *)scratch, 4 ) );
  assert( 0 == memcmp( scratch, testbufr, 8 ) );

  /* Copy and zero memory tests. */
  assert( smbcpmem( scratch, testbufr, 8 ) == (scratch + 8) );
  assert( test64 == smbget64( scratch ) );
  smbzero( scratch, 8 );
  assert( 0 == smbget64( scratch ) );
  assert( &scratch[4] == smbmset( scratch, 0x0F, 4 ) );
  assert( 0x0F0F0F0F == smbget32( scratch ) );

  (void)fprintf( stderr, "Passed all unit tests.\n" );
  return( EXIT_SUCCESS );
  } /* main */
#endif /* SMB_ENDIAN_UNIT_TEST */

/* ================================= The End ================================ */
#endif /* SMB_ENDIAN_H */
