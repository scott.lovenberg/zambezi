/* ========================================================================== **
 *                               smb2_logoff.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Logoff exchanges.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_logoff.c; 2020-09-18 11:41:35 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include "smb2_logoff.h"  /* Module Header. */


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb2_parseLogoffReq( uint16_t        const dialect,
                         uint8_t        *const msg,
                         size_t          const msgLen,
                         smb2_LogoffReq *const logoffReq )
  /** Parse an SMB2 Logoff Request message.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Logoff Request message,
   *                        at least 4 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  logoffReq A (constant) pointer to an #smb2_LogoffReq
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 4.
   *
   * \b Errors
   *  - \b See #smb2_parseBaseMsg()
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_parseBaseMsg().
   *  - The \p dialect is ignored; there are no dialect-specific
   *    fields in this message type.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_parseBaseMsg( dialect, msg, msgLen, logoffReq ) );
  } /* smb2_parseLogoffReq */

int smb2_packLogoffReq( uint16_t        const dialect,
                        smb2_LogoffReq *const logoffReq,
                        uint8_t        *const bufr,
                        uint32_t        const bSize )
  /** Pack an SMB2 Logoff Request message.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   logoffReq Either NULL, in which case appropriate default
   *                        values are used, or a pointer to an
   *                        \c #smb2_LogoffReq structure containing the
   *                        values to be used to compose the wire format
   *                        message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 4).
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_packBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *  - Passing NULL for \p logoffReq is recommended.  The body of the
   *    Logoff Request message is pre-defined.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_packBaseMsg( dialect, logoffReq, bufr, bSize ) );
  } /* smb2_packLogoffReq */

int smb2_parseLogoffResp( uint16_t         const dialect,
                          uint8_t         *const msg,
                          size_t           const msgLen,
                          smb2_LogoffResp *const logoffResp )
  /** Parse an SMB2 Logoff Response message.
   *
   * @param[in]   dialect     The dialect used when parsing the message.
   *                          There are no dialect-specific variations to
   *                          this message type, so the \p dialect value is
   *                          ignored.  This parameter is included for
   *                          interface consistency.
   * @param[in]   msg         A (constant) pointer to an array of bytes,
   *                          which \b MUST contain an SMB Logoff Response
   *                          message, at least 4 bytes in length.
   * @param[in]   msgLen      The size, in bytes, of \p msg.
   * @param[out]  logoffResp  A (constant) pointer to an #smb2_LogoffResp
   *                          structure into which the message will be
   *                          parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 4.
   *
   * \b Errors
   *  - \b See #smb2_parseBaseMsg()
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_parseBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_parseBaseMsg( dialect, msg, msgLen, logoffResp ) );
  } /* smb2_parseLogoffResp */

int smb2_packLogoffResp( uint16_t         const dialect,
                         smb2_LogoffResp *const logoffResp,
                         uint8_t         *const bufr,
                         uint32_t         const bSize )
  /** Pack an SMB2 Logoff Response message.
   *
   * @param[in]   dialect     The dialect used when packing the message.
   *                          There are no dialect-specific variations to
   *                          this message type, so the \p dialect value is
   *                          ignored.  This parameter is included for
   *                          interface consistency.
   * @param[in]   logoffResp  Either NULL, in which case appropriate
   *                          default values are used, or a pointer to an
   *                          \c #smb2_LogoffResp structure containing the
   *                          values to be used to compose the wire format
   *                          message.
   * @param[out]  bufr        Pointer to a data buffer into which the wire
   *                          format of the base message will be written.
   * @param[in]   bSize       Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 4).
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_packBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *  - Passing NULL for \p logoffResp is recommended.  The body of the
   *    Logoff Response message is pre-defined.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_packBaseMsg( dialect, logoffResp, bufr, bSize ) );
  } /* smb2_packLogoffResp */


/* ================================== Pthl ================================== */
