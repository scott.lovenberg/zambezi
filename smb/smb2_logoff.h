#ifndef SMB2_LOGOFF_H
#define SMB2_LOGOFF_H
/* ========================================================================== **
 *                               smb2_logoff.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Logoff exchanges.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_logoff.h; 2020-09-22 22:14:05 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_logoff.h
 * @author  Christopher R. Hertel
 * @brief   Parse/pack logoff request and response messages.
 * @date    15 Sep 2020
 * @version \$Id: smb2_logoff.h; 2020-09-22 22:14:05 -0500; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * The logoff request/response pair is quite simple.  All of the important
 * data is contained in the message header, and the message body for both
 * the request and response is an #smb2_BaseMsg structure.  There is, in
 * fact, no reason to even parse the bodies of these messages upon receipt.
 */

#include "smb2_baseMsg.h"


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @typedef smb2_LogoffReq
 * @brief   SMB2/3 Logoff Request message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.
 * @see     #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_LogoffReq;

/**
 * @typedef smb2_LogoffResp
 * @brief   SMB2/3 Logoff Response message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.
 * @see     #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_LogoffResp;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseLogoffReq( uint16_t        const dialect,
                         uint8_t        *const msg,
                         size_t          const msgLen,
                         smb2_LogoffReq *const logoffReq );

int smb2_packLogoffReq( uint16_t        const dialect,
                        smb2_LogoffReq *const logoffReq,
                        uint8_t        *const bufr,
                        uint32_t        const bSize );

int smb2_parseLogoffResp( uint16_t         const dialect,
                          uint8_t         *const msg,
                          size_t           const msgLen,
                          smb2_LogoffResp *const logoffResp );

int smb2_packLogoffResp( uint16_t         const dialect,
                         smb2_LogoffResp *const logoffResp,
                         uint8_t         *const bufr,
                         uint32_t         const bSize );


/* ============================= smb2_logoff.h ============================== */
#endif /* SMB2_LOGOFF_H */
