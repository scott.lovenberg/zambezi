/* ========================================================================== **
 *                              smb2_negotiate.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: SMB2/3 Negotiate Request/Response message handling.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_negotiate.c; 2021-03-10 17:06:14 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *  - This one was a pain in the buttons to write.
 *
 * ========================================================================== **
 */

#include <assert.h>           /* For debugging; see assert(3).  */

#include "smb_endian.h"       /* SMB byte order conversions.    */
#include "smb_util.h"         /* Utility macros.                */
#include "smb2_negotiate.h"   /* Module header.                 */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *
 *  NEG_CTX_HDR_SIZE    - Size of the Negotiate Context header block,
 *                        which is the prefix of every SMB3 Negotiate
 *                        Context object.
 *
 *  NEG_SEC_BUFR_OFFSET - The standard offset of the Negotiate Response
 *                        Security Buffer.  This is the offset (from the
 *                        start of the SMB2 header) at which the Negotiate
 *                        Response SecurityBuffer field should be located.
 *                        It is possible (but unlikely) that there will be
 *                        padding ahead of the SecurityBuffer.
 */

#define NEG_CTX_HDR_SIZE      8

#define NEG_SEC_BUFR_OFFSET 128


/* -------------------------------------------------------------------------- **
 * Functions
 */

/* Parse the Negotiate Request mess.
 *  A comedy in two or three acts, depending...
 *  - Parse the message.
 *  - Normalize the dialect list and check for the 3.1.1 (or above) dialect.
 *  - If the dialect is 3.1.1 or greater, reparse the message looking for
 *    contexts [see smb2_parseNegReq2ndPass()].
 */

int smb2_parseNegotiateReq( uint8_t           *const msg,
                            size_t             const msgLen,
                            smb2_NegotiateReq *const negReq )
  /** Unpack an SMB2 Negotiate Request message (first pass).
   *
   * @param[in]   msg     The source buffer, containing the received message
   *                      in wire format.  This must indicate the start of
   *                      the Negotiate Request block, which immediately
   *                      follows the SMB2 Header.
   * @param[in]   msgLen  The length of the message, excluding the header,
   *                      in bytes (that is, the size of \p msg).
   * @param[out]  negReq  A pointer to an \c #smb2_NegotiateReq structure.
   *                      This structure should be zeroed by the caller
   *                      before it is passed in.  If all goes well, it will
   *                      be filled with goodies upon return.
   *
   * @returns   On success, a positive value indicating the number of bytes
   *            of \p msg that have been parsed.  This is the size of the
   *            fixed portion of the SMB2 Negotiate Request message (36).\n
   *            On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value.
   *  - -2  \b ERROR: Empty dialect list; \c DialectCount is zero.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of an SMB2/3
   *                  Negotiate Request.  A minimum of \c #SMB2_NEG_REQ_SIZE
   *                  bytes are required.
   *
   * \b Notes
   *  - It is assumed that some basic sanity checking has been performed
   *    before this function is called.  However, the \c assert(3) macro is
   *    used to check that pointer parameters are not NULL.  For production
   *    use, compile with \c NDEBUG set.
   *  - This function unpacks only the fixed portion of the message.  The
   *    dynamic portion, which includes the list of \c Dialects and the
   *    optional \c ContextList, must be parsed separately.
   *  - On the first pass, the Negotiate Request must be assumed to be in
   *    its most primative form: SMBv2.0.2.  This function parses the
   *    message under that assumtion.
   *  - Pointers...
   *    - \b \c #smb2_NegotiateReq::ClientGuid <br>
   *      Unless there is a parsing error, this will point to the Client
   *      GUID, which is contained within the fixed portion of the message.
   *    - \b \c #smb2_NegotiateReq::Dialects <br>
   *      If \p msgLen is at least <tt>((2 * DialectCount) +
   *      #SMB2_NEG_REQ_SIZE)</tt> bytes in length (that is, large enough to
   *      include the entire dialect list), this will point to the location
   *      immediately following the fixed portion of the message.  Otherwise,
   *      this pointer will be NULL.
   *    - All other pointers in the \c #smb2_NegotiateReq structure will be
   *      set to NULL.  The fields to which they refer were introduced in
   *      v3.1.1 and are not recognized by earlier dialects.
   *
   * @see \c smb2_getDialectList()
   * @see \c smb2_parseNegReq2ndPass()
   * @see \c smb2_parseNegotiateCtx()
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != negReq );
  /* Check buffer size. */
  if( msgLen < SMB2_NEG_REQ_SIZE )
    return( -SMB2_NEG_REQ_SIZE );
  /* Test StructureSize. */
  if( (negReq->StructureSize = smbget16( msg )) != SMB2_NEG_REQ_STRUCT_SIZE )
    return( -1 );

  /* Unpack that rascal.  Assume we are pre-3.1.1. */
  negReq->DialectCount    = smbget16( &msg[2] );
  negReq->SecurityMode    = smbget16( &msg[4] );
  negReq->Capabilities    = smbget32( &msg[8] );
  negReq->ClientGuid      = &msg[12];
  /* Copy the reserved fields. */
  (void)memcpy( negReq->Reserved1, &msg[6], 2 );
  (void)memcpy( &(negReq->ClientStartTime), &(msg[28]), 8 );

  /* Pointers to objects located beyond the fixed portion of the message. */
  negReq->Dialects             = NULL;
  negReq->Padding              = NULL;
  negReq->NegotiateContextList = NULL;

  /* Check for an empty dialect list. */
  if( 0 == negReq->DialectCount )
    return( -2 );
  /* See if we can find the dialect list. */
  if( msgLen >=
      (SMB2_NEG_REQ_SIZE + (negReq->DialectCount *sizeof( uint16_t ))) )
    {
    negReq->Dialects = (uint16_t *)&(msg[SMB2_NEG_REQ_SIZE]);
    }

  return( SMB2_NEG_REQ_SIZE );
  } /* smb2_parseNegotiateReq */

int smb2_parseNegReq2ndPass( uint16_t           const dialect,
                             uint8_t           *const msg,
                             size_t             const msgLen,
                             smb2_NegotiateReq *const negReq )
  /** Unpack an SMB2 Negotiate Request message (second pass).
   *
   * @param[in]     dialect The dialect to use when re-parsing the message.
   * @param[in]     msg     The source buffer, containing the received
   *                        message in wire format.  This \b must point to
   *                        the same message buffer that was initially parsed
   *                        by the \c smb2_parseNegotiateReq() function.
   * @param[in]     msgLen  The length of the message, excluding the header,
   *                        in bytes.
   * @param[in,out] negReq  A pointer to an \c #smb2_NegotiateReq structure.
   *                        This \b should be the same #smb2_NegotiateReq
   *                        structure that was returned from a preceding
   *                        call to \c smb2_parseNegotiateReq().
   *
   * @returns   On success, the number of \b contexts (as indicated by the
   *            \c NegotiateContextCount field) will be returned.  If there
   *            are no contexts, zero (0) is returned.\n
   *            A negative value is returned on error.
   *
   * \b Errors
   *  - -1  \b ERROR: Bogus offset; the value of \c NegotiateContextOffset
   *                  indicates an invalid location.  The offset must be a
   *                  multiple of eight (8), and must indicate a location
   *                  beyond the end of the dialect list.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the complete Negotiate Request.
   *                  A minimum of \c #SMB2_NEG_REQ_SIZE bytes are needed.
   *
   * \b Notes
   *  - The \p msg and \p negReq parameters should be the same as those
   *    passed into an earlier call to \c smb2_parseNegotiateReq().
   *  - The \p msg pointer should indicate the start of the fixed-length
   *    portion of the Negotiate Request, which follows immediately after
   *    the SMB2 Header.
   *  - This function uses \p dialect to decide how to re-parse the received
   *    message.
   *    - For dialects prior to SMBv3.1.1 there is no additional parsing to
   *      be done and the function will immediately return 0 (success).
   *    - For SMBv3.1.1 and above,
   *      - The \c ClientStartTime field will be overwritten.
   *      - The \c Reserved2, \c NegotiateContextOffset, and
   *        \c NegotiateContextCount fields will be filled in instead.
   *      - An attempt will be made to set the \c NegotiateContextList and
   *        \c Padding pointers.  These will only be assigned if they both
   *        begin within the buffer, as indicated by \p msg and \p msgLen.
   *      - If there is no padding, the \c Padding pointer will be NULL.
   *      - The value of the \c DialectCount field, filled in by the previous
   *        pass, will be used to locate the start of the \c Padding, if any.
   *      - The \c NegotiateContextList field will be set \e IFF the Context
   *        List begins within the given \p msg buffer.  This does not ensure
   *        that the entire Context List is contained within the buffer.
   *  - So...
   *    - If the return value is negative, there's an error to be handled.
   *    - If the return value is zero (0), there are no contexts in the
   *      message.
   *    - If the return value is positive, it represents the number of
   *      contexts in the context block and:
   *      - If there is no padding, the \c Padding pointer will be NULL.
   *        Otherwise, it will point to the first byte of padding.  The
   *        padding content should be zero and should be ignored.
   *      - The \c NegotiateContextList pointer will point to the start of
   *        the first \c #smb2_Negotiate_Context in the list.
   *
   * @see \c smb2_parseNegotiateReq()
   * @see \c smb2_parseNegotiateCtx()
   */
  {
  uint32_t minOffset = 0;
  uint32_t padLen    = 0;

  /* The basics. */
  if( dialect < SMB2_V0311 )
    return( 0 );  /* Nothing to do. */

  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != negReq );
  /* Check buffer size. */
  if( msgLen < SMB2_NEG_REQ_SIZE )
    return( -SMB2_NEG_REQ_SIZE );

  /* Extract the context offset and length.
   *  This is the main point of this function.
   *  Everything else is just frosting on the eels.
   */
  negReq->NegotiateContextOffset = smbget32( &msg[28] );
  negReq->NegotiateContextCount  = smbget16( &msg[32] );
  (void)memcpy( negReq->Reserved2, &msg[34], 2 );

  /* Check of the Context Count and Offset.
   *  If the count is zero, all's well and we're done.
   *  If the offset is not a multiple of 8, or is too small, error.
   */
  if( 0 == negReq->NegotiateContextCount )
    return( 0 );
  if( 0 != (negReq->NegotiateContextOffset % 8) )
    return( -1 );
  minOffset = SMB2_NEG_REQ_SIZE
            + (uint32_t)(negReq->DialectCount * sizeof( uint16_t ));
  if( negReq->NegotiateContextOffset < (minOffset + SMB2_HEADER_SIZE) )
    return( -1 );

  /* Re-initialize the pointer fields, just to be sure. */
  negReq->Padding = NULL;
  negReq->NegotiateContextList = NULL;

  /* Do the next set of bytes (padding or Ctx Offset) fall within <msg>?
   *  If not, return as-is.
   */
  if( SMB_ALIGN8( minOffset ) > msgLen )
    return( negReq->NegotiateContextCount );

  /* Calculate padding length, and see if the padding is within <msg>.  */
  padLen = negReq->NegotiateContextOffset - (minOffset + SMB2_HEADER_SIZE);
  if( (padLen > 0) && ((minOffset + padLen) <= msgLen ) )
    negReq->Padding = &(msg[minOffset]);

  /* Calculate the offset relative to <msg> and point there.
   *  The size of the context list is not given in the message, so we
   *  don't know whether we have the whole thing or not.  We can,
   *  however, determine whether the Context list starts within <msg>.
   */
  minOffset = (negReq->NegotiateContextOffset - SMB2_HEADER_SIZE);
  if( minOffset < msgLen )
    negReq->NegotiateContextList = &(msg[minOffset]);

  return( negReq->NegotiateContextCount );
  } /* smb2_parseNegReq2ndPass */

/* Pack a Negotiate Request mess. */

int smb2_packNegotiateReq( smb2_NegotiateReq *const negReq,
                           uint8_t           *const bufr,
                           uint32_t           const bSize )
  /** Compose a common-form SMB2 Negotiate Request message.
   *
   * @param[in]   negReq  A pointer to an \c #smb2_NegotiateReq structure
   *                      containing the data needed to create the
   *                      wire-format request message.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      Negotiate reqest message will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On success, a positive number, indicating the number of bytes
   *            written to \p bufr.  On error, a negative value is returned.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; more than \p bSize bytes are required
   *                  for the composed message.  The absolute value of
   *                  \c n provides the minimum number of bytes required.
   *
   * \b Notes
   *  - By "common-form", we mean the basic format introduced with SMBv2.0.2.
   *    With SMBv3.1.1, an updated format that allows for Negotiate Contexts
   *    was introduced.  The updated format is backward compatible with the
   *    original "common" format.  The \c smb2_packNegReq2ndPass() function
   *    can be used to update the composed message to include Negotiate
   *    Context data.
   *  - If \c negReq->Dialects is NULL, and/or \c negReq->DialectCount is
   *    zero (0), then no dialect entries will be copied to \p bufr.  The
   *    The value of the \c negReq->DialectCount field is copied into the
   *    \p bufr in any case.
   *  - If \c negReq->Dialects is NULL then the number of dialect entries,
   *    as indicated by \c negReq->DialectCount, is ignored when calculating
   *    the minimum size for \p bufr.  This assumes that the dialect list
   *    will be stored in a separate memory block, which is a perfectly
   *    reasonable thing to do.
   *
   * @see \c smb2_setDialectList()
   * @see \c smb2_packNegReq2ndPass()
   * @see \c smb2_packNegotiateCtx()
   */
  {
  int      i;
  uint8_t *p       = bufr;
  int      minSize = SMB2_NEG_REQ_SIZE;

  /* Sanity Checks. */
  assert( NULL != negReq );
  assert( NULL != bufr );

  /* Verify the minimum buffer size. */
  if( NULL != negReq->Dialects )
    minSize += (int)(negReq->DialectCount * sizeof( uint16_t ));
  if( bSize < minSize )
    return( -minSize );

  /* Write basic values into the buffer. */
  p = smbset16( p, negReq->StructureSize );           /* StructureSize    */
  p = smbset16( p, negReq->DialectCount );            /* DialectCount     */
  p = smbset16( p, negReq->SecurityMode );            /* SecurityMode     */
  p = smbcpmem( p, negReq->Reserved1, 2 );            /* Reserved1        */
  p = smbset32( p, negReq->Capabilities );            /* Capabilities     */
  p = smbcpmem( p, negReq->ClientGuid, 16 );          /* ClientGuid       */
  p = smbcpmem( p, &negReq->ClientStartTime, 8 );     /* ClientStartTime  */

  /* Write the dialect list. */
  if( NULL != negReq->Dialects )
    {
    for( i = 0; i < negReq->DialectCount; i++ )
      p = smbset16( p, negReq->Dialects[i] );
    }
  return( minSize );
  } /* smb2_packNegotiateReq */

int smb2_packNegReq2ndPass( smb2_NegotiateReq *const negReq,
                            uint8_t           *const bufr,
                            uint32_t           const bSize )
  /** Update a packed Negotiate Request with Negotiate Context data.
   *
   * @param[in]   negReq  A pointer to a \c #smb2_NegotiateReq structure
   *                      containing the Negotiate Context data needed
   *                      to update the message buffer.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      updated Negotiate reqest values will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On error, a negative number.
   *            On success, the number of \c Padding bytes required to
   *            correctly align the Negotiate Context, which will be one
   *            of [0, 2, 4, 6].
   *
   * \b Errors
   *    -n  \b ERROR: Short buffer; based on \p bSize, the input \b bufr
   *                  not large enough to contain the fixed portion of a
   *                  Negotiate Request message.  \c |n| is the minimum
   *                  number of bytes required which, in this case, is
   *                  \c #SMB2_NEG_REQ_SIZE.
   *
   * \b Notes
   *  - This function does not add \p Padding or context data to message.
   *    It updates the fixed portion of the message by overwriting the
   *    \c #smb2_NegotiateReq::ClientStartTime field.  Instead,
   *    \c #smb2_NegotiateReq::NegotiateContextOffset and
   *    \c #smb2_NegotiateReq::NegotiateContextCount are filled in (as is
   *    the \c #smb2_NegotiateReq::Reserved2 field).  These fields occupy
   *    the same location in the outgoing message.
   *
   * @see \c smb2_packNegotiateReq()
   * @see \c smb2_packNegotiateCtx()
   */
  {
  uint8_t *p;
  int      rslt;

  /* Sanity Checks. */
  assert( NULL != negReq );
  assert( NULL != bufr );
  if( bSize < SMB2_NEG_REQ_SIZE )
    return( -SMB2_NEG_REQ_SIZE );

  /* The busywork.  */
  p = &bufr[28];
  p = smbset32( p, negReq->NegotiateContextOffset );
  p = smbset16( p, negReq->NegotiateContextCount );
  p = smbcpmem( p, negReq->Reserved2, 2 );

  /* Calculate padding. */
  rslt = SMB2_NEG_REQ_SIZE + (int)(negReq->DialectCount * sizeof( uint16_t ));
  return( SMB_PAD8( rslt ) );
  } /* smb2_packNegReq2ndPass */

/* Parse a Negotiate Response message.  */

int smb2_parseNegotiateResp( uint8_t            *const msg,
                             size_t              const msgLen,
                             smb2_NegotiateResp *const negResp )
  /** Unpack an SMB2 Negotiate Response message, first pass.
   *
   * @param[in]   msg     The source buffer, containing the received
   *                      message in wire format.  This must indicate the
   *                      start of the Negotiate Response block, which
   *                      immediately follows the SMB2 header.
   * @param[in]   msgLen  The length of the message, excluding the header,
   *                      in bytes.
   * @param[out]  negResp A pointer to an #smb2_NegotiateResp structure.
   *                      As usual, this structure should be zeroed by the
   *                      caller.
   *
   * @returns   On success, a positive value indicating the number of bytes
   *            of \p msg that have been parsed.  This will be the size of
   *            the fixed portion of the SMB2 Negotiate Response (always
   *            64).  The length of the Security Buffer and any Context
   *            blocks are not included in the returned result.\n
   *            On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the StructureSize field did not
   *                  contain the required value.
   *  - -2  \b ERROR: The \c #smb2_NegotiateResp::SecurityBufferOffset field
   *                  contains an offset that is too small (indicates a
   *                  location within the fixed portion of the message).
   *  - -3  \b ERROR: The \c #smb2_NegotiateResp::NegotiateContextList
   *                  begins at a non-aligned offset, or is too small
   *                  (indicates an offset that is within either the fixed
   *                  portion of the message or the Security Buffer).
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of a Negotiate
   *                  Response message.  A minimum of \c #SMB2_NEG_RESP_SIZE
   *                  bytes are needed.
   *
   * \b Notes
   *  - Pointers
   *    - The \c #smb2_NegotiateResp::ServerGuid field of \p negResp will
   *      point to the location in \p msg at which the Server's GUID value
   *      is stored.  The server GUID is a fixed \c #SMB2_SERVER_GUID_LENGTH
   *      bytes in length.
   *    - The \c #smb2_NegotiateResp::SecurityBuffer field in \p negResp
   *      will point to the start of the security buffer, or will be NULL
   *      if the start of the security buffer is beyond the end of the given
   *      \p msg buffer, as determined by \p msgLen.
   *    - The \c #smb2_NegotiateResp::NegotiateContextList is only checked
   *      if the designated dialect is 3.1.1 or greater.  Otherwise, the
   *      field is set to NULL in \p negResp.  \c NegotiateContextList will
   *      also be NULL if the dialect is 3.1.1 or greater and the start of
   *      the context list is beyond end of the \p msg buffer, as indicated
   *      by \p msgLen.
   *  - This function checks to see whether either the Security Buffer or
   *    the Negotiate Context List \em start at an illegal position.  In
   *    particular, do either of them (based upon the offset given in the
   *    message) start within the fixed-length part of the message, or
   *    does the Negotiate Context List start within the Security Buffer.
   *    Either of these unfortunate conditions would indicate a malformed
   *    message.
   *  - This function <em>does not</em> check to see if the Security Buffer
   *    or the Negotiate Context List extend beyond the end of the message.
   *  - The value of \p msgLen need not be more than the length of the
   *    fixed-length section of the message.
   *  - This function does not apply any filters to the
   *    \c #smb2_NegotiateResp::Capabilities field after parsing.  For
   *    pedantic correctness, use \c smb2_filterServerCaps().
   *
   * @see \c smb2_parseNegotiateCtx()
   * @see \c smb2_filterServerCaps()
   */
  {
  int bytesUsed = SMB2_NEG_RESP_SIZE;   /* Bytes of <msg> consumed. */
  int relOffset = 0;                    /* Scratch variable.        */

  /* Insanity checks. */
  assert( NULL != msg );
  assert( NULL != negResp );
  if( msgLen < SMB2_NEG_RESP_SIZE )
    return( -SMB2_NEG_RESP_SIZE );
  if( (negResp->StructureSize = smbget16( msg )) != SMB2_NEG_RESP_STRUCT_SIZE )
    return( -1 );

  /* Unpacket */
  negResp->SecurityMode         = smbget16( &msg[2] );
  negResp->DialectRevision      = smbget16( &msg[4] );
  (void)memcpy( negResp->Reserved1, &msg[6], 2 );
  negResp->ServerGuid           = &msg[8];
  negResp->Capabilities         = smbget32( &msg[24] );
  negResp->MaxTransactSize      = smbget32( &msg[28] );
  negResp->MaxReadSize          = smbget32( &msg[32] );
  negResp->MaxWriteSize         = smbget32( &msg[36] );
  negResp->SystemTime           = smbget64( &msg[40] );
  negResp->ServerStartTime      = smbget64( &msg[48] );
  negResp->SecurityBufferOffset = smbget16( &msg[56] );
  negResp->SecurityBufferLength = smbget16( &msg[58] );
  (void)memcpy( negResp->Reserved2, &msg[60], 4 );
  negResp->SecurityBuffer       = NULL;
  negResp->NegotiateContextList = NULL;

  /* Find the Security Buffer, if present. */
  if( 0 != negResp->SecurityBufferLength )
    {
    if( negResp->SecurityBufferOffset < NEG_SEC_BUFR_OFFSET )
      return( -2 );     /* Starts before the end of the fixed portion.  */
    relOffset = (int)(negResp->SecurityBufferOffset - SMB2_HEADER_SIZE);
    if( relOffset <= msgLen )
      {
      negResp->SecurityBuffer = &msg[relOffset];
      bytesUsed = relOffset + negResp->SecurityBufferLength;
      }
    }

  /* Version 3.1.1 adds support for Negotiate Contexts. */
  if( negResp->DialectRevision >= SMB2_V0311 )
    {
    negResp->NegotiateContextCount  = smbget16( &msg[6] );
    negResp->NegotiateContextOffset = smbget32( &msg[60] );
    if( negResp->NegotiateContextCount > 0 )
      {
      if( (negResp->NegotiateContextOffset < bytesUsed)
       || !SMB_IS_ALIGNED8( negResp->NegotiateContextOffset ) )
        return( -3 );
      relOffset = (int)(negResp->NegotiateContextOffset - SMB2_HEADER_SIZE);
      if( relOffset <= msgLen )
        negResp->NegotiateContextList = &msg[relOffset];
      }
    }
  return( SMB2_NEG_RESP_SIZE );
  } /* smb2_parseNegotiateResp */

/* Pack a Negotiate Response message.  */

int smb2_packNegotiateResp( const smb2_NegotiateResp *const negResp,
                            uint8_t                  *const bufr,
                            uint32_t                  const bSize )
  /** Compose the fixed portion of an SMB2 Negotiate Response message.
   *
   * @param[in]   negResp A pointer to an \c #smb2_NegotiateResp structure,
   *                      with all of the values required for composing the
   *                      fixed portion of the Negotiate Response message.
   * @param[out]  bufr    A pointer to a buffer into which the composed
   *                      Negotiate Response message will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On success, the number of bytes of \p bufr that were
   *            consumed.  On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: The \c SecurityBufferOffset indicates a location
   *                  within the fixed portion of the message buffer.
   *  - -2  \b ERROR: The \c NegotiateContextOffset indicates a location
   *                  within the Security buffer or the fixed portion of
   *                  the message buffer.
   *  - -n  \b ERROR: Short buffer; At least \c abs(n) bytes are required
   *                  in order to add the security buffer to the fixed
   *                  portion of the message.
   *
   * \b Notes
   *  - The dialect indicated in the \c #smb2_NegotiateResp::DialectRevision
   *    field of the \p negResp parameter be used to determine whether to
   *    write the values of \c NegotiateContextCount and
   *    \c NegotiateContextOffset into the buffer.  If the dialect is less
   *    than 3.1.1, the \c Reserved1 and \c Reserved2 values will be copied
   *    instead.
   *  - This function does not apply any filters to the
   *    \c #smb2_NegotiateResp::Capabilities field.  For pedantic
   *    correctness, use \c smb2_filterServerCaps().
   *
   * @see \c smb2_packNegotiateCtx()
   * @see \c smb2_filterServerCaps()
   */
  {
  uint8_t *p = bufr;
  uint32_t offset;

  /* Sanity Checks. */
  assert( NULL != negResp );
  assert( NULL != bufr );

  /* Verify the minimum buffer size. */
  if( bSize < SMB2_NEG_RESP_SIZE )
    return( -SMB2_NEG_RESP_SIZE );

  /* Sanity check the SecurityBufferOffset. */
  if( negResp->SecurityBufferOffset < (SMB2_HEADER_SIZE + SMB2_NEG_RESP_SIZE) )
    return( -1 );

  /* Pack that packet. */
  p = smbset16( p, negResp->StructureSize );
  p = smbset16( p, negResp->SecurityMode );
  p = smbset16( p, negResp->DialectRevision );
  p = smbcpmem( p, negResp->Reserved1, 2 );
  p = smbcpmem( p, negResp->ServerGuid, 16 );
  p = smbset32( p, negResp->Capabilities );
  p = smbset32( p, negResp->MaxTransactSize );
  p = smbset32( p, negResp->MaxReadSize );
  p = smbset32( p, negResp->MaxWriteSize );
  p = smbset64( p, negResp->SystemTime );
  p = smbset64( p, negResp->ServerStartTime );
  p = smbset16( p, negResp->SecurityBufferOffset );
  p = smbset16( p, negResp->SecurityBufferLength );
  p = smbcpmem( p, negResp->Reserved2, 4 );
  if( negResp->DialectRevision >= SMB2_V0311 )
    {
    (void)smbset16( &bufr[6],  negResp->NegotiateContextCount );
    (void)smbset32( &bufr[60], negResp->NegotiateContextOffset );
    /* Sanity check the NegotiateContextOffset. */
    offset  = negResp->SecurityBufferOffset;
    offset += negResp->SecurityBufferLength;
    if( negResp->NegotiateContextOffset < SMB_ALIGN8( offset ) )
      return( -2 );
    }
  return( SMB2_NEG_RESP_SIZE );
  } /* smb2_packNegotiateResp */

/* Parse/pack Negotiate Context Headers.  */

int smb2_parseNegotiateCtx( uint8_t                *const msgCtx,
                            size_t const                  msgLen,
                            smb2_Negotiate_Context *const negCtx )
  /** Unpack a single Negotiate Context header from an SMB2 Negotiate Request.
   *
   * @param[in]   msgCtx  A pointer to a wire-format Negotiate Context to be
   *                      parsed.
   * @param[in]   msgLen  The number of bytes available in the buffer
   *                      indicated by \p msgCtx.
   * @param[out]  negCtx  A pointer to an #smb2_Negotiate_Context structure
   *                      into which the message will be parsed.
   *
   * @returns   On success, 8 is returned.  This is the number of bytes of
   *            \p msgCtx that are consumed by parsing the SMB2 Negotiate
   *            Context header.  On error, a negative value is returned.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msgCtx
   *                  is too short to contain a complete Negotiate Context
   *                  header.  The return value indicates the minimum number
   *                  of bytes needed in order to parse a complete Negotiate
   *                  Context header (8).
   */
  {
  /* Pain and suffering. */
  assert( NULL != msgCtx );
  assert( NULL != negCtx );
  if( msgLen < NEG_CTX_HDR_SIZE )
    return( -NEG_CTX_HDR_SIZE );

  /* Quick parse. */
  negCtx->ContextType = smbget16( msgCtx );
  negCtx->DataLength  = smbget16( &msgCtx[2] );
  (void)memcpy( negCtx->Reserved, &msgCtx[4], 4 );
  negCtx->Data        = &msgCtx[NEG_CTX_HDR_SIZE];
  return( NEG_CTX_HDR_SIZE );
  } /* smb2_parseNegotiateCtx */

int smb2_packNegotiateCtx( smb2_Negotiate_Context *const negCtx,
                           uint8_t                *const bufr,
                           uint32_t                const bSize )
  /** Compose a Negotiate Context header.
   *
   * @param[in]   negCtx  Pointer to an \c #smb2_Negotiate_Context
   *                      structure containing the values to be
   *                      transmitted.
   * @param[out]  bufr    Pointer to a buffer into which the context header
   *                      data will be written.
   * @param[in]   bSize   The size, in bytes of \p bufr.
   *
   * @returns   On success, the number of bytes of \p bufr that were
   *            consumed; this will always be eight (8) bytes.  On error,
   *            a negative value is returned.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; the buffer must be at least the wire
   *                  size of the Negotiate Context header, which is eight
   *                  (8) bytes.  If \p bSize indicates that the buffer is
   *                  less than that, the function will return -8.
   */
  {
  uint8_t *p = bufr;

  /* Sanity Checks. */
  assert( NULL != negCtx );
  assert( NULL != bufr );
  /* Verify the minimum buffer size. */
  if( bSize < NEG_CTX_HDR_SIZE )
    return( -NEG_CTX_HDR_SIZE );

  /* Packit */
  p = smbset16( p, negCtx->ContextType );
  p = smbset16( p, negCtx->DataLength );
  p = smbcpmem( p, negCtx->Reserved, 4 );
  return( NEG_CTX_HDR_SIZE );
  } /* smb2_packNegotiateCtx */

/* Parse and Pack known Negotiate Context structures. */

int smb2_parsePreAuthIntegrityCaps( uint8_t                     *const ctxData,
                                    uint16_t                     const dataLen,
                                    smb2_PreAuth_Integrity_Caps *const paCaps )
  /** Parse a PreAuth Integrity Capabilities blob.
   *
   * @param[in]   ctxData   A constant pointer to a buffer containing a
   *                        wire-format PreAuth Integrity Negotiate Context.
   * @param[in]   dataLen   The size, in bytes, of the buffer indicated by
   *                        \p ctxData.
   * @param[out]  paCaps    A pointer to an #smb2_PreAuth_Integrity_Caps
   *                        structure that will receive the parsed data.
   *
   * @returns   On error, a negative value is returned. \n
   *            On success, the number of bytes consumed by parsing the
   *            message.
   *
   * \b Errors
   *  - -4  \b ERROR: Short buffer; the input \p ctxData buffer must be at
   *                  least 4 bytes in length to provide the count of hash
   *                  algorithms and the length of the "salt" field.
   *  - -n  \b ERROR: Short buffer; based on \p dataLen, the input \p ctxData
   *                  is too short to contain the PreAuth Integrity Negotiate
   *                  Context payload.  The return value provides the minimum
   *                  number of bytes required.  If this is greater than
   *                  the value of \c #smb2_Negotiate_Context::DataLength
   *                  (from the context header) then this context is
   *                  malformed.
   *
   * \b Notes
   *  - On success, the return value will be the number of bytes consumed.
   *    Use the \c SMB_ALIGN8() macro to find the offset of the next
   *    context block in the list (if any).
   *  - The length of the context data (without padding) is given in the
   *    \c #smb2_Negotiate_Context::DataLength field.
   *  - This function does not validate the number of entries in the
   *    #smb2_PreAuth_Integrity_Caps::HashAlgorithmCount field.  This value
   *    \b should be 1 since (at present) only one
   *    #SMB2_Negotiate_HashAlgorithm is defined.  More hash algorithms may,
   *    however, be added without a corresponding update to the Dialect Id.
   *  - This function <em>does not</em> convert the list of
   *    #smb2_PreAuth_Integrity_Caps::HashAlgorithms to host byte
   *    order.  \b See: smbget16array().
   *
   * @see \c smb2_parseNegotiateCtx()
   * @see \c SMB_ALIGN8()
   */
  {
  int saltOffset;
  int msgSize;

  /* First test: Ensure that we have *at least* 4 bytes. */
  if( dataLen < 4 )
    return( -4 );

  /* Numerics. */
  paCaps->HashAlgorithmCount = smbget16(  ctxData );
  paCaps->SaltLength         = smbget16( &ctxData[2] );
  saltOffset = 4 + (int)(paCaps->HashAlgorithmCount * sizeof(uint16_t));
  msgSize    = (int)(saltOffset + paCaps->SaltLength);

  /* Now verify the total data length. */
  if( msgSize < dataLen )
    return( -msgSize );

  /* Data blobs. */
  paCaps->HashAlgorithms = (uint16_t *)(&ctxData[4]);
  paCaps->Salt           = &ctxData[saltOffset];

  return( msgSize );
  } /* smb2_parsePreAuthIntegrityCaps */

int smb2_packPreAuthIntegrityCaps( smb2_PreAuth_Integrity_Caps *const paCaps,
                                   uint8_t                     *const bufr,
                                   uint32_t                     const bSize )
  /** Pack a PreAuth Integrity Capabilities blob.
   *
   * @param[in]   paCaps  Pointer to a \c #smb2_PreAuth_Integrity_Caps
   *                      structure containing the data to be marshalled.
   * @param[out]  bufr    Pointer to a buffer into which the
   *                      Preauthentication Integrity Capabilities data will
   *                      be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On success, the number of bytes of \p bufr that were
   *            consumed.  On error, a negative value.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; based on \p bSize, the given \p bufr
   *                  is not large enough to be used to store the composed
   *                  message.  A minimum of \c abs( n ) bytes are required.
   *
   * @see \c smb2_packNegotiateCtx()
   * @see \c SMB_ALIGN8()
   */
  {
  uint8_t *p = bufr;
  int      saltOffset;
  int      msgSize;

  /* Sanity Checks. */
  assert( NULL != paCaps );
  assert( NULL != bufr );
  /* Verify the minimum buffer size. */
  saltOffset = 4 + (int)(paCaps->HashAlgorithmCount * sizeof( uint16_t ));
  msgSize    = (int)(saltOffset + paCaps->SaltLength);
  if( bSize < msgSize )
    return( -msgSize );

  /* Pack. */
  p = smbset16( p, paCaps->HashAlgorithmCount );
  p = smbset16( p, paCaps->SaltLength );
  p += smbset16array( (uint16_t *)p,
                      paCaps->HashAlgorithms,
                      paCaps->HashAlgorithmCount );
  p = smbcpmem( p, paCaps->Salt, paCaps->SaltLength );
  return( msgSize );
  } /* smb2_packPreAuthIntegrityCaps */

int smb2_parseEncryptionCaps( uint8_t              *const ctxData,
                              uint16_t              const dataLen,
                              smb2_Encryption_Caps *const enCaps )
  /** Parse a Negotiate Encryption Capabilities blob.
   *
   * @param[in]   ctxData   A constant pointer to an Encryption Capabilities
   *                        Negotiate Context data blob.
   * @param[in]   dataLen   The size, in bytes, of the buffer indicated by
   *                        \p ctxData.
   * @param[out]  enCaps    A pointer to an #smb2_Encryption_Caps structure
   *                        that will receive the parsed data.
   *
   * @returns   On error, a negative value is returned. \n
   *            On success, the number of bytes consumed by parsing the
   *            message.
   *
   * \b Errors
   *  - -2  \b ERROR: Short buffer; the \p ctxData buffer must be at least 2
   *                  bytes in length to provide the count of ciphers.
   *  - -n  \b ERROR: Short buffer; based on \p dataLen, the input \p ctxData
   *                  is too short to contain the array of Ciphers.
   *
   * \b Notes
   *  - This function does not check that the number of entries in the
   *    #smb2_Encryption_Caps::CipherCount field is valid.  This value
   *    should be greater than zero.
   *  - This function does not convert the list of
   *    #smb2_Encryption_Caps::Ciphers to host byte order.  \b See:
   *    smbget16array().
   *
   * @see \c smb2_parseNegotiateCtx()
   */
  {
  int msgSize;

  /* We need to know the CipherCount. */
  if( dataLen < 2 )
    return( -2 );

  enCaps->CipherCount = smbget16( ctxData );
  msgSize = 2 + (int)(enCaps->CipherCount * sizeof( uint16_t ));
  if( msgSize < dataLen )
    return( -msgSize );
  enCaps->Ciphers = (uint16_t *)(&ctxData[2]);

  return( msgSize );
  } /* smb2_parseEncryptionCaps */

int smb2_packEncryptionCaps( smb2_Encryption_Caps *const enCaps,
                             uint8_t              *const bufr,
                             uint32_t              const bSize )
  /** Pack a Negotiate Encryption Capabilities blob.
   *
   * @param[in]   enCaps  Pointer to a \c #smb2_Encryption_Caps structure
   *                      containing the data to be marshalled.
   * @param[out]  bufr    Pointer to a buffer into which the Encryption
   *                      Capabilities data will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On success, the number of bytes of \p bufr that were
   *            consumed.  On error, a negative value.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; based on \p bSize, the given \p bufr
   *                  is not large enough to be used to store the composed
   *                  message.  A minimum of \c abs( n ) bytes are required.
   *
   * @see \c smb2_packNegotiateCtx()
   * @see \c SMB_ALIGN8()
   */
  {
  uint8_t *p = bufr;
  int      msgSize;

  /* Sanity Checks. */
  assert( NULL != enCaps );
  assert( NULL != bufr );
  /* Verify the minimum buffer size. */
  msgSize = 2 + (int)(enCaps->CipherCount * sizeof( uint16_t ));
  if( bSize < msgSize )
    return( -msgSize );

  /* Pack. */
  p = smbset16( p, enCaps->CipherCount );
  (void)smbset16array( (uint16_t *)p, enCaps->Ciphers, enCaps->CipherCount );
  return( msgSize );
  } /* smb2_packEncryptionCaps */

int smb2_parseCompressionCaps( uint8_t               *const ctxData,
                               uint16_t const               dataLen,
                               smb2_Compression_Caps *const cCaps )
  /** Parse a Compression Capabilities blob.
   *
   * @param[in]   ctxData   A constant pointer to the context data that is
   *                        the Compression Capabilities Negotiate Context.
   * @param[in]   dataLen   The size, in bytes, of the buffer indicated by
   *                        \p ctxData.
   * @param[out]  cCaps     A pointer to an \c #smb2_Compression_Caps
   *                        structure that will receive the parsed data.
   *
   * @returns   On error, a negative value is returned. \n
   *            On success, the number of bytes consumed by parsing the
   *            message.
   *
   * \b Errors
   *  - -8  \b ERROR: Short buffer; based on \p dataLen, the input
   *                  \p ctxData is too short to contain the fixed portion
   *                  of the Compression Capabilities Context.
   *  - -n  \b ERROR: Short buffer; based on \p dataLen, the input
   *                  \p ctxData is too short to contain the list of
   *                  Compression Algorithms.  At least \c abs( n ) bytes
   *                  are required in order to unpack the message.
   *
   * \b Notes
   *  - This function does not check the count of entries in the
   *    \c #smb2_Compression_Caps::CompressionAlgorithmCount field.  This
   *    value should be greater than zero, though this is not called out
   *    in the specification.
   *  - This function does not convert the list of
   *    \c #smb2_Compression_Caps::CompressionAlgorithms to host byte order.
   *    \b See: \c smbget16array().
   *
   * @see \c smb2_parseNegotiateCtx()
   */
  {
  int msgSize;

  if( dataLen < 8 )
    return( -8 );

  cCaps->CompressionAlgorithmCount = smbget16( ctxData );
  msgSize = 8 + (int)(cCaps->CompressionAlgorithmCount * sizeof( uint16_t ));
  if( msgSize < dataLen )
    return( -msgSize );
  (void)memcpy( cCaps->Padding, &ctxData[2], 2 );
  cCaps->Flags = smbget32( &ctxData[4] );
  cCaps->CompressionAlgorithms = (uint16_t *)&(ctxData[8]);

  return( msgSize );
  } /* smb2_parseCompressionCaps */

int smb2_packCompressionCaps( smb2_Compression_Caps *const cCaps,
                              uint8_t               *const bufr,
                              uint32_t               const bSize )
  /** Pack a Compression Capabilities blob.
   *
   * @param[in]   cCaps   Pointer to a \c #smb2_Compression_Caps structure
   *                      containing the data to be marshalled.
   * @param[out]  bufr    Pointer to a buffer into which the Compression
   *                      Capabilities data will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On success, the number of bytes of \p bufr that were
   *            consumed.  On error, a negative value.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; based on \p bSize, the given \p bufr
   *                  is not large enough to store the composed message.
   *                  A minimum of \c abs( n ) bytes are required.
   *
   * @see \c smb2_packNegotiateCtx()
   * @see \c SMB_ALIGN8()
   */
  {
  uint8_t *p = bufr;
  int      msgSize;

  /* Sanity Checks. */
  assert( NULL != cCaps );
  assert( NULL != bufr );
  /* Verify the minimum buffer size. */
  msgSize = 8 + (int)(cCaps->CompressionAlgorithmCount * sizeof( uint16_t ));
  if( bSize < msgSize )
    return( -msgSize );

  /* Pack. */
  p = smbset16( p, cCaps->CompressionAlgorithmCount );
  p = smbcpmem( p, cCaps->Padding, 2 );
  p = smbset32( p, cCaps->Flags );
  (void)smbset16array( (uint16_t *)p,
                       cCaps->CompressionAlgorithms,
                       cCaps->CompressionAlgorithmCount );
  return( msgSize );
  } /* smb2_packCompressionCaps */


/* ================================ Das Ende ================================ */
