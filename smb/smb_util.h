#ifndef SMB_UTIL_H
#define SMB_UTIL_H
/* ========================================================================== **
 *                                 smb_util.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Common utilities for SMB message handling.
 *
 * Copyright (C) 2019 by Christopher R. Hertel
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//** Common utilities header file.
 * @file      smb_util.h
 * @author    Christopher R. Hertel
 * @date      3 Apr 2019
 * @version   \$Id: smb_util.h; 2020-08-15 23:18:38 -0500; crh$
 * @copyright Copyright (C) 2019 by Christopher R. Hertel
 * @brief     Types and utilities common to most project modules.
 */

#include <stdint.h>         /* Standard integer types.  */
#include <stdbool.h>        /* Standard boolean type.   */


/* -------------------------------------------------------------------------- **
 * Macros
 */

/**
 * @def   SMB_IS_ALIGNED8( OFFSET )
 * @brief Test whether the given \p OFFSET is 8-byte alligned.
 * @param[in] OFFSET  An unsigned integer value indicating an offset into
 *                    a message buffer.
 * @returns Boolean; true if the given \p OFFSET is 8-byte alligned,
 *          else false.
 * @details
 *  This macro performs a quick check to determine whether or not the given
 *  offset is a multiple of 8 bytes, and returns true if the offset is
 *  correctly alligned.
 * @hideinitializer
 */
#define SMB_IS_ALIGNED8( OFFSET ) (((OFFSET) & 7)?false:true)

/**
 * @def   SMB_ALIGN8( OFFSET )
 * @brief Calculate the next 8-byte aligned offset.
 * @param[in] OFFSET  An integer value indicating an offset into
 *                    a message buffer.
 * @returns Integer; the next multiple of 8 greater than or equal to the
 *          input value.
 * @details
 *  This macro is used to calculate the offset at which an 8-byte-aligned
 *  message sub-structure should be written.  Several SMB messages can have
 *  variable-length subsections which must begin on an 8-byte boundary
 *  within the wire-format buffer.  Example return values:
 *  - SMB_ALIGN8( 0 )  => 0
 *  - SMB_ALIGN8( 7 )  => 8
 *  - SMB_ALIGN8( 16 ) => 16
 *  - SMB_ALIGN8( 17 ) => 24
 * @hideinitializer
 */
#define SMB_ALIGN8( OFFSET ) (((int)(OFFSET) + 7) & ~7)

/**
 * @def   SMB_PAD8( OFFSET )
 * @brief Calculate padding needed to align data to an 8-byte byte boundary.
 * @param[in] OFFSET  An unsigned integer value indicating an offset into
 *                    a message buffer.
 * @returns An integer in the range 0..7.
 * @details
 *  This macro returns the minimum value to be added to \p OFFSET to produce
 *  a multiple of 8.  This represents the number of padding bytes required
 *  in order to start the next message subsection on an 8-byte boundary.
 *  Example return values:
 *  - SMB_PAD8( 15 ) => 1
 *  - SMB_PAD8( 16 ) => 0
 *  - SMB_PAD8( 17 ) => 7
 * @hideinitializer
 */
#define SMB_PAD8( OFFSET ) ((8 - ((OFFSET) & 7)) & 7)


/* ================================= La Fin ================================= */
#endif /* SMB_UTIL_H */
