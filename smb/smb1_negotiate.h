#ifndef SMB1_NEGOTIATE_H
#define SMB1_NEGOTIATE_H
/* ========================================================================== **
 *                              smb1_negotiate.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description:
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb1_negotiate.h; 2020-09-22 22:14:05 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb1_negotiate.h
 * @author  Christopher R. Hertel
 * @brief   SMB1 (CIFS) Protocol Negotation.
 * @date    24-Aug-2020
 * @version \$Id: smb1_negotiate.h; 2020-09-22 22:14:05 -0500; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  The SMB2 protocol negotiation can be initiated using an SMB1 Negotiate
 *  Request message.  This module provides a very simple, negotiate-only,
 *  mini SMB1 implementation for the sole purpose of supporting the
 *  establishment of an SMB2/SMB3 connection from SMB1 (aka. CIFS).
 *
 *  Server-Side
 *  -----------
 *
 *  When using an SMB1 NegProt request to negotiate an SMB2 dialect, there
 *  are four possible responses from the server:
 *
 *  1.  If the client sends and the server accepts the "2.???" dialect
 *      string, then the server should send an <b>SMB2 Negotiate Response</b>
 *      indicating that the 0x02FF dialect has been selected.  0x02FF
 *      indicates that further processing is required, and the client must
 *      send a subsequent <b>SMB2 Negotiate Request</b>.
 *  2.  If the client sends and the server accepts the "2.002" dialect
 *      string, then the server should send an <b>SMB2 Negotiate Response</b>
 *      with the 0x0202 dialect selected.  0x0202 indicates that the "2.002"
 *      dialect has been successfully negotiated.
 *
 *  In cases 1 and 2, the server should respond with an SMB2 (not SMB1)
 *  Negotiate Response message.
 *
 *  If the client did not send either SMB2 dialect string (i.e., the client
 *  does not support SMB2):
 *
 *  <ol start=3>
 *  <li>If the server supports any dialect of SMB1 then the server should
 *      complete SMB1 protocol negotiation.  This means that the server
 *      should either select one of the SMB1 dialects presented by the
 *      client, or return an index value of 0xFFFF to indicate that no
 *      suitable dialect was offered.
 *  <li>If the server does not support SMB1:
 *      If the client did not offer any acceptable SMB2 dialect strings,
 *      the server should drop the transport connection without sending a
 *      response.
 *  </ol>
 *
 *  The server will only return an SMB1_COM_NEGOTIATE response if it actually
 *  supports the legacy SMB1 protocol ("NT LM 0.12" dialect or earlier).
 *
 *  SMB2 servers built upon this toolkit should never send an SMB1 Negprot
 *  response.  If a pass-through to an SMB1 implementation is provided, that
 *  SMB1 implementation should complete SMB1 negotiation.
 *
 *  Client-Side
 *  -----------
 *
 *  There is no reason, under normal circumstances, that an SMB2/3 client
 *  would send an SMB1 Negotiate Request.  Only a client that acutally
 *  supports SMB1 would send an SMB1 Negotiate Request.  That being the case,
 *  a non-SMB1 client should also never receive an SMB1 Negotiate Response
 *  even from a legacy server.
 *
 *  Casting logic to the wind, this module does provide a function that will
 *  marshall an SMB1 Negotiate Request message.  It's useful for testing,
 *  but can otherwise be ignored.
 *
 * @see <a href="http://www.ubiqx.org/cifs/SMB.html">[IMPCIFS; 2]:
 *      Implementing CIFS; §2. SMB: The Server Message Block Protocol</a>
 * @see <a href="@msdocs/ms-cifs/25c8c3c9-58fc-4bb8-aa8f-0272dede84c5">[MS-CIFS;
 *      2.2.4.52.1]: \c SMB_COM_NEGOTIATE Request</a>
 * @see <a href="@msdocs/ms-cifs/a4229e1a-8a4e-489a-a2eb-11b7f360e60c">[MS-CIFS;
 *      2.2.4.52.2]: \c SMB_COM_NEGOTIATE Response</a>
 * @see <a href="@msdocs/ms-smb/9fef5150-5501-44a8-b5b5-0b20057187ac">[MS-SMB;
 *      2.2.4.5]: SMB_COM_NEGOTIATE</a>
 * @see <a href="@msdocs/ms-smb2/26646611-6a0f-4549-9c82-f9343e750a81">[MS-SMB2;
 *      3.3.5.3]: Receiving an SMB_COM_NEGOTIATE</a>
 * @see <a href="https://docs.microsoft.com/en-us/archive/blogs/josebda/the-deprecation-of-smb1-you-should-be-planning-to-get-rid-of-this-old-smb-dialect/">[SMBDEAD]:
 *      The Deprecation of SMB1 – You should be planning to get rid of this
 *      old SMB dialect</a> (2015)
 */

#include <stdbool.h>    /* Boolean type.  */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 */
/**
 * @def     SMB1_HEADER_SIZE
 * @brief   The fixed size of the wire-format SMB1 (CIFS) header.
 * @details The wire format of SMB1 header is a fixed 32 bytes in length.
 */
#define SMB1_HEADER_SIZE  32

/**
 * @def     SMB1_SIGNATURE_OFFSET
 * @brief   Offset of the SMB1 security signature.
 * @details Offset, relative to the start of the SMB1 Header, at which the
 *          security signature starts (offset 14).
 * @def     SMB1_SIGNATURE_LENGTH
 * @brief   The length, in bytes, of the SMB1 security signature field.
 */
#define SMB1_SIGNATURE_OFFSET   14
#define SMB1_SIGNATURE_LENGTH   8

/**
 * @def     SMB1_COM_NEGOTIATE
 * @brief   The command code for the SMB1 Negotiate (NegProt) command.
 * @details
 *  In [MS-CIFS], this command is referred to as \c SMB_COM_NEGOTIATE.
 *  This is correct and appropriate for a variety of reasons.  In this
 *  module, however, the prefix "SMB1" is used instead of "SMB" for
 *  added clarity and consistency.
 */
#define SMB1_COM_NEGOTIATE   0x72


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  smb1_Header
 * @brief   SMB1 message header attributes.
 * @details
 *  The SMB1 header is a fixed 32 bytes in length.  It has been around since
 *  the early 1980's and has seen its share of additions and minor
 *  modifications.
 */
typedef struct
  {
  uint8_t  ProtocolId[4]; /**< Protocol identification string.              */
  uint8_t  Command;       /**< SMB1 Command Code.                           */
  uint32_t Status;        /**< NTSTATUS error code.                         */
  uint8_t  Flags;         /**< A set of flags bits.                         */
  uint16_t Flags2;        /**< More flags bits.                             */
  uint8_t *Signature;     /**< Security Signature; zero filled pre-signing. */
  uint8_t  Reserved[2];   /**< Reserved-MB0.                                */
  uint16_t TID;           /**< Tree Id.                                     */
  uint32_t PID;           /**< Full 32 bits of the client PID.              */
  uint16_t UID;           /**< Server-assigned User Id.                     */
  uint16_t MID;           /**< Multiplex Id.                                */
  } smb1_Header;

/**
 * @struct  smb1_NegProtReq
 * @brief   Basic structure of an SMB1 NegProt Request message.
 * @details
 *  The payload of the SMB1 NegProt Request consists of the list of dialect
 *  strings.  Each dialect name is nul terminated and prefixed by a byte
 *  containing the value 0x02.  This formatting is original to the protocol.
 * @note
 *  This structure provides only the fields necessary for sending the list
 *  of dialects.  The \c WordCount field is not provided.  In an SMB1
 *  NegProt request, this field must always be zero.
 * @see <a href="http://ubiqx.org/cifs/SMB.html#SMB.4.3">[IMPCIFS; 2.4.3]</a>
 * @see <a href="http://ubiqx.org/cifs/SMB.html#SMB.6.2">[IMPCIFS; 2.6.2]</a>
 */
typedef struct
  {
  uint16_t  ByteCount;        /**< Size, in bytes, of the Dialects buffer.  */
  uint8_t  *Dialects;         /**< A byte array contaning dialect strings.  */
  } smb1_NegProtReq;

/**
 * @struct  smb1_NegProtReqParams
 * @brief   Minimal parameter set required for composing an #smb1_NegProtReq.
 * @details
 *  The fields of this structure represent the minimal set of parameters
 *  needed in order to compose a baseline SMB1 Negotiate Parameters request
 *  message.  Fixed default values can be used for the rest.
 */
typedef struct
  {
  uint32_t  PID;        /**< 32-bit Process Id; unique per connection.        */
  uint16_t  MID;        /**< 16-bit Multiplex Id; unique per PID/connection.  */
  bool      v0202;      /**< True to send the "2.002" dialect string.         */
  bool      vWild;      /**< True to send the "2.???" dialect string.         */
  } smb1_NegProtReqParams;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb1_mkNegProtReq( smb1_NegProtReqParams *const negReqParams,
                       uint8_t               *const bufr,
                       uint32_t               const bSize );

int smb1_deriveDialects( uint8_t               *const msg,
                         size_t                 const msgLen,
                         smb1_NegProtReqParams *const negReqParams );


/* ============================ smb1_negotiate.h ============================ */
#endif /* SMB1_NEGOTIATE_H */
