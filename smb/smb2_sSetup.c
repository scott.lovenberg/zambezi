/* ========================================================================== **
 *                                smb2_sSetup.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Session Setup messages.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_sSetup.c; 2020-09-16 14:17:41 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>       /* For debugging; see assert(3).  */

#include "smb_endian.h"   /* SMB byte order conversions.    */
#include "smb2_sSetup.h"  /* Module Header.                 */


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb2_parseSSetupReq( uint16_t       const dialect,
                         uint8_t        *const msg,
                         size_t          const msgLen,
                         smb2_sSetupReq *const ssReq )
  /** Parse an SMB2 Session Setup Request message.
   *
   * @param[in]   dialect The dialect used when parsing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   msg     The source buffer, containing the received message
   *                      in wire format.  This must indicate the start of
   *                      the fixed-length Session Setup Request block, which
   *                      immediately follows the SMB2 header.
   * @param[in]   msgLen  The length of the message, excluding the header,
   *                      in bytes.
   * @param[out]  ssReq   A pointer to an \c #smb2_sSetupReq structure.
   *                      This structure should be zeroed by the caller
   *                      before it is passed in.  If all goes well, it will
   *                      be filled with goodies upon return.
   *
   * @returns   On success, a positive value indicating the number of bytes
   *            of \p msg that have been parsed.\n
   *            On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value.
   *  - -2  \b ERROR: The \c SecurityBufferOffset incorrectly indicates a
   *                  location within the fixed-length portion of the
   *                  message.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of an SMB2/3
   *                  Session Setup Request.  A minimum of
   *                  \c #SMB2_SSETUP_REQ_SIZE bytes are required.
   *
   * \b Notes
   *  - If the Security Buffer is not contained within \p msg, the
   *    \c SecurityBuffer field will be NULL and the return value will be
   *    \c #SMB2_SSETUP_REQ_SIZE.
   *  - If the Security Buffer \e is contained within \p msg, then the
   *    \c SecurityBuffer field will point to the location in \p msg at
   *    which the buffer begins, and the return value will include the
   *    length of the buffer.
   */
  {
  int tmpLen;

  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != ssReq );
  /* Check buffer size. */
  if( msgLen < SMB2_SSETUP_REQ_SIZE )
    return( -SMB2_SSETUP_REQ_SIZE );
  /* Test StructureSize. */
  if( (ssReq->StructureSize = smbget16( msg )) != SMB2_SSETUP_REQ_STRUCT_SIZE )
    return( -1 );

  /* Unpack. */
  ssReq->Flags        = msg[2];
  ssReq->SecurityMode = msg[3];
  ssReq->Capabilities = smbget32( &msg[4] );
  (void)memcpy(  ssReq->Reserved, &msg[8], 4 );
  ssReq->SecurityBufferOffset = smbget16( &msg[12] );
  ssReq->SecurityBufferLength = smbget16( &msg[14] );
  ssReq->PreviousSessionId    = smbget64( &msg[16] );

  /* Another quick sanity check. */
  if( ssReq->SecurityBufferOffset < (SMB2_HEADER_SIZE + SMB2_SSETUP_REQ_SIZE) )
    return( -2 );

  /* Try to find the Security Buffer. */
  tmpLen = ssReq->SecurityBufferOffset
         + ssReq->SecurityBufferLength
         - SMB2_HEADER_SIZE;
  if( tmpLen < msgLen )
    {
    ssReq->SecurityBuffer = NULL;
    return( SMB2_SSETUP_REQ_SIZE );
    }
  /* We have an intact Security Buffer. */
  ssReq->SecurityBuffer = &msg[ssReq->SecurityBufferOffset - SMB2_HEADER_SIZE];
  return( tmpLen );
  } /* smb2_parseSSetupReq */

int smb2_packSSetupReq( uint16_t        const dialect,
                        smb2_sSetupReq *const ssReq,
                        uint8_t        *const bufr,
                        uint32_t        const bSize )
  /** Compose an SMB2 Session Setup Request message.
   *
   * @param[in]   dialect The dialect used when packing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   ssReq   A pointer to an \c #smb2_sSetupReq structure
   *                      that contains the data needed to create the
   *                      wire-format request message.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      Session Setup Request message will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On success, a positive number, indicating the number of bytes
   *            written to \p bufr.  On error, a negative value is returned.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; more than \p bSize bytes are required
   *                  for the composed message.  At least \c |n| bytes are
   *                  required.
   *
   * \b Notes
   *  - The return value will be \c ±SMB2_SSETUP_REQ_SIZE.  If positive, it
   *    indicates the number of bytes written to \p bufr.  If negative, it
   *    indicates that \p bufr (based on \p bSize) is too small.
   *  - The \c SecurityBuffer field is ignored.  The contents of the security
   *    buffer are not copied into \p bufr.
   *  - The \c #smb2_sSetupReq.Flags field is valid for SMB3 (and above)
   *    dialects only.  For SMB2, this field must be zero.  This requirement
   *    is not enforced by this function.
   */
  {
  uint8_t *p = bufr;

  /* Sanity Checks. */
  assert( NULL != ssReq );
  assert( NULL != bufr );
  if( bSize < SMB2_SSETUP_REQ_SIZE )
    return( -SMB2_SSETUP_REQ_SIZE );

  /* Pack */
  p = smbset16( p, ssReq->StructureSize );
  p = smbset8(  p, ssReq->Flags );
  p = smbset8(  p, ssReq->SecurityMode );
  p = smbset32( p, ssReq->Capabilities );
  p = smbcpmem( p, ssReq->Reserved, 4 );
  p = smbset16( p, ssReq->SecurityBufferOffset );
  p = smbset16( p, ssReq->SecurityBufferLength );
  p = smbset64( p, ssReq->PreviousSessionId );

  return( SMB2_SSETUP_REQ_SIZE );
  } /* smb2_packSSetupReq */

int smb2_parseSSetupResp( uint16_t         const dialect,
                          uint8_t         *const msg,
                          size_t           const msgLen,
                          smb2_sSetupResp *const ssResp )
  /** Parse an SMB2/3 Session Setup Request message.
   *
   * @param[in]   dialect The dialect used when parsing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   msg     The source buffer, containing the received
   *                      message in wire format.  This must indicate the
   *                      start of the Session Setup Response block, which
   *                      immediately follows the SMB2 header.
   * @param[in]   msgLen  The length of the message, excluding the header,
   *                      in bytes.
   * @param[out]  ssResp  A pointer to an \c #smb2_sSetupResp structure into
   *                      which the message will be parsed.  As usual, this
   *                      structure should be zeroed by the caller.
   *
   * @returns   On success, a positive value indicating the number of bytes
   *            of \p msg that have been parsed.\n
   *            On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value.
   *  - -2  \b ERROR: The \c SecurityBufferOffset incorrectly points to a
   *                  location within the fixed-length portion of the
   *                  message.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of an SMB2/3
   *                  Session Setup Response.  At least
   *                  \c #SMB2_SSETUP_RESP_SIZE bytes are required.
   *
   * \b Notes
   *  - If the Security Buffer is not contained within \p msg, the
   *    \c SecurityBuffer field will be NULL and the return value will be
   *    \c #SMB2_SSETUP_RESP_SIZE.
   *  - If the Security Buffer \e is contained within \p msg, then the
   *    \c SecurityBuffer field will point to the location in \p msg at
   *    which the buffer begins, and the return value will include the
   *    length of the buffer.
   */
  {
  int tmpLen;

  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != ssResp );
  /* Check buffer size. */
  if( msgLen < SMB2_SSETUP_RESP_SIZE )
    return( -SMB2_SSETUP_RESP_SIZE );

  /* Test StructureSize. */
  ssResp->StructureSize = smbget16( msg );
  if( ssResp->StructureSize != SMB2_SSETUP_RESP_STRUCT_SIZE )
    return( -1 );

  /* Unpack. */
  ssResp->SessionFlags         = smbget16( &msg[2] );
  ssResp->SecurityBufferOffset = smbget16( &msg[4] );
  ssResp->SecurityBufferLength = smbget16( &msg[6] );

  /* Another quick sanity check. */
  if( ssResp->SecurityBufferOffset < (SMB2_HEADER_SIZE+SMB2_SSETUP_RESP_SIZE) )
    return( -2 );

  /* Try to find the Security Buffer. */
  tmpLen = ssResp->SecurityBufferOffset
         + ssResp->SecurityBufferLength
         - SMB2_HEADER_SIZE;
  if( tmpLen < msgLen )
    {
    ssResp->SecurityBuffer = NULL;
    return( SMB2_SSETUP_RESP_SIZE );
    }
  /* We have an intact Security Buffer. */
  ssResp->SecurityBuffer = &msg[ssResp->SecurityBufferOffset-SMB2_HEADER_SIZE];
  return( tmpLen );
  } /* smb2_parseSSetupResp */

int smb2_packSSetupResp( uint16_t         const dialect,
                         smb2_sSetupResp *const ssResp,
                         uint8_t         *const bufr,
                         uint32_t         const bSize )
  /** Compose an SMB2 Session Setup Response message.
   *
   * @param[in]   dialect The dialect used when packing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   ssResp  A pointer to an \c #smb2_sSetupResp structure
   *                      that contains the data needed to create the
   *                      wire-format request message.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      Session Setup Response message will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   On success, a positive number, indicating the number of bytes
   *            written to \p bufr.  On error, a negative value is returned.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; more than \p bSize bytes are required
   *                  for the composed message.  At least \c |n| bytes are
   *                  required.
   *
   * \b Notes
   *  - The return value will be \c ±SMB2_SSETUP_RESP_SIZE.  If positive, it
   *    indicates the number of bytes written to \p bufr.  If negative, it
   *    indicates that \p bufr (based on \p bSize) is too small.
   *  - The \c SecurityBuffer field is ignored.  The contents of the security
   *    buffer are not copied into \p bufr.
   */
  {
  uint8_t *p = bufr;

  /* Sanity Checks. */
  assert( NULL != ssResp );
  assert( NULL != bufr );
  if( bSize < SMB2_SSETUP_RESP_SIZE )
    return( -SMB2_SSETUP_RESP_SIZE );

  /* Pack */
  p = smbset16( p, ssResp->StructureSize );
  p = smbset16( p, ssResp->SessionFlags );
  p = smbset16( p, ssResp->SecurityBufferOffset );
  p = smbset16( p, ssResp->SecurityBufferLength );

  return( SMB2_SSETUP_RESP_SIZE );
  } /* smb2_packSSetupResp */


/* ================================= Fertig ================================= */
