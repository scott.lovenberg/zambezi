/* ========================================================================== **
 *                                 smb2_lock.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Lock exchanges.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_lock.c; 2020-09-25 17:00:44 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>       /* For debugging; see assert(3).            */

#include "smb_endian.h"   /* Convert between host and SMB byte order. */
#include "smb2_lock.h"    /* Module Header.                           */


/* -------------------------------------------------------------------------- **
 * Functions
 */
int smb2_parseLockReq( uint16_t      const dialect,
                       uint8_t      *const msg,
                       size_t        const msgLen,
                       smb2_LockReq *const lockReq )
  /** Parse an SMB2 Lock Request message.
   *
   * @param[in]   dialect The dialect used when parsing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   msg     A (constant) pointer to an array of bytes, which
   *                      \b MUST contain an SMB Lock Request message.
   * @param[in]   msgLen  The size, in bytes, of \p msg.
   * @param[out]  lockReq A (constant) pointer to an #smb2_LockReq
   *                      structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  On success, the
   *            return value is the number of bytes of \p msg that were
   *            consumed by parsing (which should always be 24).
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of an SMB2/3
   *                  Lock Request.  A minimum of \c #SMB2_LOCK_REQ_SIZE
   *                  bytes are required.
   *
   * \b Notes
   *  - The \c LockSequenceNumber and \c LockSequenceIndex fields of the
   *    \c #smb2_LockReq are nibblefields of 4 and 28 bits respectively.
   *    This function reads these fields as a single 32-bit unsigned
   *    integer value, and then separates out the subfield values.
   */
  {
  uint32_t tmp32;

  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != lockReq );
  if( msgLen < SMB2_LOCK_REQ_SIZE )                   /* Check buffer size.   */
    return( -SMB2_LOCK_REQ_SIZE );

  /* Read and test the StructureSize. */
  if( (lockReq->StructureSize = smbget16( msg )) != SMB2_LOCK_REQ_STRUCT_SIZE )
    return( -1 );

  /* Parse the remaining fields.  */
  lockReq->LockCount = smbget16( &msg[2] );
  tmp32              = smbget32( &msg[4] );
  lockReq->FileId    = &msg[8];
  lockReq->Locks  = ( msgLen >= SMB2_LOCK_REQ_STRUCT_SIZE )
                  ? &msg[SMB2_LOCK_REQ_SIZE]
                  : NULL;

  /* Carve out the bit fields. */
  lockReq->LockSequenceNumber = (uint8_t)((tmp32 >> 28) & 0x0F ) ;
  lockReq->LockSequenceIndex  = (tmp32 & 0x0FFFFFFF);

  return( SMB2_LOCK_REQ_SIZE );
  } /* smb2_parseLockReq */

int smb2_packLockReq( uint16_t      const dialect,
                      smb2_LockReq *const lockReq,
                      uint8_t      *const bufr,
                      uint32_t      const bSize )
  /** Pack an SMB2 Lock Request message.
   *
   * @param[in]   dialect The dialect used when packing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   lockReq A pointer to an \c #smb2_LockReq structure
   *                      containing the values to be used to compose the
   *                      wire format message.
   * @param[out]  bufr    Pointer to a data buffer into which the wire
   *                      format of the message will be written.
   * @param[in]   bSize   Number of bytes available in \p bufr.
   *
   * @returns   On error, a negative number is returned.  On success, the
   *            return value is the number of bytes of \p bufr that were
   *            used to store the message (always \c #SMB2_LOCK_REQ_BUFR_SIZE
   *            bytes).
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; based on \p bSize, the output buffer,
   *                  \p bufr, is not at least \c SMB2_LOCK_REQ_BUFR_SIZE
   *                  bytes in length.
   *
   * \b Notes
   *  - This function <em>does not</em> copy the \c #smb2_LockReq::FileId
   *    field into the output buffer.  Only the first four fields of
   *    \c lockReq are written into \p bufr.  The assumption is made that
   *    scatter/gather techniques will be used to send messages and that
   *    it will be more efficient to send the \c FileId directly than to
   *    copy it into another buffer before sending.  As a result, the
   *    return value is \c ±SMB2_LOCK_REQ_BUFR_SIZE.
   *  - The LockSequenceNumber and LockSequenceIndex fields are unsigned
   *    integer bit fields of non-standard length.  These two values are
   *    combined into a single \c uint32_t value before being written to
   *    the output buffer.  This ensures correct wire-format encoding.
   */
  {
  uint8_t *p = bufr;
  uint32_t tmp32;

  /* Sanity Checks. */
  assert( NULL != lockReq );
  assert( NULL != bufr );
  /* Verify the minimum buffer size. */
  if( bSize < SMB2_LOCK_REQ_BUFR_SIZE )
    return( -SMB2_LOCK_REQ_BUFR_SIZE );

  /* Calculize the LockSequence field value.  */
  tmp32  = ((uint32_t)(lockReq->LockSequenceNumber & 0x0F) << 28);
  tmp32 |= (lockReq->LockSequenceIndex & 0x0FFFFFFF);
  /* Pack the buffer. */
  p = smbset16( p, lockReq->StructureSize );
  p = smbset16( p, lockReq->LockCount );
  p = smbset32( p, tmp32 );
  return( SMB2_LOCK_REQ_BUFR_SIZE );
  } /* smb2_packLockReq */

int smb2_parseLockElement( uint16_t          const dialect,
                           uint8_t          *const msg,
                           size_t            const msgLen,
                           smb2_LockElement *const lockElm )
  /** Unpack a Lock Element structure from an incoming buffer.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Lock Element in wire
   *                        format.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  lockElm   A (constant) pointer to an #smb2_LockElement
   *                        structure into which the Lock Element will be
   *                        parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be \c #SMB2_LOCK_ELEMENT_SIZE.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg
   *                  is too small to contain an SMB2/3 Lock Element.  A
   *                  minimum of \c #SMB2_LOCK_ELEMENT_SIZE bytes are
   *                  required.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != lockElm );
  if( msgLen < SMB2_LOCK_ELEMENT_SIZE )
    return( -SMB2_LOCK_ELEMENT_SIZE );

  /* Do the parse thing.  */
  lockElm->Offset = smbget64( msg );
  lockElm->Length = smbget64( &msg[8] );
  lockElm->Flags  = smbget32( &msg[16] );
  (void)memcpy( lockElm->Reserved, &msg[20], 4 );

  return( SMB2_LOCK_ELEMENT_SIZE );
  } /* smb2_parseLockElement */

int smb2_packLockElement( uint16_t          const dialect,
                          smb2_LockElement *const lockElm,
                          uint8_t          *const bufr,
                          uint32_t          const bSize )
  /** Pack a Lock Element strucure into an outgoing buffer.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        There are no dialect-specific variations to
   *                        this structure, so the \p dialect value is
   *                        ignored.  This parameter is included for
   *                        interface consistency.
   * @param[in]   lockElm   Either NULL, in which case appropriate
   *                        default values are used, or a pointer to an
   *                        \c #smb2_LockElement structure containing the
   *                        values to be used to compose the wire format
   *                        message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the Lock Element will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   On error, a negative number is returned.  On success, the
   *            return value is the number of bytes of \p bufr that were
   *            to write the wire format of the Lock Element, which should
   *            always be \c #SMB2_LOCK_ELEMENT_SIZE.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; based on \p bSize, the output \p bufr
   *                  is too small to contain an SMB2/3 Lock Element.  A
   *                  minimum of \c #SMB2_LOCK_ELEMENT_SIZE bytes are
   *                  required.
   */
  {
  uint8_t *p = bufr;

  /* Sanity Checks. */
  assert( NULL != lockElm );
  assert( NULL != bufr );
  /* Verify the minimum buffer size. */
  if( bSize < SMB2_LOCK_ELEMENT_SIZE )
    return( -SMB2_LOCK_ELEMENT_SIZE );

  /* Pack the Element. */
  p = smbset64( p, lockElm->Offset );
  p = smbset64( p, lockElm->Length );
  p = smbset32( p, lockElm->Flags );
  p = smbcpmem( p, lockElm->Reserved, 4 );

  return( SMB2_LOCK_ELEMENT_SIZE );
  } /* smb2_packLockElement */

int smb2_parseLockResp( uint16_t       const dialect,
                        uint8_t       *const msg,
                        size_t         const msgLen,
                        smb2_LockResp *const lockResp )
  /** Parse an SMB2 Lock Response message.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        There are no dialect-specific variations to
   *                        this message type, so the \p dialect value is
   *                        ignored.  This parameter is included for
   *                        interface consistency.
   * @param[in]   msg       A (constant) pointer to an array of bytes,
   *                        which \b MUST contain an SMB Lock Response
   *                        message, at least 4 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  lockResp  A (constant) pointer to an #smb2_LockResp
   *                        structure into which the message will be
   *                        parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 4.
   *
   * \b Errors
   *  - \b See #smb2_parseBaseMsg()
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_parseBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_parseBaseMsg( dialect, msg, msgLen, lockResp ) );
  } /* smb2_parseLockResp */

int smb2_packLockResp( uint16_t       const dialect,
                       smb2_LockResp *const lockResp,
                       uint8_t       *const bufr,
                       uint32_t       const bSize )
  /** Pack an SMB2 Lock Response message.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        There are no dialect-specific variations to
   *                        this message type, so the \p dialect value is
   *                        ignored.  This parameter is included for
   *                        interface consistency.
   * @param[in]   lockResp  Either NULL, in which case appropriate
   *                        default values are used, or a pointer to an
   *                        \c #smb2_LockResp structure containing the
   *                        values to be used to compose the wire format
   *                        message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 4).
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_packBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *  - Passing NULL for \p lockResp is recommended.  The body of the
   *    Lock Response message is pre-defined.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_packBaseMsg( dialect, lockResp, bufr, bSize ) );
  } /* smb2_packLockResp */


/* =============================== Byte Ranger ============================== */
