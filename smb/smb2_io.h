#ifndef SMB2_IO_H
#define SMB2_IO_H
/* ========================================================================== **
 *                                  smb2_io.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Fundamental I/O operations: read, write, close.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_io.h; 2020-08-15 23:18:38 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_io.h
 * @author  Christopher R. Hertel
 * @brief   Implements SMB2 Read, Write, and Flush.
 * @date    5 May 2020
 * @version \$Id: smb2_io.h; 2020-08-15 23:18:38 -0500; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  Basic cornerstones of I/O.
 *
 *  The SMB2 read, write, and and flush operations provide basic I/O
 *  functionality, as you might expect.
 *  - The SMB2 Read Request provides a file handle, offset, and read length,
 *    and (when everything works correctly) the Read Response returns the
 *    requested data.
 *  - The SMB2 Write Request provides a file handle, offset, write length,
 *    and data to be written.  The response indicates success or failure.
 *  - The Flush operation is meant to ensure that previously written data is
 *    in a quiesced and resiliant state.
 *
 *  In SMB2, however, the Read and Write operations provide support for a
 *  handful of additional features.
 *  - <b>Compression and unbuffered I/O (Flags fields)</b>\n
 *    \c #smb2_ReadReqFlags and \c #smb2_WriteReqFlags allow the client's
 *    calling process to request that the server bypass buffering and data
 *    caching and access the underlying object store directly.  On Read, the
 *    client may also request that the data be compressed before it is
 *    transferred.
 *  - <b>Minimum Count</b>\n
 *    Read operations may succeed and still not return the number of bytes
 *    requested.  This can happen, for instance, when reading past the
 *    end-of-file.  The \c  #smb2_ReadReq.MinimumCount field indicates the
 *    minimum number of bytes that the client requires in order for the read
 *    to be considered successful.
 *  - <b>Read Caching Hints</b>\n
 *    The \c #smb2_ReadReq.RemainingBytes field provides a mechanism by
 *    which the client can let the server know how many more bytes it
 *    intends to read in the current sequence.  The server \e may take this
 *    as a clue to perform read-ahead caching.
 *  - <b>Data Offset Optimization</b>\n
 *    The #smb2_ReadReq.Padding field allows the client to indicate how much
 *    padding (if any) it wants the server to place between the body of the
 *    Read Response message and the return buffer.  The server is free to
 *    ignore the request.
 *  - <b>Support for RDMA transfers.</b>\n
 *    With SMBv3, several new fields were added to read and write messages
 *    to support RDMA data transfer.
 *
 * @see <a
 *  href="@msdocs/ms-smb2/5606ad47-5ee0-437a-817e-70c366052962">[MS-SMB2]:
 *  Server Message Block (SMB) Protocol Versions 2 and 3</a>
 * @see <a
 *  href="@msdocs/ms-smbd/1ca5f4ae-e5b1-493d-b87d-f4464325e6e3">[MS-SMBD]:
 *  SMB2 Remote Direct Memory Access (RDMA) Transport Protocol</a>
 *
 * @todo
 *  - We will need a module to support Parsing/Packing of the RDMA Channel
 *    Info blocks, as well as any other RDMA-specific messaging.
 *  - The functions could use better per-dialect filtering.  Needs review.
 */

#include "smb2_baseMsg.h"    /* The Flush Response is a Base Message.  */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def     SMB2_READ_REQ_SIZE
 * @brief   The byte length of the fixed portion of an SMB2 Read Request.
 * @details The size, in bytes, of the fixed-length portion of an SMB2
 *          Read Request message.
 *
 * @def     SMB2_READ_REQ_STRUCT_SIZE
 * @brief   SMB2 Read Request StructureSize.
 * @details The structure size, in bytes, of the SMB2 Read Request message.
 *          The #smb2_ReadReq.StructureSize field MUST be set to this value.
 *
 * @def     SMB2_READ_RESP_SIZE
 * @brief   The byte length of the fixed portion of an SMB2 Read Response.
 * @details The size, in bytes, of the fixed-length portion of an SMB2
 *          Read Response message.
 *
 * @def     SMB2_READ_RESP_STRUCT_SIZE
 * @brief   SMB2 Read Response StructureSize.
 * @details The structure size, in bytes, of the SMB2 Read Response message.
 *          The #smb2_ReadResp.StructureSize field MUST be set to this
 *          value.
 *
 * @def     SMB2_WRITE_REQ_SIZE
 * @brief   The byte length of the fixed portion of an SMB2 Write Request.
 * @details The size, in bytes, of the fixed-length portion of an SMB2
 *          Write Request message.
 *
 * @def     SMB2_WRITE_REQ_STRUCT_SIZE
 * @brief   SMB2 Write Request StructureSize.
 * @details The structure size, in bytes, of the SMB2 Write Request message.
 *          The #smb2_WriteReq.StructureSize field MUST be set to this
 *          value.
 *
 * @def     SMB2_WRITE_RESP_SIZE
 * @brief   The byte length of the fixed portion of an SMB2 Write Response.
 * @details The size, in bytes, of the fixed length portion of an SMB2
 *          Write Response message.
 *
 * @def     SMB2_WRITE_RESP_STRUCT_SIZE
 * @brief   SMB2 Write Response StructureSize.
 * @details The structure size, in bytes, of the SMB2 Write Response message.
 *          The #smb2_WriteResp.StructureSize field MUST be set to this
 *          value.
 *
 * @def     SMB2_FLUSH_REQ_SIZE
 * @brief   The byte length of the fixed portion of an SMB2 Flush Request.
 * @details The size, in bytes, of the fixed length portion of an SMB2
 *          Flush Request message.
 *
 * @def     SMB2_FLUSH_REQ_STRUCT_SIZE
 * @brief   SMB2 Flush Request StructureSize.
 * @details The structure size, in bytes, of the SMB2 Flush Request message.
 *          The #smb2_FlushReq.StructureSize field MUST be set to this
 *          value.
 *
 * @def     SMB2_FLUSH_RESP_SIZE
 * @brief   The byte length of the fixed portion of an SMB2 Flush Response.
 * @details The size, in bytes, of the fixed length portion of an SMB2
 *          Flush Response message.
 *
 * @def     SMB2_FLUSH_RESP_STRUCT_SIZE
 * @brief   SMB2 Flush Response StructureSize.
 * @details The structure size, in bytes, of the SMB2 Flush Response message.
 *          The #smb2_FlushResp.StructureSize field MUST be set to this
 *          value.
 */
#define SMB2_READ_REQ_SIZE          48
#define SMB2_READ_REQ_STRUCT_SIZE   49

#define SMB2_READ_RESP_SIZE         16
#define SMB2_READ_RESP_STRUCT_SIZE  17

#define SMB2_WRITE_REQ_SIZE         48
#define SMB2_WRITE_REQ_STRUCT_SIZE  49

#define SMB2_WRITE_RESP_SIZE        16
#define SMB2_WRITE_RESP_STRUCT_SIZE 17

#define SMB2_FLUSH_REQ_SIZE         24
#define SMB2_FLUSH_REQ_STRUCT_SIZE  SMB2_FLUSH_REQ_SIZE

#define SMB2_FLUSH_RESP_SIZE        SMB2_BASEMSG_SIZE
#define SMB2_FLUSH_RESP_STRUCT_SIZE SMB2_BASEMSG_STRUCT_SIZE


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum smb2_ReadReqFlags
 *      Read operation modifier flags.  Multiple flags may be set.
 * @var SMB2_READFLAG_NONE
 *      No flags.  Can be used to zero out the \c #smb2_ReadReq.Flags field.
 * @var SMB2_READFLAG_READ_UNBUFFERED
 *      Requests that the data be read directly from the underlying storage,
 *      pipe, or device.
 * @var SMB2_READFLAG_REQUEST_COMPRESSED
 *      Requests that the server compress the read response data when
 *      replying to the request.  This flag is valid for SMBv3.1.1 and above
 *      only.  Not all v3.1.1 servers support read compression.
 */
typedef enum
  {
  SMB2_READFLAG_NONE                = 0x00,   /* No flags.                */
  SMB2_READFLAG_READ_UNBUFFERED     = 0x01,   /* Request unbuffered read. */
  SMB2_READFLAG_REQUEST_COMPRESSED  = 0x02    /* Request compression.     */
  } smb2_ReadReqFlags;

/**
 * @enum smb2_WriteReqFlags
 *      Write operation modifier flags.  Multiple flags may be set.
 * @var SMB2_WRITEFLAG_NONE
 *      No flags.  Can be used to zero out the \c #smb2_WriteReq.Flags
 *      field.
 * @var SMB2_WRITEFLAG_WRITE_THROUGH
 *      Requests that the server perform write-through, ensuring that data
 *      is written to the underlying object store.  This flag is not valid
 *      for SMBv2.0.2.
 * @var SMB2_WRITEFLAG_WRITE_UNBUFFERED
 *      Bypass file buffering entirely on write.  This flag is supported
 *      only on SMBv3.0.2 and above.
 */
typedef enum
  {
  SMB2_WRITEFLAG_NONE             = 0x00,   /* No flags.                  */
  SMB2_WRITEFLAG_WRITE_THROUGH    = 0x01,   /* Request write-through.     */
  SMB2_WRITEFLAG_WRITE_UNBUFFERED = 0x02    /* Request unbuffered write.  */
  } smb2_WriteReqFlags;

/**
 * @enum smb2_ChannelFlags
 *    RDMA Channel flags.  These are only valid for the SMB 3.0.0 and
 *    above.  For the SMBv2 dialect family, the #smb2_ReadReq.Channel
 *    and #smb2_WriteReq:Channel fields are reserved and must be zero.
 *    For the SMB3 dialect family, only one of the options may be set.
 *
 * @see <a href="@msdocs/ms-smbd/bee890cb-48f0-42a3-ba62-f1a3a19b0edc">[MS-SMBD;
 *         2.2.3.1]: Buffer Descriptor V1 Structure</a>
 *
 * @var SMB2_CHANNEL_NONE
 *      There is no channel information in the request.  (Zero; no flags
 *      set.)  The \c [Read|Write]ChannelInfoOffset and
 *      \c [Read|Write]ChannelInfoLength fields should also be zero, and
 *      must be ignored by the server.
 * @var SMB2_CHANNEL_RDMA_V1
 *      The \c [Read|Write]ChannelInfoOffset and
 *      \c [Read|Write]ChannelInfoLength fields indicate the location and
 *      size of one or more \c SMB_DIRECT_BUFFER_DESCRIPTOR_V1 structures
 *      that are present in an appended RDMA channel information block.
 * @var SMB2_CHANNEL_RDMA_V1_INVALIDATE
 *      This flag is valid for SMBv3.0.2 and above.\n
 *      The \c [Read|Write]ChannelInfoOffset and
 *      \c [Read|Write]ChannelInfoLength fields indicate the location and
 *      size of one or more \c SMB_DIRECT_BUFFER_DESCRIPTOR_V1 structures
 *      that are present in an appended RDMA channel information block.  The
 *      server is requested to perform remote invalidation when replying to
 *      the request.
 * @see <a href="@msdocs/ms-smbd/73412f19-a712-493b-b89e-c678e21bf91f">[MS-SMBD;
 *         3.1.4.2]: Send Message</a>
 */
typedef enum
  {
  SMB2_CHANNEL_NONE               = 0x00000000,
  SMB2_CHANNEL_RDMA_V1            = 0x00000001,
  SMB2_CHANNEL_RDMA_V1_INVALIDATE = 0x00000002
  } smb2_ChannelFlags;


/* -------------------------------------------------------------------------- **
 * Typedefs
 *//**
 * @struct  smb2_ReadReq
 * @brief   SMB2 Read Request message structure.
 * @details
 *  SMB2 Read Request structure with added flag fields, features, and
 *  RDMA support.
 *
 * \b Pointers
 *  - \b \c #FileId - Points to a 16-byte array which contains the
 *                    \c #FileId of the open file from which to read.
 *  - \b \c #Buffer - If RDMA is in use for this read request, \c #Buffer
 *                    should point to a Channel Info block.  Otherwise, NULL.
 *
 * @var smb2_ReadReq::Padding
 *      A polite request, sent to the server, to indicate the amount of
 *      padding the client would like the server to provide between the
 *      fixed portion of the Read Response and the returned data.  The
 *      padding request is given as an offset relative to the start of the
 *      #smb2_Header.  The server is free to ignore this request.
 * @var smb2_ReadReq::Flags
 *      Flags to request buffer bypass and compressed data transfer.
 *      The available flags are defined in \c #smb2_ReadReqFlags.
 * @var smb2_ReadReq::Channel
 *      Must be assigned one (and only one) of the values provided by
 *      \c #smb2_ChannelFlags.
 * @var smb2_ReadReq::ReadChannelInfoOffset
 *      If the dialect is in the SMB3 family and \c #Channel is not
 *      \c SMB2_CHANNEL_NONE, the \c #ReadChannelInfoOffset field will
 *      indicate the location of an RDMA Channel Info Block within the
 *      request \c #Buffer.
 * @var smb2_ReadReq::ReadChannelInfoLength
 *      If the dialect is in the SMB3 family and \c #Channel is not
 *      \c #SMB2_CHANNEL_NONE, the \c #ReadChannelInfoLength field will
 *      indicate the length, in bytes, of the Channel Info Block.
 * @var smb2_ReadReq::Buffer
 *      If there is an RDMA Channel Info Block, it will be contained within
 *      the #Buffer.
 */
typedef struct
  {
  uint16_t  StructureSize;          /**< Structure size, fixed at 49.   */
  uint8_t   Padding;                /**< Reply buffer offset request.   */
  uint8_t   Flags;                  /**< See #smb2_ReadReqFlags.        */
  uint32_t  Length;                 /**< The requested read length.     */
  uint64_t  Offset;                 /**< Location from which to read.   */
  uint8_t  *FileId;                 /**< Pointer to the 16-byte FileId. */
  uint32_t  MinimumCount;           /**< Required minumum read length.  */
  uint32_t  Channel;                /**< See #smb2_ChannelFlags.        */
  uint32_t  RemainingBytes;         /**< Caching hint for the server.   */
  uint16_t  ReadChannelInfoOffset;  /**< Channel Info block location.   */
  uint16_t  ReadChannelInfoLength;  /**< Channel Info block size.       */
  uint8_t  *Buffer;                 /**< Channel Info block pointer.    */
  } smb2_ReadReq;

/**
 * @struct  smb2_ReadResp
 * @brief   SMB2 Read Response message structure.
 * @details
 *  SMB2 Read Response structure.  Used, of course, to return the data
 *  requested by a Read Request.
 *
 * \b Pointers
 *  - \b \c #Buffer - Pointer to the returned data, unless the data was
 *                    returned via RDMA.
 */
typedef struct
  {
  uint16_t  StructureSize;          /**< Structure size, fixed at 17.       */
  uint8_t   DataOffset;             /**< Returned data message offset.      */
  uint8_t   Reserved1[1];           /**< Reserved-MB0.                      */
  uint32_t  DataLength;             /**< The number of bytes returned.      */
  uint32_t  DataRemaining;          /**< Number of bytes returned via RDMA. */
  uint8_t   Reserved2[4];           /**< Reserved-MB0.                      */
  uint8_t  *Buffer;                 /**< Pointer to the return buffer.      */
  } smb2_ReadResp;

/**
 * @struct  smb2_WriteReq
 * @brief   SMB2 Write Request message structure.
 * @details
 *  The SMB2 Write Request includes the basic elements of a write
 *  operation: The file handle, the blob of data to be written, the size
 *  of the data blob, and the offset at which to write it.  Additionally,
 *  flags are available to request that the write be made directly to the
 *  underlying object store.  For SMB3, several fields are present to
 *  support RDMA data transfers.
 *
 *   \b Pointers
 *    - \b \c #FileId - Points to a 16-byte array which contains the FileId
 *                      of the open file into which the data will be written.
 *    - \b \c #Buffer - A pointer to the data to be written or, if RDMA is
 *                      in use, a pointer to a Channel Info block.
 *
 * @var smb2_WriteReq::Flags
 *      Manage buffered I/O on write.  See \c #smb2_WriteReqFlags.
 * @var smb2_WriteReq::Channel
 *      In the SMB2 dialect family, this field is reserved and must be zero.
 *      For the SMB3 dialect family, this field must be one of the values
 *      provided by \c #smb2_ChannelFlags.
 * @var smb2_WriteReq::WriteChannelInfoOffset
 *      If \c Channel is not \c SMB2_CHANNEL_NONE, this field will indicate
 *      the location of an RDMA Channel Info Block within the
 *      #smb2_WriteReq.Buffer.
 * @var smb2_WriteReq::WriteChannelInfoLength
 *      If \c Channel is not \c SMB2_CHANNEL_NONE, this field will indicate
 *      the length, in bytes, of the Channel Info Block.
 * @var smb2_WriteReq::Buffer
 *      For RDMA transfers, the \c Buffer will contain a Channel Info Block
 *      indicating the RDMA channel on which the data to be written will be
 *      received.  For non-RDMA, the Buffer will contain the write data
 *      itself.
 *
 * \note
 *  Read [MS-SMB2] and [MS-SMBD] regarding the use of the Channel Info Block,
 *  particularly regarding the use of the \c #smb2_WriteReq.Buffer blob.
 *  The description given here is preliminary.
 *
 * @todo
 *  - Research the Channel Info Block and verify that our documentation is
 *    correct and complete.  Explain how read and write operations are
 *    modified when the I/O data is sent on an RDMA channel.
 */
typedef struct
  {
  uint16_t  StructureSize;          /**< Structure size, fixed at 49.         */
  uint16_t  DataOffset;             /**< Message offset of the write data.    */
  uint32_t  Length;                 /**< Number of bytes to to be written.    */
  uint64_t  Offset;                 /**< The file location at which to write. */
  uint8_t  *FileId;                 /**< Pointer to the 16-byte FileId.       */
  uint32_t  Channel;                /**< See #smb2_ChannelFlags.              */
  uint32_t  RemainingBytes;         /**< The number of RDMA-transfer bytes.   */
  uint16_t  WriteChannelInfoOffset; /**< Message offset of RDMA Channel Info. */
  uint16_t  WriteChannelInfoLength; /**< Size of the RDMA Channel Info.       */
  uint32_t  Flags;                  /**< See #smb2_WriteReqFlags.             */
  uint8_t  *Buffer;                 /**< Data blob.                           */
  } smb2_WriteReq;

/**
 * @struct  smb2_WriteResp
 * @brief   SMB2 Write Response message structure.
 * @details
 *  The SMB2 Write Response is used to indicate the success of a write
 *  operation.  An SMB2 Error Response structure will be returned if an
 *  error occurs.
 *
 * \b Notes
 *  - Most of the fields in the SMB2 Write Response are reserved.  The
 *    reserved fields should be set to zero on transmission, and ignored
 *    upon receipt.
 *  - The reserved fields are handled as arrays of bytes rather than unsigned
 *    integers.  Data is read and written to these fields in wire order.
 */
typedef struct
  {
  uint16_t  StructureSize;              /**< Structure size, fixed at 17. */
  uint8_t   Reserved[2];                /**< Reserved-MB0.                */
  uint32_t  Count;                      /**< Number of bytes written.     */
  uint8_t   Remaining[4];               /**< Unused: Reserved-MB0.        */
  uint8_t   WriteChannelInfoOffset[2];  /**< Unused: Reserved-MB0.        */
  uint8_t   WriteChannelInfoLength[2];  /**< Unused: Reserved-MB0.        */
  } smb2_WriteResp;

/**
 * @struct  smb2_FlushReq
 * @brief   SMB2/3 I/O Flush Request message structure.
 * @details The server needs only the FileId of the file to be quiesced.
 *          Note that the FileId is aligned on an 8-byte boundary.  This
 *          is typical for SMB2 message structures.
 *
 * \b Pointers
 *  - \b \c #FileId - A pointer to an array of 16-bytes which contains the
 *                    actual \c FileId.
 *
 * @var smb2_FlushReq::FileId
 *      A pointer to a 16-byte array that contains the \c FileId of the open
 *      file to be flushed.  An SMB2_FILEID is made up of two 8-byte (64-bit)
 *      subfields, which have significance when working with Durable,
 *      Resilient, and Persistent handles.  Otherwise, the \c FileId can be
 *      viewed as a 128-bit GUID.
 *
 * @see <a href="@msdocs/ms-smb2/f1d9b40d-e335-45fc-9d0b-199a31ede4c3">[MS-SMB2;
 *         2.2.14.1]: SMB2_FILEID</a>
 * @see <a href="@msdocs/ms-smb2/eb446d90-a173-4797-9e83-d09490b203f4">[MS-SMB2;
 *         3.3.1.10]: Per Open</a>
 *
 */
typedef struct
  {
  uint16_t  StructureSize;  /**< Structure size, fixed at 24.     */
  uint8_t   Reserved1[2];   /**< Reserved-MB0; aligns to 4 bytes. */
  uint8_t   Reserved2[4];   /**< Reserved-MB0; aligns to 8 bytes. */
  uint8_t  *FileId;         /**< Pointer to a 16-byte FileId.     */
  } smb2_FlushReq;

/**
 * @typedef smb2_FlushResp
 * @brief   SMB2/3 I/O Flush Response message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.  The Flush Response
 *          message is used to return a #STATUS_SUCCESS result.
 * @see     #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_FlushResp;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseReadReq( uint16_t      const dialect,
                       uint8_t      *const msg,
                       size_t        const msgLen,
                       smb2_ReadReq *const readReq );

int smb2_packReadReq( uint16_t      const dialect,
                      smb2_ReadReq *const readReq,
                      uint8_t      *const bufr,
                      uint32_t      const bSize );

int smb2_parseReadResp( uint16_t        const dialect,
                        uint8_t        *const msg,
                        size_t          const msgLen,
                        smb2_ReadResp  *const readResp );

int smb2_packReadResp( uint16_t       const dialect,
                       smb2_ReadResp *const readResp,
                       uint8_t       *const bufr,
                       uint32_t       const bSize );

int smb2_parseWriteReq( uint16_t        const dialect,
                        uint8_t        *const msg,
                        size_t          const msgLen,
                        smb2_WriteReq  *const writeReq );

int smb2_packWriteReq( uint16_t       const dialect,
                       smb2_WriteReq *const writeReq,
                       uint8_t       *const bufr,
                       uint32_t       const bSize );

int smb2_parseWriteResp( uint16_t        const dialect,
                         uint8_t        *const msg,
                         size_t          const msgLen,
                         smb2_WriteResp *const writeResp );

int smb2_packWriteResp( uint16_t        const dialect,
                        smb2_WriteResp *const writeResp,
                        uint8_t        *const bufr,
                        uint32_t        const bSize );

int smb2_parseFlushReq( uint16_t        const dialect,
                        uint8_t        *const msg,
                        size_t          const msgLen,
                        smb2_FlushReq  *const flushReq );

int smb2_packFlushReq( uint16_t       const dialect,
                       smb2_FlushReq *const flushReq,
                       uint8_t       *const bufr,
                       uint32_t       const bSize );

int smb2_parseFlushResp( uint16_t         const dialect,
                         uint8_t         *const msg,
                         size_t           const msgLen,
                         smb2_FlushResp  *const flushResp );

int smb2_packFlushResp( uint16_t        const dialect,
                        smb2_FlushResp *const baseMsg,
                        uint8_t        *const bufr,
                        uint32_t        const bSize );


/* ================================ smb2_io.h =============================== */
#endif /* SMB2_IO_H */
