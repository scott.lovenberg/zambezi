#ifndef SMB_NEGOTIATE_H
#define SMB_NEGOTIATE_H
/* ========================================================================== **
 *                              smb2_negotiate.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: SMB2/3 Negotiate Request/Response message handling.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_negotiate.h; 2021-03-10 17:06:15 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file      smb2_negotiate.h
 * @author    Christopher R. Hertel
 * @brief     Parse and pack SMB2/3 Negotiate request and response messages.
 * @date      10 Apr 2020
 * @version   \$Id: smb2_negotiate.h; 2021-03-10 17:06:15 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  The SMB2 negotiate exchange is the first step in setting up an SMB2/3
 *  connection between the client and the server.  It provides the ground
 *  rules for the connection, including the SMB dialect, capabilities, and
 *  features that the client and server will (and will not) use.
 *
 * \b Notes
 * - The negotiation of SMB2/3 dialects \e may be initiated by an \b SMB1
 *   Negotiate Request that includes the \c "SMB 2.002" and/or the
 *   \c "SMB 2.???" dialect string.
 *
 * @see smb1_negotiate.h
 * @see <a href="@msdocs/ms-cifs/25c8c3c9-58fc-4bb8-aa8f-0272dede84c5">[MS-CIFS;
 *  2.2.4.52.1]: \c SMB_COM_NEGOTIATE Request</a>
 * @see The \c #SMB2_Dialect enumeration type, defined in smb2_header.h.
 * @see <a href="@msdocs/ms-smb2/e14db7ff-763a-4263-8b10-0c3944f52fc5">[MS-SMB2;
 *  2.2.3]: SMB2 NEGOTIATE Request</a>
 * @see <a href="@msdocs/ms-smb2/63abf97c-0d09-47e2-88d6-6bfa552949a5">[MS-SMB2;
 *  2.2.4]: SMB2 NEGOTIATE Response</a>
 *
 * @todo
 *  - As of release v20200826, [MS-SMB2] states that clients \b SHOULD
 *    send a \c #SMB2_NETNAME_NEGOTIATE_CONTEXT_ID context, but the
 *    server \b MUST ignore it.  This will probably be clarified in a
 *    later update.  In the mean time, parsing and packing of this
 *    context type has not been implemented.  See [MS-SMB2; 3.3.5.4].
 *  - The \c #SMB2_TRANSPORT_CAPABILITIES context type is listed in <a
 *    href="@msdocs/ms-winerrata/2cdafcfa-ce51-426a-9678-630a505a1a35">
 *    [MS-WINERRATA] for [MS-SMB2]</a>.  The structure is presented as a
 *    4-byte \c Reserved field.  This Capabilities type can be
 *    implemented once it has been better defined.
 *  - The SMB1 \c SMB_COM_NEGOTIATE message can be used to initiate
 *    and SMB2/3 session.  It is probably worth-while to provide a
 *    mini-implementation of SMB1 protocol negotiation.
 */

#include "smb_util.h"       /* SMB utility macros (padding calculator). */
#include "smb2_header.h"    /* SMB2/3 Message Header module.            */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def     SMB2_NEG_REQ_SIZE
 * @brief   Byte length of the fixed portion of an SMB2 Negotiate Request.
 * @details The size, in bytes, of the fixed-length portion of an SMB2/3
 *          Negotiate Request message.
 *
 * @def     SMB2_NEG_REQ_STRUCT_SIZE
 * @brief   SMB2 Negotiate Request StructureSize.
 * @details The structure size, in bytes, of the SMB2 Negotiate Request
 *          message.  The \c #smb2_NegotiateReq.StructureSize field MUST be
 *          set to this value.
 * @note    In most cases, if there is a variable portion to the message,
 *          the \c StructureSize field will be set to the size of the fixed
 *          portion plus one, meaning that the value will be odd.  Wireshark
 *          goes so far as to label the low-order bit as a flag that
 *          indicates the presence of a "Dynamic Part".  In the Negotiate
 *          Request, however, the low-order bit is \b not set despite the
 *          presence of a variable portion.
 *
 * @def     SMB2_NEG_RESP_SIZE
 * @brief   The fixed-portion byte length of the SMB2 Negotiate Response.
 * @details The size, in bytes, of the fixed-length portion of an SMB2
 *          Negotiate Response.
 *
 * @def     SMB2_NEG_RESP_STRUCT_SIZE
 * @brief   SMB2 Negotiate Response StructureSize.
 * @details The structure size, in bytes, of the SMB2/3 Negotiate Response
 *          message, plus one.  This is given as one more than the fixed
 *          portion length to indicate that the message has a variable
 *          length portion as well.
 *
 * @def     SMB2_CLIENT_GUID_LENGTH
 * @brief   The length, in bytes, of the Client's GUID.
 * @details A GUID is a "Globally Unique Identifier".  This value is the
 *          length, in bytes, of the \c ClientGuid field within the SMB2/3
 *          Negotiate Request message.
 *
 * @def     SMB2_SERVER_GUID_LENGTH
 * @brief   The length, in bytes, of the Server's GUID.
 * @details A GUID is a "Globally Unique Identifier".  This value is the
 *          length, in bytes, of the \c ServerGuid field within the SMB2/3
 *          Negotiate Response message.
 */

#define SMB2_NEG_REQ_SIZE         36
#define SMB2_NEG_REQ_STRUCT_SIZE  36

#define SMB2_NEG_RESP_SIZE        65
#define SMB2_NEG_RESP_STRUCT_SIZE 65

#define SMB2_CLIENT_GUID_LENGTH   16
#define SMB2_SERVER_GUID_LENGTH   16


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum SMB2_SecurityMode
 *  SMB2/3 Negotiate and Session Setup SecurityMode bits.
 *
 * \b Notes
 *  - The SecurityMode field is present in both Negotiate Request and
 *    Response messages, and also in Session Setup Requests.
 *
 *  - In Request messages, the SIGNING_ENABLED and SIGNING_REQUIRED bits are
 *    mutually exclusive and the SIGNING_ENABLED bit should be ignored.
 *
 *  - In the NegProt Response the ENABLED and REQUIRED bits are independent.
 *
 * @see <a href="@msdocs/ms-smb2/5a3c2c28-d6b0-48ed-b917-a86b2ca4575f">[MS-SMB2;
 *  2.2.5]: SMB2 SESSION_SETUP Request</a>
 *
 * @var SMB2_NEGOTIATE_SIGNING_ENABLED
 *  When set, this bit indicates that security signatures are \b enabled.
 *
 * @var SMB2_NEGOTIATE_SIGNING_REQUIRED
 *  When set, this bit indicates that security signatures are \b required.
 *
 * @var SMB2_NEGOTIATE_SECMODE_MASK
 *  SecurityMode field bitmask.
 */
typedef enum
  {
  SMB2_NEGOTIATE_SIGNING_ENABLED  = 0x0001,
  SMB2_NEGOTIATE_SIGNING_REQUIRED = 0x0002,
  SMB2_NEGOTIATE_SECMODE_MASK     = 0x0003
  } SMB2_SecurityMode;

/**
 * @enum SMB2_Negotiate_Capabilities
 *  SMB2/3 Negotiate and Session Setup Capability bits.
 *
 * @see <a href="@msdocs/ms-smb2/63abf97c-0d09-47e2-88d6-6bfa552949a5">[MS-SMB2;
 *  2.2.4]: SMB2 NEGOTIATE Response</a>
 * @see <a href="@msdocs/ms-smb2/5a3c2c28-d6b0-48ed-b917-a86b2ca4575f">[MS-SMB2;
 *  2.2.5]: SMB2 SESSION_SETUP Request</a>
 *
 * @var SMB2_GLOBAL_CAP_DFS
 *  When set, this bit indicates support for Distributed File System (DFS).
 *
 * @var SMB2_GLOBAL_CAP_LEASING
 *  When set, this bit indicates support for file leasing (and OpLocks).
 *
 * @var SMB2_GLOBAL_CAP_LARGE_MTU
 *  When set, this bit indicates support for multi-credit operations.
 *
 * @var SMB2_GLOBAL_CAP_MULTI_CHANNEL
 *  When set, this bit indicates support for creating a single session over
 *  multiple network channels.
 *
 * @var SMB2_GLOBAL_CAP_PERSISTENT_HANDLES
 *  When set, this bit indicates support for persistent handles.
 *
 * @var SMB2_GLOBAL_CAP_DIRECTORY_LEASING
 *  When set, this bit indicates support for directory leasing.
 *
 * @var SMB2_GLOBAL_CAP_ENCRYPTION
 *  When set, this bit indicates support for SMB2+ encryption.
 *
 * @var SMB2_GLOBAL_CAP_NEGPROT_MASK
 *  Field mask for the Negotiate Response Capabilities field.
 *  In the Negotiate Request, this mask is only valid for #SMB2_V0300
 *  and above. In Negotiate Requests from clients that do not support
 *  SMB3+, the Capabilites field \b must be set to zero and \b must be
 *  ignored by the server.
 *
 * @var SMB2_GLOBAL_CAP_SSETUP_MASK
 *  Field mask for the Session Setup Request Capabilities field.
 *  Only SMB2_GLOBAL_CAP_DFS is supported.
 */
typedef enum
  {
  SMB2_GLOBAL_CAP_DFS                 = 0x00000001,
  SMB2_GLOBAL_CAP_LEASING             = 0x00000002,
  SMB2_GLOBAL_CAP_LARGE_MTU           = 0x00000004,
  SMB2_GLOBAL_CAP_MULTI_CHANNEL       = 0x00000008,
  SMB2_GLOBAL_CAP_PERSISTENT_HANDLES  = 0x00000010,
  SMB2_GLOBAL_CAP_DIRECTORY_LEASING   = 0x00000020,
  SMB2_GLOBAL_CAP_ENCRYPTION          = 0x00000040,
  SMB2_GLOBAL_CAP_NEGPROT_MASK        = 0x0000007F,
  SMB2_GLOBAL_CAP_SSETUP_MASK         = 0x00000001
  } SMB2_Negotiate_Capabilities;

/**
 * @enum SMB2_Negotiate_ContextType
 *  Negotiate Contexts were introduced with SMBv3.1.1.
 *
 * @see <a href="@msdocs/ms-smb2/15332256-522e-4a53-8cd7-0bd17678a2f7">[MS-SMB2;
 *  2.2.3.1]: SMB2 NEGOTIATE_CONTEXT Request Values</a>
 *
 * @var SMB2_PREAUTH_INTEGRITY_CAPABILITIES
 *      Indicates support for preauthentication integrity checks.
 *
 * @var SMB2_ENCRYPTION_CAPABILITIES
 *      Indicates support for message encryption (sealing).
 *
 * @var SMB2_COMPRESSION_CAPABILITIES
 *      Indicates support for data compression.
 *
 * @var SMB2_NETNAME_NEGOTIATE_CONTEXT_ID
 *      Provides the server name as specified by the client.  The name is
 *      given as a NUL-terminated Unicode string in UTF-16LE format.  The
 *      NUL character, therefore, is the two-octet sequence {0x00, 0x00}.
 *      The server is expected to ignore this context (...but it might be
 *      fun to peek anyway).
 *
 * @var SMB2_TRANSPORT_CAPABILITIES
 *      This Context Type is defined in the [MS-SMB2] Errata, released
 *      22-June-2020.  Though the context type is defined, the errata
 *      provides only a placeholder definition of the context type.
 * @see <a href="@msdocs/ms-winerrata/2cdafcfa-ce51-426a-9678-630a505a1a35">
 *      [MS-WINERRATA] for [MS-SMB2]</a>
 */
typedef enum
  {
  SMB2_PREAUTH_INTEGRITY_CAPABILITIES = 0x0001,
  SMB2_ENCRYPTION_CAPABILITIES        = 0x0002,
  SMB2_COMPRESSION_CAPABILITIES       = 0x0004,
  SMB2_NETNAME_NEGOTIATE_CONTEXT_ID   = 0x0005,
  SMB2_TRANSPORT_CAPABILITIES         = 0x0006
  } SMB2_Negotiate_ContextType;

/**
 * @enum SMB2_Negotiate_HashAlgorithm
 *
 * @see <a href="@msdocs/ms-smb2/5a07bd66-4734-4af8-abcf-5a44ff7ee0e5">[MS-SMB2;
 *  2.2.3.1.1]: SMB2_PREAUTH_INTEGRITY_CAPABILITIES Context</a>
 *
 * @var SMB2_HASH_ALGORITHM_SHA512
 *  When set in a Negotiate Context, this value indicates support for the
 *  SHA-512 hash algorithm.
 */
typedef enum
  {
  SMB2_HASH_ALGORITHM_SHA512  = 0x0001
  } SMB2_Negotiate_HashAlgorithm;

/**
 * @enum SMB2_Negotiate_Cipher
 *
 * @see <a href="@msdocs/ms-smb2/16693be7-2b27-4d3b-804b-f605bde5bcdd">[MS-SMB2;
 *  2.2.3.1.2]: SMB2_ENCRYPTION_CAPABILITIES Context</a>
 *
 * @var SMB2_CIPHER_NONE
 *  Only sent by the server; this value indicates that the client has
 *  offered no ciphers that are also supported by the server (cipher
 *  negotiation has failed).
 * @see <a href="@msdocs/ms-smb2/b39f253e-4963-40df-8dff-2f9040ebbeb1">[MS-SMB2;
 *  3.3.5.4]: Receiving an SMB2 NEGOTIATE Request</a>
 *
 * @var SMB2_CIPHER_AES128CCM
 *  Indicates support for the AES-128-CCM (Counter with CBC-MAC) cypher.
 * @see <a href="https://en.wikipedia.org/wiki/CCM_mode">Wikipedia: CCM Mode</a>
 *
 * @var SMB2_CIPHER_AES128GCM
 *  Indicates support for the AES-128-GCM (Galois/Counter Mode) cypher.
 * @see <a href="https://en.wikipedia.org/wiki/Galois/Counter_Mode">Wikipedia:
 *  Galois/Counter Mode</a>
 */
typedef enum
  {
  SMB2_CIPHER_NONE      = 0x0000,
  SMB2_CIPHER_AES128CCM = 0x0001,
  SMB2_CIPHER_AES128GCM = 0x0002
  } SMB2_Negotiate_Cipher;

/**
 * @enum SMB2_Compression_Caps_Flags
 * @see <a href="@msdocs/ms-smb2/78e0c942-ab41-472b-b117-4a95ebe88271">[MS-SMB2;
 *  2.2.3.1.3]: SMB2_COMPRESSION_CAPABILITIES Context</a>
 *
 * @var SMB2_COMPRESSION_CAPABILITIES_FLAG_NONE
 *      No flags.
 *
 * @var SMB2_COMPRESSION_CAPABILITIES_FLAG_CHAINED
 *      The sender supports chained compression on the connection.
 */
typedef enum
  {
  SMB2_COMPRESSION_CAPABILITIES_FLAG_NONE     = 0x00000000,
  SMB2_COMPRESSION_CAPABILITIES_FLAG_CHAINED  = 0x00000001
  } SMB2_Compression_Caps_Flags;

/**
 * @enum SMB2_Negotiate_Compression
 *
 * @see <a href="@msdocs/ms-smb2/78e0c942-ab41-472b-b117-4a95ebe88271">[MS-SMB2;
 *  2.2.3.1.3]: SMB2_COMPRESSION_CAPABILITIES Context</a>
 * @see <a href="@msdocs/ms-xca/a8b7cb0a-92a6-4187-a23b-5e14273b96f8">[MS-XCA]:
 *  Xpress Compression Algorithm</a>
 *
 * @var SMB2_COMPRESSION_NONE
 *      No compression.
 *
 * @var SMB2_COMPRESSION_LZNT1
 *      The LZNT1 compression algorithm.
 *
 * @var SMB2_COMPRESSION_LZ77
 *      The LZ77 compression algorithm.
 *
 * @var SMB2_COMPRESSION_LZ77H
 *      The LZ77 compression algorithm with Huffman encoding.
 *
 * @var SMB2_COMPRESSION_PATTERN_V1
 *      SMB3 Pattern Scanning algorithm, version 1.
 */
typedef enum
  {
  SMB2_COMPRESSION_NONE       = 0x0000,
  SMB2_COMPRESSION_LZNT1      = 0x0001,
  SMB2_COMPRESSION_LZ77       = 0x0002,
  SMB2_COMPRESSION_LZ77H      = 0x0003,
  SMB2_COMPRESSION_PATTERN_V1 = 0x0004
  } SMB2_Negotiate_Compression;


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  smb2_NegotiateReq
 * @brief   SMB2/3 Protocol Negotiation Request.
 * @details
 *  This structure contains the values that are extracted from, or used to
 *  compose, an SMB2/3 Negotiate Request as transmitted on the wire.  As
 *  with all of these modules, integer values are stored in host byte-order
 *  in the structure and converted to/from SMB byte order on the wire.
 *
 * \b Pointers
 *  - \b \c #ClientGuid - Pointer to the client's unique identifier.  The
 *                        Client GUID is always \c #SMB2_CLIENT_GUID_LENGTH
 *                        bytes in length.
 *  - \b \c #Dialects   - Pointer to the array of dialect IDs sent by
 *                        or received from the client.
 *  - \b \c #Padding    - For incoming messages, this will point to the
 *                        location within the received packet at which the
 *                        padding begins.  If there is no padding, it will
 *                        be NULL.
 *  - \b \c #NegotiateContextList -
 *                        Pointer to a list of Negotiate Context records,
 *                        or NULL, in which case \c #NegotiateContextCount
 *                        \b MUST be zero.
 *
 * \b Notes
 *  - The 8-byte \c ClientStartTime field, described in [MS-SMB2; 2.2.3],
 *    is not used.  The specification says that the client "MUST set this
 *    field to zero (0) and the server MUST ignore it...".  That it has a
 *    name is simply a distraction.  It is just another reserved field.
 *    In SMBv3.1.1, this field is replaced by \c NegotiateContextOffset,
 *    \c NegotiateContextCount, and \c Reserved2.
 *
 * @see <a href="@msdocs/ms-smb2/e14db7ff-763a-4263-8b10-0c3944f52fc5">[MS-SMB2;
 *  2.2.3]: SMB2 NEGOTIATE Request</a>
 *
 * @var smb2_NegotiateReq::NegotiateContextOffset
 *      The offset, from the start of the message, at which to find the
 *      \c #NegotiateContextList, if it is present.
 *
 * @var smb2_NegotiateReq::NegotiateContextCount
 *      The number of Negotiate Contexts contained in the message.
 *
 * @var smb2_NegotiateReq::NegotiateContextList
 *      A pointer to the Context entries, or NULL.
 */
typedef struct
  {
  uint16_t  StructureSize;  /**< Request structure size, fixed at 36 bytes.   */
  uint16_t  DialectCount;   /**< Count of the included dialect options.       */
  uint16_t  SecurityMode;   /**< Signing enabled or required.                 */
  uint8_t   Reserved1[2];   /**< Reserved-MB0.                                */
  uint32_t  Capabilities;   /**< Client capabilities (supported features).    */
  uint8_t  *ClientGuid;     /**< Client's unique ID.                          */
  union
    {
    uint64_t  ClientStartTime;  /**< Dialects less than 3.1.1: Reserved-MB0.  */
    struct
      {
      uint32_t  NegotiateContextOffset; /**< 3.1.1 or greater dialects only.  */
      uint16_t  NegotiateContextCount;  /**< 3.1.1 or greater dialects only.  */
      uint8_t   Reserved2[2];           /**< Reserved-MB0.                    */
      };
    };
  uint16_t *Dialects;             /**< Pointer to the list of DialectIds.     */
  uint8_t  *Padding;              /**< Pointer to the padding bytes, or NULL. */
  uint8_t  *NegotiateContextList; /**< 3.1.1 or greater dialects, else NULL.  */
  } smb2_NegotiateReq;

/**
 * @struct  smb2_NegotiateResp
 * @brief   SMB2/3 Protocol Negotiation Response.
 * @details
 *  This structure contains the values that are extracted from, or used to
 *  compose, an SMB2/3 Negotiate response as transmitted on the wire.
 *
 * \b Pointers
 *  - \b \c #ServerGuid     - Pointer to the server's unique identifier.
 *                            The Server GUID is always
 *                            \c #SMB2_SERVER_GUID_LENGTH bytes in length.
 *  - \b \c #SecurityBuffer - For incoming messages, this will be set to
 *                            point to the location of the security buffer,
 *                            if any.  If #SecurityBufferLength is zero,
 *                            this pointer will be set to NULL.
 *  - \b \c #Padding        - For incoming messages, this will point to the
 *                            location within the received packet at which
 *                            the padding begins.  If there is no padding, it
 *                            will be NULL.
 *  - \b \c #NegotiateContextList -
 *                            For incoming messages, if the dialect is 3.1.1
 *                            or above, this pointer will be set to indicate
 *                            the location of the Negotiate Context List in
 *                            the received buffer.  If the dialect is less
 *                            than 3.1.1, or if there is no Context List,
 *                            this pointer will be NULL.
 *
 * \b Notes
 *  - The \c #NegotiateContextCount, \c #NegotiateContextOffset, and
 *    \c #NegotiateContextList fields were added in the SMBv3.1.1 dialect
 *    revision.  These fields are reserved and must be zero (NULL) in
 *    dialects of SMB2/3 prior to 3.1.1.
 *
 * @see <a href="@msdocs/ms-smb2/63abf97c-0d09-47e2-88d6-6bfa552949a5">[MS-SMB2;
 *  2.2.4]: SMB2 NEGOTIATE Response</a>
 *
 * @var smb2_NegotiateResp::NegotiateContextCount
 *      For SMBv3.1.1 and above, this is the count of context list entries.
 *      The count \b must be zero if the list is empty.  For earlier
 *      dialects, this field is \c #smb2_NegotiateResp::Reserved1 which must
 *      be zero.
 * @var smb2_NegotiateResp::ServerGuid
 *      A pointer to the server's globally unique identifier.  The server
 *      GUID is always \c #SMB2_SERVER_GUID_LENGTH bytes in length.
 * @var smb2_NegotiateResp::MaxTransactSize
 *      Some query and SET_INFO requests may send a large amount of data
 *      to the server.  This field specifies the server's upper bound for
 *      such requests.
 * @var smb2_NegotiateResp::MaxReadSize
 *      The maximum read length that the server will accept in an SMB2 Read
 *      request.
 * @var smb2_NegotiateResp::MaxWriteSize
 *      The maximum write length that the server will accept in an SMB2 Write
 *      request.
 * @var smb2_NegotiateResp::SystemTime
 *      The current time on the server, expressed in Windows FILETIME
 *      format.  See: <a
 *      href="@msdocs/ms-dtyp/2c57429b-fdd4-488f-b5fc-9e4cf020fcdf">[MS-DTYP;
 *      2.3.3]</a>
 * @var smb2_NegotiateResp::SecurityBuffer
 *      A pointer to the variable-length security buffer for the response,
 *      or NULL if the buffer is empty.
 * @var smb2_NegotiateResp::NegotiateContextList
 *      For SMBv3.1.1 and above this is a pointer to a list of context
 *      blocks, or NULL if the list is empty.
 */
typedef struct
  {
  uint16_t  StructureSize;            /**< Response structure size; 65 bytes. */
  uint16_t  SecurityMode;             /**< Signing enabled and/or required.   */
  uint16_t  DialectRevision;          /**< Server's selected dialect.         */
  union
    {
    uint8_t   Reserved1[2];           /**< Dialects < 3.1.1: Reserved-MB0.    */
    uint16_t  NegotiateContextCount;  /**< Count of context list items.       */
    };
  uint8_t  *ServerGuid;               /**< Server's unique ID (16 bytes).     */
  uint32_t  Capabilities;             /**< Server's protocol capabilities.    */
  uint32_t  MaxTransactSize;          /**< Maximum query response size.       */
  uint32_t  MaxReadSize;              /**< Max Read size, in bytes.           */
  uint32_t  MaxWriteSize;             /**< Max Write size, in bytes.          */
  uint64_t  SystemTime;               /**< Current time on Server.            */
  uint64_t  ServerStartTime;          /**< SMB2/3 Server startup time.        */
  uint16_t  SecurityBufferOffset;     /**< Security buffer location.          */
  uint16_t  SecurityBufferLength;     /**< Security buffer size, in bytes.    */
  union
    {
    uint8_t  Reserved2[4];            /**< Dialects < 3.1.1: Reserved-MB0.    */
    uint32_t NegotiateContextOffset;  /**< Context List location.             */
    };
  uint8_t  *SecurityBuffer;           /**< Pointer to the Security buffer.    */
  uint8_t  *Padding;                  /**< Pointer to the padding bytes.      */
  uint8_t  *NegotiateContextList;     /**< Pointer to the Context list.       */
  } smb2_NegotiateResp;

/**
 * @struct  smb2_Negotiate_Context
 * @brief   SMB2 Negotiate Context structure.
 * @details
 *  The SMB2 NEGOTIATE_CONTEXT structure was introduced in SMB3.1.1.
 *  It is used in both request and response messages, and provides an
 *  extensible mechanism for transmitting additional negotiation
 *  paramaters (contexts).  This structure is used to frame the actual
 *  context object, which is contained within the
 *  \c #smb2_Negotiate_Context::Data field.
 *
 * \b Pointers
 *  - \b \c #Data - Points to the actual context object.
 *
 * \b Notes
 *  - Negotiate contexts are provided in a list of one or more entries.
 *    Context types may occur in any order.
 *  - Each context in the list must begin at an offset that is a multiple
 *    of 8, relative to the start of the SMB2 Header.  There may be padding
 *    between entries.
 *  - As usual, padding should be made up of NUL bytes and must be ignored
 *    by the receiver.
 *
 * @see #SMB2_Negotiate_ContextType
 * @see <a href="@msdocs/ms-smb2/15332256-522e-4a53-8cd7-0bd17678a2f7">[MS-SMB2;
 *  2.2.3.1]: SMB2 NEGOTIATE_CONTEXT Request Values</a>
 * @see <a href="@msdocs/ms-smb2/55fa717b-1662-4b0c-8f92-7ba7768ed85d">[MS-SMB2;
 *  2.2.4.1]: SMB2 NEGOTIATE_CONTEXT Response Values</a>
 */
typedef struct
  {
  uint16_t ContextType;   /**< A pre-defined context type identifier.       */
  uint16_t DataLength;    /**< The length, in bytes, of the \c Data blob.   */
  uint8_t  Reserved[4];   /**< Reserved-MB0 (must be zero).                 */
  uint8_t *Data;          /**< Pointer to the \c Data blob.                 */
  } smb2_Negotiate_Context;

/**
 * @struct  smb2_PreAuth_Integrity_Caps
 * @brief   Context for preauthentication integrity hash algorithm negotiation.
 * @details
 *  The preauthentication integrity mechanism is used to detect (and,
 *  therefore, prevent) certain kinds of downgrade attacks that can be used
 *  to reduce the security of an SMB3 connection.  This Negotiation Context
 *  structure is used to negotiate the hash algorithm to use and, optionally,
 *  to provide "salt" for use in the integrity calculations.
 *
 *  If this structure is used, the client must provide a list of supported
 *  hash algorithms (identified numerically, see
 *  #SMB2_Negotiate_HashAlgorithm).  The server will respond with a single
 *  entry, or a STATUS_SMB_NO_PREAUTH_INTEGRITY_HASH_OVERLAP error if
 *  negotiation fails.
 *
 * @see #SMB2_Negotiate_HashAlgorithm
 * @see <a href="@msdocs/ms-smb2/5a07bd66-4734-4af8-abcf-5a44ff7ee0e5">[MS-SMB2;
 *  2.2.3.1.1]: SMB2_PREAUTH_INTEGRITY_CAPABILITIES Context (request)</a>
 * @see <a href="@msdocs/ms-smb2/40e1607f-edae-4e0a-b3ec-e9b4713b2a0f">[MS-SMB2;
 *  2.2.4.1.1]: SMB2_PREAUTH_INTEGRITY_CAPABILITIES Context (response)</a>
 * @see <a href="@msdocs/ms-smb2/b39f253e-4963-40df-8dff-2f9040ebbeb1">[MS-SMB2;
 *  3.3.5.4]: Receiving an SMB2 NEGOTIATE Request</a>
 */
typedef struct
  {
  uint16_t  HashAlgorithmCount; /**< The number of \c #HashAlgorithms entries.*/
  uint16_t  SaltLength;         /**< Length, in bytes, of the \c Salt field.  */
  uint16_t *HashAlgorithms;     /**< Pointer to an array of 2 byte Hash IDs.  */
  uint8_t  *Salt;               /**< Pointer to an array of bytes.            */
  } smb2_PreAuth_Integrity_Caps;

/**
 * @struct  smb2_Encryption_Caps
 * @brief   Negotiate Context for encryption algorithm selection.
 * @details
 *  The client uses this structure to send a list of cipher IDs to the
 *  server.  The server will use the same structure to return the selected
 *  cipher back to the client.  If there are no common ciphers, the server
 *  will return a single \c Ciphers value of SMB2_CIPHER_NONE (0x0000).
 *
 * @see #SMB2_Negotiate_Cipher
 * @see <a href="@msdocs/ms-smb2/16693be7-2b27-4d3b-804b-f605bde5bcdd">[MS-SMB2;
 *  2.2.3.1.2]: SMB2_ENCRYPTION_CAPABILITIES Context (request)</a>
 * @see <a href="@msdocs/ms-smb2/5579ec72-d64f-439f-800f-fd7aa7695f0e">[MS-SMB2;
 *  2.2.4.1.2]: SMB2_ENCRYPTION_CAPABILITIES Context (response)</a>
 * @see <a href="@msdocs/ms-smb2/b39f253e-4963-40df-8dff-2f9040ebbeb1">[MS-SMB2;
 *  3.3.5.4]: Receiving an SMB2 NEGOTIATE Request</a>
 */
typedef struct
  {
  uint16_t  CipherCount;  /**< The number of entries in the \c Ciphers field. */
  uint16_t *Ciphers;      /**< An array of two-byte cipher ID values.         */
  } smb2_Encryption_Caps;

/**
 * @struct  smb2_Compression_Caps
 * @brief   Negotiate Context for compression algorithm selection.
 * @details
 *  The client may send this structure to indicate which (if any) compression
 *  algorithms it supports.  The supported algorithms are listed in an array
 *  of 16-bit #SMB2_Negotiate_Compression algorithm ID values.
 *
 * @var smb2_Compression_Caps::CompressionAlgorithmCount
 *      The number of \c #CompressionAlgorithms entries that follow.
 *
 * @var smb2_Compression_Caps::Padding
 *      Two bytes; reserved, must be zero (MBZ).
 *
 * @var smb2_Compression_Caps::Flags
 *      Compression capabilities flags.  See #SMB2_Compression_Caps_Flags.
 *
 * @var smb2_Compression_Caps::CompressionAlgorithms
 *      An array of two-byte (16-bit) #SMB2_Negotiate_Compression values.
 */
typedef struct
  {
  uint16_t  CompressionAlgorithmCount;
  uint8_t   Padding[2];
  uint32_t  Flags;
  uint16_t *CompressionAlgorithms;
  } smb2_Compression_Caps;


/* -------------------------------------------------------------------------- **
 * Doxygen Fudge
 *
 *  The following ifdef block forces Doxygen to produce documentation
 *  for the static functions defined in this header file.
 *  Note that these declarations are neither static nor inline, which
 *  is incorrect.  Doxygen don't care.
 */

#ifdef DOXY_TEXT
uint32_t smb2_filterServerCaps( const uint16_t dialect, uint32_t caps );
uint16_t smb2_getDialectList( const uint16_t  num,
                              uint16_t *const src,
                              uint16_t *const dst );
uint16_t smb2_setDialectList( const uint16_t  num,
                              uint16_t *const src,
                              uint16_t *const dst );
#endif /* DOXY_TEXT */


/* -------------------------------------------------------------------------- **
 * Static inline functions:
 */

static inline uint32_t smb2_filterServerCaps( const uint16_t dialect,
                                              uint32_t       caps )
  /** Server-side Capability bits filter.
   *
   * @param[in] dialect The dialect used to choose the filter to apply.
   * @param[in] caps    Initial Capabilities flags (the flags to be masked).
   *
   * @returns   The same as the input \p caps value, except that any
   *            Capabilities bits that are invalid for the given
   *            \p dialect will be cleared.
   *
   * \b Notes
   *  - This function is defined as both static and inline.  It should
   *    reduce to something very small.  It only appears complex.
   *  - This filter function may be called by a server implementation
   *    prior to composing an outgoing SMB2 Negotiate Response message.
   *    It may also be called by a client after a received SMB2 Negotiate
   *    Response message has been parsed (and the selected dialect
   *    returned).
   *  - If \p dialect is given as \c #SMB2_VNONE, or any unknown dialect,
   *    then filtering will be bypassed.  This is probably not what you
   *    want.  \b See: \c smb2_header.h
   *  - The Capabilities flag rules for a Negotiate Response are, in fact,
   *    a bit weird because some bits are ignored in v3.1.1.  Negotiate
   *    Requests and Session Setup Requests have different Capabilities
   *    rules.
   *
   * @details
   *    These bit filters are presented as a stand-alone function for two
   *    reasons:
   *    1.  The same filter logic can be used by the client and/or the server.
   *    2.  Placing the logic in a separate function puts control of and
   *        responsibility for applying the filters into the caller's hands.
   *
   * @see \c smb2_packNegotiateResp()
   * @see \c smb2_parseNegotiateResp()
   * @see <a
   *  href="@msdocs/ms-smb2/63abf97c-0d09-47e2-88d6-6bfa552949a5">[MS-SMB2;
   *  2.2.4]; in the description of the \c Capabilities bits.</a>
   * @see <a
   *  href="@msdocs/ms-smb2/bcd6d594-017b-47fc-8742-b7d847791783">[MS-SMB2;
   *  3.3.5.3.1]; the rules for responding with the SMB2 Wildcard Dialect
   *  Id.</a>
   * @see <a
   *  href="@msdocs/ms-smb2/b39f253e-4963-40df-8dff-2f9040ebbeb1">[MS-SMB2;
   *  3.3.5.4]; the rules for responding to an SMB2 Negotiate Request.</a>
   */
  {
  switch( dialect )
    {
    case SMB2_V0202:
      /* Only the DFS flag is valid.  */
      return( caps & SMB2_GLOBAL_CAP_DFS );
    case SMB2_V0210:
      /* Turn off Multi-Channel, Persistent Handles, etc. */
      caps &= ~(uint32_t)( SMB2_GLOBAL_CAP_MULTI_CHANNEL
                         | SMB2_GLOBAL_CAP_PERSISTENT_HANDLES
                         | SMB2_GLOBAL_CAP_DIRECTORY_LEASING
                         | SMB2_GLOBAL_CAP_ENCRYPTION );
      break;
    case SMB2_VWILD:
      /* [MS-SMB;3.3.5.3.1] explains that an SMB2-capable server should
       * respond to an SMB1 (CIFS) Negotiate Request by sending an SMB2
       * Negotiate Response with the SMB2 Wildcard dialect Id (0x02FF).
       * These are the only valid capabilities in such a response.
       */
      return( caps & ( SMB2_GLOBAL_CAP_DFS
                     | SMB2_GLOBAL_CAP_LEASING
                     | SMB2_GLOBAL_CAP_LARGE_MTU ) );
    case SMB2_V0300:
    case SMB2_V0302:
      /* All known flags are, basically, acceptable for v3.0.x. */
      break;
    case SMB2_V0311:
      /* With v3.1.1, negotiation of encryption is handled via Negotiate
       * Contexts.  The client indicates support for encryption by setting
       * SMB2_GLOBAL_CAP_ENCRYPTION in the request, but the server must
       * clear this bit in the response, as described in [MS-SMB2;3.3.5.4].
       */
      caps &= ~(uint32_t)SMB2_GLOBAL_CAP_ENCRYPTION;
      break;
    default:
      /* For SMB2_VNONE or any unknown dialect, do no filtering.  */
      return( caps );
    }

  /* Last filter: allow only known flags. */
  return( caps & SMB2_GLOBAL_CAP_NEGPROT_MASK );
  } /* smb2_filterServerCaps */

static inline uint16_t smb2_getDialectList( const uint16_t  num,
                                            uint16_t *const src,
                                            uint16_t *const dst )
  /** Convert an array of SMB2/3 dialect IDs to host byte order.
   *
   * @param[in]   num The number of list entries to be converted.
   * @param[in]   src Pointer to a list of 16-bit unsigned integers, assumed
   *                  to be Dialect Ids in SMB byte order (wire format).
   * @param[out]  dst Pointer to an array of 16-bit integers.  The array
   *                  must be large enough to receive \p num entries.  The
   *                  converted values will be written into this array.
   *                  If \p dst is NULL, the contents of \p src will be
   *                  converted in place.
   *
   * @returns   The highest value Dialect Id in the array.
   *            This \em should be a valid dialect value, but \em may be
   *            outside of the prescribed range.  There is, unfortunately,
   *            nothing to prevent a feral client from sending bogus values.
   *            You have been warned.
   *
   * \b Notes
   *  - This function is defined as both static and inline.
   *  - Both \p src and \p dst must be large enough to contain \p num
   *    entries.
   *  - Both \p src and \p dst \e may safely point to the same location.
   *    If they do, the host-byte-order values will overwrite the original
   *    SMB wire-format values in place.
   *  - The order of the values within the list is, per [MS-SMB2], not
   *    significant.  Also, the specification does not rule out duplicates.
   *    Expect the würst.
   *
   * @see \c smb2_parseNegotiateReq()
   * @see \c smb2_parseNegReq2ndPass()
   * @see \c smb2_parseNegotiateCtx()
   */
  {
  int       i;
  uint8_t  *p = (uint8_t *)src;
  uint16_t *q;
  uint16_t  maxD = 0;

  /* Sanity checks. */
  assert( num > 0 );
  assert( NULL != src );

  q = ( NULL == dst ) ? src : dst;

  for( i = 0; i < num; i++ )
    {
    q[i] = smbget16( (uint8_t *)p );
    p += sizeof( uint16_t );
    if( q[i] > maxD )
      maxD = q[i];
    }
  return( maxD );
  } /* smb2_getDialectList */

static inline uint16_t smb2_setDialectList( const uint16_t  num,
                                            uint16_t *const src,
                                            uint16_t *const dst )
  /** Convert an array of 16-bit Dialect IDs from host to SMB byte order.
   *
   * @param[in]   num The number of array entries to be converted.
   * @param[in]   src Pointer to an array of 16-bit unsigned integers,
   *                  assumed to be Dialect IDs in host byte order.
   * @param[out]  dst Pointer to an array of 16-bit unsigned integers.  The
   *                  converted values will be written into this array.  The
   *                  array myst be large enough to receive \p num entries.
   *
   * @returns   The highest value Dialect ID in the array, which \em should
   *            be a valid Dialect ID.
   *
   * \b Notes
   *  - This function is defined as both static and inline.
   *  - Both \p src and \p dst must be large enough to contain \p num
   *    entries.
   *  - Both \p src and \p dst may safely point to the same location.  If so
   *    the SMB byte order values will overwrite the original values in
   *    place.
   *  - The values in the list \em should be valid Dialect IDs, in ascending
   *    order, without duplicates.  Well-written servers \em should be able
   *    to handle bogus values, but this is an assumption that should not be
   *    tested <em>in vivo</em>.
   */
  {
  int       i;
  uint8_t  *p    = (uint8_t *)dst;
  uint16_t  maxD = 0;

  for( i = 0; i < num; i++ )
    {
    p = smbset16( p, src[i] );
    if( src[i] > maxD )
      maxD = src[i];
    }
  return( maxD );
  } /* smb2_setDialectList */


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

/* Parse a Negotiate Request message. */

int smb2_parseNegotiateReq( uint8_t           *const msg,
                            size_t             const msgLen,
                            smb2_NegotiateReq *const negReq );

int smb2_parseNegReq2ndPass( uint16_t           const dialect,
                             uint8_t           *const msg,
                             size_t             const msgLen,
                             smb2_NegotiateReq *const negReq );

/* Pack a Negotiate Request message.  */

int smb2_packNegotiateReq( smb2_NegotiateReq *const negReq,
                           uint8_t           *const bufr,
                           uint32_t           const bSize );

int smb2_packNegReq2ndPass( smb2_NegotiateReq *const negReq,
                            uint8_t           *const bufr,
                            uint32_t           const bSize );

/* Parse and Pack a Negotiate Response messages.  */

int smb2_parseNegotiateResp( uint8_t            *const msg,
                             size_t              const msgLen,
                             smb2_NegotiateResp *const negResp );

int smb2_packNegotiateResp( const smb2_NegotiateResp *const negResp,
                            uint8_t                  *const bufr,
                            uint32_t                  const bSize );

/* Parse and Pack Negotiate Context List headers. */

int smb2_parseNegotiateCtx( uint8_t                 *const msgCtx,
                            size_t const                   msgLen,
                            smb2_Negotiate_Context  *const negCtx );

int smb2_packNegotiateCtx( smb2_Negotiate_Context *const negCtx,
                           uint8_t                *const bufr,
                           uint32_t                const bSize );

/* Parse and Pack known Negotiate Context structures. */

int smb2_parsePreAuthIntegrityCaps( uint8_t                     *const ctxData,
                                    uint16_t                     const dataLen,
                                    smb2_PreAuth_Integrity_Caps *const paCaps );

int smb2_packPreAuthIntegrityCaps( smb2_PreAuth_Integrity_Caps *const paCaps,
                                   uint8_t                     *const bufr,
                                   uint32_t                     const bSize );

int smb2_parseEncryptionCaps( uint8_t              *const ctxData,
                              uint16_t              const dataLen,
                              smb2_Encryption_Caps *const enCaps );

int smb2_packEncryptionCaps( smb2_Encryption_Caps *const enCaps,
                             uint8_t              *const bufr,
                             uint32_t              const bSize );

int smb2_parseCompressionCaps( uint8_t               *const ctxData,
                               uint16_t const               dataLen,
                               smb2_Compression_Caps *const cCaps );

int smb2_packCompressionCaps( smb2_Compression_Caps *const cCaps,
                              uint8_t               *const bufr,
                              uint32_t               const bSize );


/* ============================ smb2_negotiate.h ============================ */
#endif /* SMB_NEGOTIATE_H */
