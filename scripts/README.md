Scripts
-------

This directory contains:

* `git_utils.py`:     A utility module.
* `git_keywords.py`:  A git pre-commit script.
* Other stuff:        Other stuff.

The Python scripts are used to perform keyword substitution on program files
as they are committed to the repository, adding or updating the information
associated with specific keywords.

Zambezi uses keywords in its source files, which helps us keep track of
what happened when and who did it.  Yes, git also provides that information
(and in greater detail), but if'n all you've got is a copy of a file outside
of a repository then you're out of context and out of luck.  Keyword
substitution mitigates that problem (to some extent).

Setting up your repository to support keyword substitution:

1. Make sure you have Python installed (Python 2, not Py3).

2. Make sure `git_keywords.py` is executable.

3. In the `<repository>/.git/hooks` directory:
   ```
   $ ln -s ../../scripts/git_keywords.py pre-commit
   ```

4. Make sure that the `<repository>/.gitattributes` file contains something
   that looks sort of like this:
   ```
   *.py kwsub=true
   *.dox kwsub=true
   *.[ch] kwsub=true
   README.md kwsub=true
   ```

5. Ensure that your username and e'mail address are correctly assigned in
   the `<repository>/.git/config` file.  If you are using a GitLab account,
   your e'mail address should probably be the address you're using on GitLab.
____
$Id: README.md; 2019-04-25 15:26:55 -0500; Christopher R. Hertel$
