Zambezi::NBT
------------

This is awkward.  Then again, so is the spelling of "awkward".

NBT is a set of protocols that work together to create a virtual LAN
environment overlaid on an IPv4 internetwork.  NBT was created in the 1980s,
when SMB ran on DOS systems.  The reason NBT was created was that SMB (and
several other services and applications) used a DOS-standard network API called
NetBIOS.

<table align="center"><tr><td>:pencil: <b>Note:</b> NetBIOS is an API, not a
network protocol.</td></tr></table>

So SMB (and others) ran on top of NetBIOS the way modern network code runs on
sockets.  That meant that it was possible to replace the underlying network
system as long as the NetBIOS API, and the semantics it imposed, were
maintained.

...and that's what NBT was meant to do.  It maps the NetBIOS API onto TCP and
UDP.

So the awkward bit is that, for years now, there have been promises that the
NBT layer will "go away".  Perhaps some day it will, but for now there are a
handful of reasons that creating an NBT reference implementation is a nominally
acceptable idea:

- Not many people know how NBT works.\
  Very few people even \e care how it works but, as it turns out, yours
  truly is one of those few.  A reference implementation provides yet
  another opportunity to share the ancient lore.

- NBT is relatively small and simple.\
  It is also quite well documented, even it's oddities.

- NBT is actually fairly well designed.\
  Okay, so the Datagram Service is a bit weird, but most implementations just
  bypass the hairy bits.  NBT provides namespace management, unicast,
  multicast, and broadcast datagrams, and session services.

- NBT does still get used.\
  It is still seen in the wild; it's not extinct.  Also, there was some old
  code lying around that could be fairly easily repurposed for this
  sub-project.  What the heck, eh?

- NBT provides a second transport.\
  An additional transport opens up some testing opportunities.

- NBT makes me happy.

Licenses
--------

The NBT subsystem is part of Zambezi, so individual parts will be licensed
under the LGPLv3, while others (runnable programs) will be under the
AGPLv3.  Again, we plan on adding additional licenses so that this code can
be used as a reference implementation.

Modules
-------

What we're working on, and what's been completed so far:

- [x] NBT name encoding
      ([`nbt_names`](https://ubiqx.gitlab.io/zambezi/nbt__names_8h.html))
- [x] NBT Name Service
      ([`nbt_nbns`](https://ubiqx.gitlab.io/zambezi/nbt__nbns_8h.html))
- [ ] NBT Datagram Service (low priority)
- [x] NBT Session Service
      ([`nbt_nbss`](https://ubiqx.gitlab.io/zambezi/nbt__nbss_8h.html))
- [ ] NBT Engine - We'll need code for a dæmon toolkit to make this all go.
- [ ] NBT Tools - Port old `libcifs` NBT tools to this codebase.

References
----------

<dl>
  <dt>[[IMPCIFS]](http://www.ubiqx.org/cifs/)</dt>
    <dd>
    Hertel, Christopher R., "Implementing CIFS - The Common Internet File
    System", Prentice Hall, August 2003, ISBN:013047116X
    </dd>
  <dt>[[MS-NBTE]](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-nbte/3461cfa8-3d28-4fa3-8163-131bf1046fa3)</dt>
  <dd>
    Microsoft Corporation, "NetBIOS over TCP (NBT) Extensions"
  </dd>
  <dt>[[NBGUIDE]](https://web.archive.org/web/20170724042731/http://www.netbiosguide.com:80/)</dt>
    <dd>
    Winston, Gavin, "NetBIOS Specification", 1998-2012 (Archived)
    </dd>
  <dt>[[RFC1001]](http://www.ietf.org/rfc/rfc1001.txt)</dt>
    <dd>
    Network Working Group, "Protocol Standard for a NetBIOS Service on a
    TCP/UDP Transport: Concepts and Methods", STD 19, RFC 1001, March 1987
    </dd>
  <dt>[[RFC1002]](http://www.ietf.org/rfc/rfc1002.txt)</dt>
    <dd>
    Network Working Group, "Protocol Standard for a NetBIOS Service on a
    TCP/UDP Transport: Detailed Specifications", STD 19, RFC 1002, March 1987
    </dd>
  <dt>[[SMBURL]](https://tools.ietf.org/html/draft-crhertel-smb-url-12)</dt>
    <dd>
    Expired (but still relevant) draft SMB URL specification.
    </dd>
  <dt>[[STD19]](https://www.rfc-editor.org/info/std19)</dt>
    <dd>
    [RFC1001] and [RFC1002] together comprise IETF STD 19.
    </dd>
</dl>

____
$Id: README.md; 2021-03-03 13:24:37 -0600; crh$\
ℵ⟚℺⟛⟟
