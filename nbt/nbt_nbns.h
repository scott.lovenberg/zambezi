#ifndef NBT_NBNS_H
#define NBT_NBNS_H
/* ========================================================================== **
 *                                 nbt_nbns.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description:
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_nbns.h; 2020-11-02 19:46:48 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    nbt_nbns.h
 * @author  Christopher R. Hertel
 * @brief   NBT NetBIOS Name Service utilities.
 * @date    7 Oct 2020
 * @version \$Id: nbt_nbns.h; 2020-11-02 19:46:48 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  Utilities needed to create the following:
 *  - An NBT Name Service client that can claim and query names, etc.
 *  - A local NBT name table, including locally owned names and a resolved
 *    names cache.
 *  - A NetBIOS Name Server (NBNS).
 *
 *  The workings of the NBT NetBIOS Name Service, including message formats
 *  and processing rules, are explained in the NBT section of [IMPCIFS].
 *  There Name Service isn't particularly big, but it is a bit complex and
 *  sometimes subtle.  A single flag bit can entirely change the meaning of
 *  a message.
 *
 * @see <a href="http://ubiqx.org/cifs/NetBIOS.html">[IMPCIFS; 1] NBT:
 *  NetBIOS over TCP/IP</a>
 * @see <a href="http://www.ietf.org/rfc/rfc1001.txt">[RFC1001]: Protocol
 *  Standard for a NetBIOS Service on a TCP/UDP Transport: Concepts
 *  and Methods</a>
 * @see <a href="http://www.ietf.org/rfc/rfc1002.txt">[RFC1002]: Protocol
 *  Standard for a NetBIOS Service on a TCP/UDP Transport: Detailed
 *  Specifications</a>
 * @see
 *  <a href="@msdocs/ms-nbte/3461cfa8-3d28-4fa3-8163-131bf1046fa3">[MS-NBTE]:
 *  NetBIOS over TCP (NBT) Extensions</a>
 */

#include <stdbool.h>      /* The boolean type is used in <nbt_nsMsg>. */

#include "nbt_common.h"   /* All the things.                          */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 */

/**
 * @def nbt_nsHEADER_LEN
 *  The length of a NBT Name Service message header in octets; always 12.
 */
#define nbt_nsHEADER_LEN 12


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum  nbt_nsHdrFlags
 * @brief Name Service header \c Flags subfield values and field masks.
 * @details
 *  The NBT Name Service message header is an array of 16-bit unsigned
 *  integers.  The array has six elements, the second of which contains
 *  a set of message flags, as follows:<pre>
 *    | 0 | 1 2 3 4 | 5 6 7 8 9 A B | C D E F |
 *    |---+---------+---------------+---------+
 *    | R | OpCode  |   NM_Flags    |  RCode  |</pre>
 *
 *  The set of values defined by this enumerated type can be used to read,
 *  write, and clear the Flags values.
 *
 * @see <a href="http://ubiqx.org/cifs/NetBIOS.html#NBT.3.1.4">[IMPCIFS;
 *      1.3.1.4]: Name Service Packet Headers</a>
 * @see <a href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.2.1">[IMPCIFS;
 *      1.4.2.1]: Name Service Headers</a>
 * @see <a href="https://www.ietf.org/rfc/rfc1002.html#section-4.2.1.1"
 *      >[RFC1002; 4.2.1.1]: HEADER</a>
 *
 * @var nbt_nsRCODE_POS_RSP
 *  In request messages, the `RCode` field \b must be zero.  In a response,
 *  an `RCode` value of zero indicates success.
 * @var nbt_nsRCODE_FMT_ERR
 *  The receiver did not like your message.  Something was wrong with the
 *  packet format.
 * @var nbt_nsRCODE_SRV_ERR
 *  This error can occur if the initiating request was sent to an NBNS
 *  (NetBIOS Name Server) that is having difficulties.
 * @var nbt_nsRCODE_NAM_ERR
 *  The requested name does not exist in the queried table.
 * @var nbt_nsRCODE_IMP_ERR
 *  Returned by an NBNS if it recieves an unsolicited Name Update Request
 *  from a client.  See
 *  <a href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.1.2">[IMPCIFS;
 *     1.4.3.1.2]: Unicast (NBNS) Name Registration</a>
 * @var nbt_nsRCODE_RFS_ERR
 *  The NBNS has made a policy decision not to register the name.
 * @var nbt_nsRCODE_ACT_ERR
 *  The name is in use by another system.  You can't have it.
 * @var nbt_nsRCODE_CFT_ERR
 *  More than one node currently claims ownership of the name.
 * @var nbt_nsRCODE_MASK
 *  Bitmask for valid `RCode` values.
 *
 * @var nbt_nsAA_BIT
 *  This should be zero in registration requests and name queries.  If set in
 *  a response, it indicates that the responding node is either the owner of
 *  the name (thus, authoritative) or the NBNS.  This bit will also be set in
 *  a Name Conflict Demand message (<a
 *  href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.6">[IMPCIFS; 1.4.3.6]:
 *  Name Conflict Demand</a>).
 * @var nbt_nsTC_BIT
 *  Used to indicate that a list of IP addresses returned from the NBNS was
 *  truncated because the list was too long to fit within a single Name
 *  Service message.  In practice, this bit is ignored.  See <a
 *  href="http://ubiqx.org/cifs/NetBIOS.html#NBT.7.2">[IMPCIFS; 1.7.2]:
 *  Twenty-five IPs or Less</a>.
 * @var nbt_nsRD_BIT
 *  This bit only has meaning in request messages.  The standard behavior,
 *  however, is to copy the value of this flag from the request into the
 *  coresponding reply.  This bit is a carryover from the Domain Name System
 *  (DNS), upon which the NBT Name Service is loosely based.  In requests,
 *  this bit is set or clear depending upon the type of request being sent.
 *  See <a href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.1.2">[IMPCIFS;
 *  1.4.3.2]: Name Query</a>.
 * @var nbt_nsRA_BIT
 *  This bit should only be set in messages sent from the NBNS.
 * @var nbt_nsB_BIT
 *  Indicates that the message is being sent as a broadcast to the local
 *  LAN only.
 * @var nbt_nsNMFLAG_MASK
 *  Bitmask used to isolate the NM_FLAGS bitfield.
 *
 * @var nbt_nsOPCODE_QUERY
 *  Request that the given name be resolved to an IPv4 address.  This OpCode
 *  is used to send the following message types:
 *  - Name Query Request
 *  - Positive Name Query Response
 *  - Negative Name Query Response
 *  - Node Status Request and Response
 *  - End-node Challenge Name Registration Response
 * @var nbt_nsOPCODE_REGISTER
 *  Request registration of the given name to IPv4 address mapping.  This
 *  OpCode used for the following messages:
 *  - Name Registration Request
 *  - Name Registration Response (positive and negative)
 *  - Name Overwrite Demand
 *  - Name Update Request
 *  - Name Conflict Demand
 * @var nbt_nsOPCODE_RELEASE
 *  Request release of a previously registered name.  This OpCode is also
 *  used to broadcast a Name Release Demand.
 * @var nbt_nsOPCODE_WACK
 *  Wait for Acknowlegement.  This message is sent by an NBNS to ask a
 *  client to hold its breath for a few seconds.  This may be necessary if
 *  there is a name conflict or multihomed registration.
 * @var nbt_nsOPCODE_REFRESH
 *  Request that the NBNS refresh an already registered name.  This is also
 *  used to rebuild the NBNS database in the event of NBNS crash.  Due to a
 *  typo in [RFC1002], the OPCODE values 0x8 and 0x9 are considered
 *  equivalent and both mean NAME REFRESH REQUEST. 0x8 is more commonly used.
 * @var nbt_nsOPCODE_ALTREFRESH
 *  Due to a typo in [RFC1002], the OPCODE values 0x8 and 0x9 are both used
 *  to indicate a Refresh Request.  This value, 0x9, is less commonly used.
 *  The response to a Refresh Request is a Registration Response.
 * @var nbt_nsOPCODE_MULTIHOMED
 *  This is an extension to [STD19].  This is a Name Registration Request
 *  with a twist: it may be sent from multiple interfaces on the same node.
 * @var nbt_nsOPCODE_MASK
 *  Masks just the valid OpCode values.
 *
 * @var nbt_nsR_BIT
 *  Must be zero in a request message, and one in a response.
 *
 * @var nbt_nsHEADER_FLAGMASK
 *  This bitmask indicates only the bits within the Flags field that are
 *  not reserved.  All other bits should be zero.
 */
typedef enum
  {
  /* Return Codes.  */
  nbt_nsRCODE_POS_RSP     = 0x0000, /**< Positive Response;     \c 0x0  */
  nbt_nsRCODE_FMT_ERR     = 0x0001, /**< Format Error;          \c 0x1  */
  nbt_nsRCODE_SRV_ERR     = 0x0002, /**< Server Failure;        \c 0x2  */
  nbt_nsRCODE_NAM_ERR     = 0x0003, /**< Name Error;            \c 0x3  */
  nbt_nsRCODE_IMP_ERR     = 0x0004, /**< Unsupported Request;   \c 0x4  */
  nbt_nsRCODE_RFS_ERR     = 0x0005, /**< Refused;               \c 0x5  */
  nbt_nsRCODE_ACT_ERR     = 0x0006, /**< Active Error;          \c 0x6  */
  nbt_nsRCODE_CFT_ERR     = 0x0007, /**< Name in Conflict;      \c 0x7  */
  nbt_nsRCODE_MASK        = 0x0007, /**< `RCode` subfield mask; \c 0x7  */

  /* Name service Message Flags.  */
  nbt_nsAA_BIT            = 0x0400, /**< Authoritative Answer   */
  nbt_nsTC_BIT            = 0x0200, /**< Truncation Flag        */
  nbt_nsRD_BIT            = 0x0100, /**< Recursion Desired      */
  nbt_nsRA_BIT            = 0x0080, /**< Recursion Available    */
  nbt_nsB_BIT             = 0x0010, /**< Broadcast Flag         */
  nbt_nsNMFLAG_MASK       = 0x0790, /**< NM_FLAGS Subfield mask */

  /* OpCodes */
  nbt_nsOPCODE_QUERY      = 0x0000, /**< Query        */
  nbt_nsOPCODE_REGISTER   = 0x2800, /**< Registration */
  nbt_nsOPCODE_RELEASE    = 0x3000, /**< Release      */
  nbt_nsOPCODE_WACK       = 0x3800, /**< WACK         */
  nbt_nsOPCODE_REFRESH    = 0x4000, /**< Refresh      */
  nbt_nsOPCODE_ALTREFRESH = 0x4800, /**< Refresh      */
  nbt_nsOPCODE_MULTIHOMED = 0x7800, /**< Multi-homed  */
  nbt_nsOPCODE_MASK       = 0x7800, /**< OPCODE Mask  */

  /* Request/Response bit.  */
  nbt_nsR_BIT             = 0x8000, /**< Request/Response bit.  */

  /* Flags field mask. */
  nbt_nsHEADER_FLAGMASK   = 0xFF97  /**< Valid \c Flags field bitmask.  */
  } nbt_nsHdrFlags;

/**
 * @enum nbt_nsSubrecFlag
 * @details
 *  These flags are used to indicate which subrecords a Name Service
 *  message has or should have.  These flags are passed to/from functions
 *  to indicate which subrecords are (to be) contained within the packet.
 *
 * \b Notes
 *  - These constants are part of this module, not part of the NBT protocol.
 *  - The Name Server Resource Record, also known as an Authority Resource
 *    Record, is defined in [RFC1002] but is listed here as "never used".
 *    There is, of course, a story behind that.  This record type is listed
 *    as being used in a Redirect Name Query Response.  The Redirect, in
 *    turn, may only be sent by an NBNS and only to redirect a query to
 *    another NBNS.  There are no known implementations of this feature.  <a
 *    href="https://www.ietf.org/rfc/rfc1002.html#section-4.2.15">[RFC1002;
 *    4.2.15]</a> states that "This is an optional packet for the NBNS."
 *    See: <a href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.2.3">[IMPCIFS;
 *    1.4.3.2.3]: The Redirect Name Query Response</a>
 *
 * @var nbt_nsQUERYREC
 *  Indicates that a Query Record is included in the message.  NBT NS
 *  messages have at most one Query Record (QDCOUNT = 0|1).
 * @var nbt_nsANSREC
 *  Indicates that an Answer Record is included in the message.  NBT NS
 *  messages have at most one Answer Record (ANCOUNT = 0|1).
 * @var nbt_nsNSREC
 *  Indicates that an Authority Record is included in the message.  As with
 *  other record types, a maximum of one Authority Record is used per
 *  message (NSCOUNT = 0|1).  However, the only NBT NS message that may
 *  contain an Authority Record is the `REDIRECT NAME QUERY RESPONSE`, which
 *  really isn't used.  Really.
 * @var nbt_nsADDREC
 *  Indicates that an Additional Record is included in the message.  NBT NS
 *  messages have at most one Additional Record (ARCOUNT = 0|1).
 */
typedef enum
  {
  nbt_nsQUERYREC  = 0x01,  /**< Query Record.                     */
  nbt_nsANSREC    = 0x02,  /**< Answer Record.                    */
  nbt_nsNSREC     = 0x04,  /**< Name Server Record (never used).  */
  nbt_nsADDREC    = 0x08   /**< Additional Record.                */
  } nbt_nsSubrecFlag;

/**
 * @enum  nbt_nsQueryTag
 * @brief Query Type and Query Class constants.
 *
 *  These constants are used in Question Records to indicate the purpose
 *  of the included Name.  In most messages, the Type will be `NB`.  In
 *  Node Status Queries, the Type will be `NBSTAT`.
 *
 * @var nbt_nsQTYPE_NB
 *  Indicates that the message is part of the regular name registration and
 *  resolution system.
 * @var nbt_nsQTYPE_NBSTAT
 *  The name is being used in an Node Status Query exchange.  Node Status
 *  Queries may only be sent to end nodes.  They are used to query the end
 *  node's local NetBIOS name table and gather other node information.
 *
 * @var nbt_nsQCLASS_IN
 * Internet query class.  This is the only class defined for NBT.
 */
typedef enum
  {
  /* Query Type */
  nbt_nsQTYPE_NB      = 0x0020, /**< Name Query         */
  nbt_nsQTYPE_NBSTAT  = 0x0021, /**< Node Status Query  */
  nbt_nsQTYPE_MASK    = 0x0021, /**< Query Type Bitmask */
  /* Query Class */
  nbt_nsQCLASS_IN     = 0x0001  /**< Internet Class     */
  } nbt_nsQueryTag;

/**
 * @enum  nbt_nsRRecTag
 * @brief Resource Record Type and Class tags, and some useful constants.
 *
 * @var nbt_nsLSP
 *  The one and only Label String Pointer value ever used in NBT packets.
 *  A Label String Pointer (LSP) is used to deduplicate NBT names sent
 *  within the same message.
 *
 * @var nbt_nsRRTYPE_A
 *  The IPv4 Address Resource Record type is only used in the `REDIRECT NAME
 *  QUERY RESPONSE` which, in turn, is probably never used.
 * @var nbt_nsRRTYPE_NS
 *  The Name Server Resource Record type is only used in the `REDIRECT NAME
 *  QUERY RESPONSE` which, in turn, is probably never used.
 * @var nbt_nsRRTYPE_NULL
 *  <a href="https://www.ietf.org/rfc/rfc1002.html#section-4.2.1.3">[RFC1002;
 *  4.2.1.3]</a> states that this type is used in the `WAIT FOR
 *  ACKNOWLEDGEMENT RESPONSE` (`WACK`) message, but it is also specified for
 *  the `NEGATIVE NAME QUERY RESPONSE` in <a
 *  href="https://www.ietf.org/rfc/rfc1002.html#section-4.2.14">[RFC1002;
 *  4.2.14]</a>.  It is probably best to accept either `NB` or `NULL` for
 *  these two message types.
 *  - Windows systems (and very old versions of Samba) send an `RR_TYPE` of
 *    `NB` (0x0020).  Samba (since version 3) correctly returns the `NULL`
 *    type (0x000A).
 *  - In <a
 *    href="https://www.ietf.org/rfc/rfc1002.html#section-4.2.16">[RFC1002;
 *    4.2.14]</a>, the diagram shows the `NULL` type but gives the hex value
 *    of the `NB` type (0x0020).  `NULL` (0x000A) is the correct type value.
 * @var nbt_nsRRTYPE_NB
 *  NetBIOS general Name Service Resource Record.
 * @var nbt_nsRRTYPE_NBSTAT
 *  NetBIOS Node Status Resource Record.
 * @var nbt_nsRRTYPE_MASK
 *  Resource Record Type bitmask.
 *
 * @var nbt_nsRRCLASS_IN
 *  Internet Rescource Record class.  The Internet Class is the only class
 *  defined for NBT.
 */
typedef enum
  {
  /* Label String Pointer */
  nbt_nsLSP             = 0xC00C, /**< Pointer to offset 12.  */
  /* Resource Record Type */
  nbt_nsRRTYPE_A        = 0x0001, /**< IP Addr Resource Record (unused)     */
  nbt_nsRRTYPE_NS       = 0x0002, /**< Name Server Resource Record (unused) */
  nbt_nsRRTYPE_NULL     = 0x000A, /**< NULL Resource Record                 */
  nbt_nsRRTYPE_NB       = 0x0020, /**< NetBIOS Resource Record              */
  nbt_nsRRTYPE_NBSTAT   = 0x0021, /**< NB Status Response Resource Record   */
  nbt_nsRRTYPE_MASK     = 0x002B, /**< Resource Record Type Mask            */
  /* Resource Record Class */
  nbt_nsRRCLASS_IN      = 0x0001  /**< Internet Class */
  } nbt_nsRRecTag;

/**
 * @enum  nbt_nsRDataFlags
 * @brief RDATA flag values.
 *
 * @var nbt_nsGROUP_BIT
 *  Identifies a NetBIOS group name if set; clear for uniqe names.
 * @var nbt_nsONT_B
 *  The node is a B-mode node.
 * @var nbt_nsONT_P
 *  The node is a P-mode node.
 * @var nbt_nsONT_M
 *  The node is an M-mode node.
 * @var nbt_nsONT_H
 *  The node is an H-mode node. \n
 *  H-mode is not specified in [STD19]; it was added later as an extension.
 * @var nbt_nsONT_MASK
 *  Owner Node Type mask.
 * @var nbt_nsNBFLAG_MASK
 *  NBFlag mask.
 */
typedef enum
  {
  nbt_nsGROUP_BIT   = 0x8000, /**< Group indicator      */
  nbt_nsONT_B       = 0x0000, /**< Broadcast node       */
  nbt_nsONT_P       = 0x2000, /**< Point-to-point node  */
  nbt_nsONT_M       = 0x4000, /**< Mixed mode node      */
  nbt_nsONT_H       = 0x6000, /**< Hybrid mode node     */
  nbt_nsONT_MASK    = 0x6000, /**< Owner Node Type mask */
  nbt_nsNBFLAG_MASK = 0xE000  /**< Full flags mask      */
  } nbt_nsRDataFlags;

/**
 * @enum  nbt_nsNameFlags
 * @brief Node Status Reply \c RDATA.NODE_NAME.NAME_FLAGS bits.
 *
 * @var nbt_nsDRG
 *  If set, name is in the process of being released.
 * @var nbt_nsCNF
 *  If set, the name is currently in conflict.
 * @var nbt_nsACT
 *  The name is active.  In practice, this name is always set.
 * @var nbt_nsPRM
 *  Indicates that the name is the machine's permanent name.
 *  In practice, this bit is not used.
 * @var nbt_nsSTATE_MASK
 *  Bitmask for `(DRG | CNF | ACT | PRM)`.
 * @var nbt_nsNAMEFLAGS_MASK
 *  `(#nbt_nsNBFLAG_MASK | #nbt_nsSTATE_MASK)`. \n See \c #nbt_nsRDataFlags.
 */
typedef enum
  {
  nbt_nsDRG             = 0x1000, /* Deregister state     */
  nbt_nsCNF             = 0x0800, /* Conflict state       */
  nbt_nsACT             = 0x0400, /* Active state         */
  nbt_nsPRM             = 0x0200, /* Permanent state      */
  nbt_nsSTATE_MASK      = 0x1E00, /* State bits mask      */
  nbt_nsNAMEFLAGS_MASK  = 0xFE00  /* Full NAME_FLAGS mask */
  } nbt_nsNameFlags;

/**
 * @enum  nbt_nsMsgType
 * @brief Used to idenify the Name Service message type and format.
 * @details
 *  This list represents the complete set of message types defined in
 *  RFC1002, plus at least one more that isn't.  The Multi-homed
 *  Registration Request is an extension.
 *
 * \b Notes
 *  - There are 19 message types listed (plus the NULL message type).
 *    + The Multi-Homed Name Registration was defined after [STD19] was
 *      published.
 *    + There is no good reason to support the Redirect Name Query Response
 *      (\c #nbt_nsNAME_QUERY_REPLY_REDIR).  However, the #nbt_nsParseMsg()
 *      function will detect this message type.  A client receiving a
 *      Redirect should intepret it as a Negative Name Query Response.
 *    + The Name Overwrite Demand and the Name Update Request are almost
 *      identical.  The difference is in the \c 'B' bit:
 *      * The Name Overwrite Demand is sent as the final step in a
 *        successful \e broadcast name registration.
 *      * The Name Update Request is \e unicast to the NBNS after it has
 *        received and acted upon an End-Node Challenge.
 *      .
 *      See the \b !Alert box at the end of <a
 *      href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.1.2">[IMPCIFS;
 *      1.4.3.1.2]: Unicast (NBNS) Name Registration</a>.
 */
typedef enum
  {
  nbt_nsNULL_MSG_TYPE    = 0,   /**< NULL (unspecified message type)          */
  nbt_nsNAME_QUERY_REQST = 1,   /**< Name Query Request                       */
  nbt_nsNAME_QUERY_REPLY_POS,   /**< Positive Name Query Response             */
  nbt_nsNAME_QUERY_REPLY_NEG,   /**< Negative Name Query Response             */
  nbt_nsNAME_QUERY_REPLY_REDIR, /**< Redirect Name Query Response             */
  nbt_nsNODE_STATUS_REQST,      /**< Node Status Request                      */
  nbt_nsNODE_STATUS_REPLY,      /**< Node Status Response                     */
  nbt_nsNAME_REG_REQST,         /**< Name Registration Request                */
  nbt_nsNAME_OVERWRITE_DEMAND,  /**< Name Overwrite Demand                    */
  nbt_nsNAME_UPDATE_REQST,      /**< Name Update Request                      */
  nbt_nsNAME_REG_REPLY_POS,     /**< Positive Name Registration Response      */
  nbt_nsNAME_REG_REPLY_NEG,     /**< Negative Name Registration Response      */
  nbt_nsNAME_REG_CHALLENGE,     /**< End-node Challenge Registration Response */
  nbt_nsNAME_CONFLICT_DEMAND,   /**< Name Conflict Demand                     */
  nbt_nsNAME_RELEASE_REQST,     /**< Name Release Request and Demand          */
  nbt_nsNAME_RELEASE_REPLY_POS, /**< Positive Name Release Response           */
  nbt_nsNAME_RELEASE_REPLY_NEG, /**< Negative Name Release Response           */
  nbt_nsWACK_REPLY,             /**< Wait for ACKnowlegement                  */
  nbt_nsNAME_REFRESH_REQST,     /**< Name Refresh Request                     */
  nbt_nsMULTI_REG_REQST         /**< Multi-Homed Name Registration            */
  } nbt_nsMsgType;


/* -------------------------------------------------------------------------- **
 * Macros
 *  This set of macros reads the count fields from an NBT NS header.
 */

/**
 * @def     nbt_nsGetQDCOUNT( hdr )
 * @param   hdr   A pointer to an array of octets containing an NBT Name
 *                Service message header in wire format.
 * @returns Zero (0) if there are no Question Entries, or
 *          \c #nbt_nsQUERYREC to indicate one Question Entry.  The maximum
 *          number of question records in any NBT message is one (1).
 * @see \c #nbt_nsGetRmap()
 * @hideinitializer
 */
#define nbt_nsGetQDCOUNT( hdr ) ((hdr)[5] ? nbt_nsQUERYREC : 0)

/**
 * @def     nbt_nsGetANCOUNT( hdr )
 * @param   hdr   A pointer to an array of octets containing an NBT Name
 *                Service message header in wire format.
 * @returns Zero (0) if there are no Answer Resource Records, or
 *          \c #nbt_nsANSREC to indicate that one Answer Resource Record
 *          is present.  The maximum number of Answer Resource Records in
 *          any NBT message is one.
 * @see \c #nbt_nsGetRmap()
 * @hideinitializer
 */
#define nbt_nsGetANCOUNT( hdr ) ((hdr)[7] ? nbt_nsANSREC : 0)

/**
 * @def     nbt_nsGetNSCOUNT( hdr )
 * @param   hdr   A pointer to an array of octets containing an NBT Name
 *                Service message header in wire format.
 * @returns Zero (0) if there are no Authority Resource Records, or
 *          \c #nbt_nsNSREC to indicate the presence of one Authority
 *          Resource Record.  The maximum number of Authority Resource
 *          Records in any NBT message is one...but see the note, below.
 * @see \c #nbt_nsGetRmap()
 *
 * \b Note: The only packet that uses the \c NSCOUNT field is the
 *          <a href="https://tools.ietf.org/html/rfc1002#section-4.2.15"
 *          >`REDIRECT NAME QUERY RESPONSE`</a>, which is never used.
 * @hideinitializer
 */
#define nbt_nsGetNSCOUNT( hdr ) ((hdr)[9] ? nbt_nsNSREC : 0)

/**
 * @def     nbt_nsGetARCOUNT( hdr )
 * @param   hdr   A pointer to an array of octets containing an NBT Name
 *                Service message header in wire format.
 * @returns Zero (0) if there are no Additional Resource Records, or
 *          \c #nbt_nsADDREC to indicate the presence of one Additional
 *          Resource Record.  The maximum number of Additional Resource
 *          records in any NBT message is one.
 * @see \c #nbt_nsGetRmap()
 * @hideinitializer
 */
#define nbt_nsGetARCOUNT( hdr ) ((hdr)[11] ? nbt_nsADDREC : 0)

/**
 * @def     nbt_nsGetRmap( hdr )
 * @param   hdr   A pointer to an array of octets containing an NBT Name
 *                Service message header in wire format.
 * @returns A bitmap indicating which record types are included in the
 *          message.  We call this a "Record Map".
 * @details
 *  The \c nbt_nsGet*COUNT() macros take a short-cut.  They only look at the
 *  second byte of each 2-byte field.  This is safe because the packet
 *  descriptions in [RFC1002] only allow values of 0 and 1.
 * @hideinitializer
 */
#define nbt_nsGetRmap( hdr ) ((uint8_t)( nbt_nsGetQDCOUNT( hdr ) \
                                       | nbt_nsGetANCOUNT( hdr ) \
                                       | nbt_nsGetNSCOUNT( hdr ) \
                                       | nbt_nsGetARCOUNT( hdr ) ))


/* -------------------------------------------------------------------------- **
 * Typedefs
 *//**
 * @struct  nbt_nsMsg
 * @brief   Generalized structure to represent NBT Name Service messages.
 * @details
 *  Name Service messages are built from common parts, so a single structure
 *  can be used to handle the messages we send or receive.  Some messages
 *  will include additional "Resource Data" (RDATA), which is specific to
 *  the message Type.
 *
 * /b Notes
 *  - Neither the `QUESTION_CLASS` nor the `RR_CLASS` values are included
 *    in this structure because only the \c IN (Internet) class is used in
 *    NBT Name Service messages.
 *  - Regarding the \c #nbt_nsMsg::lsp field:
 *    + During parsing, if \c RR_name field is found to be a Label String
 *      Pointer (LSP), the \c lsp field will be set to \c true.  If the
 *      \c lsp field is true, then the \c RR_name and \c RR_name_len fields
 *      must be ignored (though they will be set to rational values).
 *    + When packing a message, if \c #nbt_nsMsg::lsp is \c true then a
 *      two-byte Label String Pointer (LSP) will be written into the output
 *      buffer instead of the \c RR_name.
 */
typedef struct
  {
  nbt_nsMsgType type;         /**< Message type indicator       */
  uint16_t      tid;          /**< Transaction Identifier       */
  uint16_t      flags;        /**< Message OpCode and Flags     */
  uint8_t       rmap;         /**< Subsection record map        */
  uint8_t      *Q_name;       /**< Question Name                */
  uint8_t       Q_name_len;   /**< Question Name Length         */
  uint16_t      Q_type;       /**< Question Type (`NB | NBSTAT`)*/
  bool          lsp;          /**< Use an LSP for the RR Name?  */
  uint8_t      *RR_name;      /**< Resource Record Name         */
  uint8_t       RR_name_len;  /**< Resource Record Name Length  */
  uint16_t      RR_type;      /**< Resource Record Type         */
  uint32_t      ttl;          /**< Resource Record Time to Live */
  uint8_t      *rdata;        /**< Additional Resource Data     */
  uint16_t      rdata_len;    /**< Octet length of the RDATA    */
  } nbt_nsMsg;

/**
 * @struct  nbt_nsAddrEntry
 * @brief   NetBIOS general Name Service Resource Record structure.
 * @details
 *  There are ten message types that make use of the Address Entry
 *  sub-record.  These include the (non-RFC) multi-homed registration
 *  message.  In all but one case, only on Address Entry structure is
 *  included in the message.  The exception is the Positive Name Query
 *  Response message, which will include an array of one or more Address
 *  Entry records.
 *
 *  On the wire, an Address Entry is composed of a 16-bit \c NB_FLAGS field
 *  followed by a 32-bit IPv4 address.  The latter is given as an unsigned
 *  integer in network byte order.
 *
 *  The \c NB_FLAGS field provides information about the associated
 *  name:<pre>
 *    | 0 | 1 2 | 3 4 5 6 7 8 9 A B C D E F |
 *    |---+-----+---------------------------+
 *    | G | ONT |        RESERVED-MB0       |
 *
 *    G:    Group Bit - 1 (true) if the name is a Group name, else 0.
 *    ONT:  Owner Node Type:
 *          0 = B node
 *          1 = P node
 *          2 = M node
 *          3 = H node (see below)</pre>
 *  [RFC1002] designates the \c ONT value of 3 as "Reserved for future use".
 *  It is now used to identify the Hybrid (H) node type.
 *
 * @see \c #nbt_nsRDataFlags
 */
typedef struct
  {
  uint16_t NB_flags;  /**< `NB_FLAGS`.    */
  uint32_t NB_addr;   /**< IPv4 Address.  */
  } nbt_nsAddrEntry;

/**
 * @struct  nbt_nsNodeNameEntry
 * @brief   Node Status Response Name Entry record.
 * @details
 *  Node Status Response messages contain an array of Name Entries, each
 *  representing a name registered by the responding end node.  We divide
 *  each Name Entry into three subfields:
 *  - The 15-octet NetBIOS name (which may include trailing padding).
 *  - A single octet that indicates the "type" of the name.
 *  - A 2-octet Flags field.
 *
 * @var nbt_nsNodeNameEntry::name
 *  This is a pointer to a length-delimited array of octets.  The name is
 *  16 octets in length, but only the first 15 octets should be used.
 *  [STD19] specifies a 16-octet name.  By convention, however, the 16th
 *  octet is reserved to indicate the way in which the name is used at the
 *  application layer.
 * @var nbt_nsNodeNameEntry::type
 *  The 16th octet of the NetBIOS name, sometimes referred to as the "suffix"
 *  or the "type byte".
 * @var nbt_nsNodeNameEntry::flags
 *  A 16-bit set of flags.  This field includes the `RDATA NB_FLAGS` with an
 *  additional set used to indicate the state of the name in the name table
 *  of the responding node.
 *  - \c #nbt_nsRDataFlags: The \c GROUP bit indicates whether the name is
 *    a unique or group (shared) name.  The ONT subfield is a 2-bit field
 *    that indicates whether the owning node is operating in B, P, M, or H
 *    mode.
 *  - \c #nbt_nsNameFlags:  Names are typically ACTive.  They may also be
 *    in the process of being deregistered (DRG), or may be in conflict
 *    (CNF).  In practice, the Permanent Name flag (PRM) is not used (though
 *    it could be).
 */
typedef struct
  {
  uint8_t  *name;         /**< Pointer to a NetBIOS name.     */
  uint8_t   type;         /**< NetBIOS Name Type identifier.  */
  uint16_t  flags;        /**< Name Attribute flags.          */
  } nbt_nsNodeNameEntry;

/**
 * @struct  nbt_nsNodeStats
 * @brief   Node Status Response RDATA structure.
 * @details
 *  A Node Status Response message includes a list of registered names
 *  followed by a statistics blob.  The list of names is prefixed by a
 *  single octet that provides the count of entries in the list.  This
 *  structure is used to receive the first breakdown of the Node Status
 *  Response \c RDATA blob.
 *
 * @var nbt_nsNodeStats::name_array
 *  Each NODE_NAME entry contains the formatted NetBIOS name (with padding
 *  and suffix), plus a set of name attributes.  See \c #nbt_nsNodeNameEntry.
 * @var nbt_nsNodeStats::statistics
 *  The first six bytes of the \c STATISTICS blob should contain the Ethernet
 *  MAC address of the interface from which the response was sent.  Samba,
 *  however, simply zero-fills those bytes.  Windows and Samba both zero-fill
 *  the remaineder of the \c STATISTICS blob.  See <a
 *  href="https://tools.ietf.org/html/rfc1002#section-4.2.18">[RFC1002;
 *  4.2.18]: `NODE STATUS RESPONSE`</a> for the intended data layout, but
 *  note that only the \c UNIT_ID field is used in practice.
 */
typedef struct
  {
  uint8_t  num_names;   /**< The count of names in the \c NODE_NAME Array.  */
  uint8_t *name_array;  /**< A pointer to the \c NODE_NAME Array.           */
  uint16_t stat_size;   /**< The size, in bytes, of the \c STATISTICS Blob. */
  uint8_t *statistics;  /**< A pointer to the \c STATISTICS Blob.           */
  } nbt_nsNodeStats;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int nbt_nsParseMsg( uint8_t   *const msg,
                    size_t     const msgLen,
                    nbt_nsMsg *const nsMsg );

int nbt_nsParseAddrEntry( uint8_t         *const rdata,
                          const uint16_t         rdata_len,
                          nbt_nsAddrEntry *const nsAddrEntry );

int nbt_nsParseWACK( uint8_t   *const rdata,
                     const uint16_t   rdata_len,
                     uint16_t  *const rdFlags );

int nbt_nsParseStatus( uint8_t         *const rdata,
                       const uint16_t         rdata_len,
                       nbt_nsNodeStats *const nodeStat );

int nbt_nsParseNameEntry( uint8_t             *const entry,
                          nbt_nsNodeNameEntry *const nodeName );

int nbt_nsPackMsg( nbt_nsMsg *const nsMsg,
                   uint8_t   *const bufr,
                   size_t     const bSize );

int nbt_nsPackAddrEntry( nbt_nsAddrEntry *const nsAddrEntry,
                         uint8_t         *const bufr,
                         size_t           const bSize );

int nbt_nsPackWACK( uint16_t const rdFlags,
                    uint8_t *const bufr,
                    size_t   const bSize  );

int nbt_nsPackNameEntry( nbt_nsNodeNameEntry *const nodeName,
                         uint8_t             *const bufr,
                         size_t               const bSize );


/* =============================== nbt_nbns.h =============================== */
#endif /* NBT_NBNS_H */
