#ifndef NBT_COMMON_H
#define NBT_COMMON_H
/* ========================================================================== **
 *                                nbt_common.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Common definitions for the NBT subsystem.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_common.h; 2021-02-06 10:32:23 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    nbt_common.h
 * @author  Christopher R. Hertel
 * @brief   Common definitions for the NBT subsystem.
 * @date    26 Apr 2020
 * @version \$Id: nbt_common.h; 2021-02-06 10:32:23 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  This is the dumpster.
 *  All of the NBT subsystem thingies that need to go somewhere to be
 *  available everywhere will probably get put here.
 *
 * @todo
 *  - Update docs to explain the NBT sub-project.
 *    + Add notes to the main README.md and Zambezi.dox files.
 *  - Figure out if Doxygen can generate a sub-page from the main page
 *    to accomodate the NBT subsystem overview.
 *  - Port the `nbtquery` tool to this toolkit.
 */

#include <stdlib.h>   /* For NULL, and other generic things.  */
#include <stdint.h>   /* Standard integer types.              */
#include <string.h>   /* For memcpy(3), et al.                */


/* -------------------------------------------------------------------------- **
 * Doxygen Fudge
 *
 *  The following ifdef block forces Doxygen to produce documentation
 *  for the static functions defined in this header file, even though
 *  EXTRACT_STATIC is set to NO in the Doxyfile.  Note, in particular,
 *  that these declarations are neither static nor inline, which is
 *  incorrect.  Doxygen don't care.
 */

#ifdef DOXY_TEXT
uint16_t nbtget16( const uint8_t *const ptr );
uint32_t nbtget32( const uint8_t *const ptr );
uint8_t *nbtset16( uint8_t *ptr, const uint16_t val );
uint8_t *nbtset32( uint8_t *ptr, const uint32_t val );
uint8_t *nbtcpmem( void *dst, const void *src, size_t len );
#endif /* DOXY_TEXT */


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/** Error Classes
 *
 * @enum  nbt_errorClass
 * @brief Pre-defined error return values used in the NBT subsystem.
 * @details
 *  All error codes, except for \c #nbt_Success, are negative integer values.
 *  \c #nbt_Success is zero.
 *
 *  The error codes are divided into classes:
 *  - \b Info: Successful completion with added information.
 *  - \b Warning: Successful completion with a caveat that should be handled.
 *  - \b Error: An operational failure that really \e must be handled.
 */
typedef enum
  {
  nbt_errClassINFO =  0x0000,   /**< Informational codes start at 0.  */
  nbt_errClassWARN = -0x1000,   /**< Warnings start at -4096.         */
  nbt_errClassERR  = -0x2000,   /**< Errors start at -8192.           */
  nbt_errClassMASK =  0xF000    /**< Error Class mask.                */
  } nbt_errorClass;

/** Error Codes
 *
 * @enum  nbt_error
 * @details
 *  A set of pre-defined error codes for the NBT toolkit.\n
 *  Consistent naming of error codes is very useful when those errors
 *  can be passed up a call stack.
 *
 *  With the exception of \c #nbt_Success, which has a value of zero (0),
 *  all error codes are negative integers.
 *
 * @var nbt_Success
 *      Success.
 * @var nbt_infoGeneric
 *      Success (same as \c #nbt_Success).
 *
 * @var nbt_warnGeneric
 *      An unspecified warning.
 * @var nbt_warnContainsDot
 *      The given NetBIOS name includes a dot character ('.'), which some
 *      systems may interpret as indicating a DNS name rather than a NetBIOS
 *      name.
 * @var nbt_warnNonPrint
 *      The given scope string contains a label that includes a non-printing
 *      8-bit character, as determined using the \c isprint(3) function.
 * @var nbt_warnNonAlpha
 *      A non-alphabetic 8-bit character was detected where an alphabetic
 *      character was expected.  This typically refers to the first character
 *      of an NBT label within a scope Id.
 * @var nbt_warnNulByte
 *      A NUL byte ('\0') was encountered where it was not expected.  This
 *      typically refers to NUL bytes within (rather than terminating) a
 *      NetBIOS name.
 * @var nbt_warnInvalidChar
 *      An unexpected octet value was found in what should be an ASCII
 *      string.
 * @var nbt_warnNonAlphaNum
 *      A non-alphanumeric 8-bit character was detected where an alphanumeric
 *      character was expected.  This typically refers to the last character
 *      of an NBT label within a scope Id.
 * @var nbt_warnEmptyStr
 *      The empty string was found where a non-empty string was expected.
 * @var nbt_warnAsterisk
 *      An asterisk character ('*') was found where it was not expected.
 *      This typically refers to the first octet of a NetBIOS name.  With
 *      one exception, NetBIOS names should not contain a leading asterisk.
 * @var nbt_warnLenExceeded
 *      A length limit was exceeded.
 * @var nbt_warnUnknownKey
 *      TBD
 * @var nbt_warnDuplicateKey
 *      TBD
 *
 * @var nbt_errGeneric
 *      Unspecified error.
 * @var nbt_errNullInput
 *      An input parameter was NULL or otherwise missing.
 * @var nbt_errNameTooLong
 *      A name (a NetBIOS name or an NBT name) exceeded the specified maximum
 *      length.
 * @var nbt_errLeadingDot
 *      A stand-along scope Id must not begin with a dot ('.') character.
 * @var nbt_errDoubleDot
 *      A pair of dot characers in sequenc within a scope Id would be
 *      interpeted as an empty label.  Empty labels are allowed only at the
 *      end of the scope Id.
 * @var nbt_errEndDot
 *      A dot ('.') character at the end of a scope Id string would be
 *      interpreted as an empty label, but the end of the scope Id string is
 *      also interpreted as an empty label.  Thus the scope Id string must
 *      not end with a dot.
 * @var nbt_errScopeTooLong
 *      The complete scope Id exceeds the maximum length.
 * @var nbt_errBadLblFlag
 *      The label flags for a label in a fully encoded NBT name do not match
 *      the expected value.  Either the flag combination is incorrect, or
 *      a Label String Pointer (LSP) flag was not expected.
 * @var nbt_errOutOfBounds
 *      An offset value indicates a location outside of the given buffer.
 * @var nbt_errTruncatedBufr
 *      A value being extracted from a buffer exceeds the length of the buffer.
 * @var nbt_errBufrTooSmall
 *      The provided buffer is too small to accept the data being written
 *      into it.
 * @var nbt_errBadL1Value
 *      A syntax error was encountered when decoding an L1-encoded NetBIOS
 *      name.
 * @var nbt_errInvalidLblLen
 *      A label length within a fully-encoded NBT name has an incorrect value.
 * @var nbt_errInvalidPacket
 *      A syntactic error was discovered while handing a message.
 * @var nbt_errInvalidSSType
 *      The Session Service message has an unrecognized message type.
 * @var nbt_errInvalidSSLen
 *      The NBT Session Service message type and length are incompatible.
 * @var nbt_errBadCalledName
 *      The Called Name in a Session Service Request is incorrectly
 *      formatted.
 * @var nbt_errBadCallingName
 *      The Calling Name in a Session Service Request is incorrectly
 *      formatted.
 * @var nbt_errUnknownCommand
 *      An unrecognized command code was encountered.
 *
 * @var nbt_errTheEnd
 *      Sets a lower bound to the error code values.
 */
typedef enum
  {
  /* Success */
  nbt_Success           = (nbt_errClassINFO - 0),
  nbt_infoGeneric       = nbt_Success,

  /* Warnings */
  nbt_warnGeneric       = (nbt_errClassWARN - 0),
  nbt_warnContainsDot   = (nbt_errClassWARN - 1),
  nbt_warnNonPrint      = (nbt_errClassWARN - 2),
  nbt_warnNonAlpha      = (nbt_errClassWARN - 3),
  nbt_warnNulByte       = (nbt_errClassWARN - 4),
  nbt_warnInvalidChar   = (nbt_errClassWARN - 5),
  nbt_warnNonAlphaNum   = (nbt_errClassWARN - 6),
  nbt_warnEmptyStr      = (nbt_errClassWARN - 7),
  nbt_warnAsterisk      = (nbt_errClassWARN - 8),
  nbt_warnLenExceeded   = (nbt_errClassWARN - 9),
  nbt_warnUnknownKey    = (nbt_errClassWARN - 10),
  nbt_warnDuplicateKey  = (nbt_errClassWARN - 11),

  /* Errors */
  nbt_errGeneric        = (nbt_errClassERR - 0),
  nbt_errNullInput      = (nbt_errClassERR - 1),
  nbt_errNameTooLong    = (nbt_errClassERR - 2),
  nbt_errLeadingDot     = (nbt_errClassERR - 3),
  nbt_errDoubleDot      = (nbt_errClassERR - 4),
  nbt_errEndDot         = (nbt_errClassERR - 5),
  nbt_errScopeTooLong   = (nbt_errClassERR - 6),
  nbt_errBadLblFlag     = (nbt_errClassERR - 7),
  nbt_errOutOfBounds    = (nbt_errClassERR - 8),
  nbt_errTruncatedBufr  = (nbt_errClassERR - 9),
  nbt_errBufrTooSmall   = (nbt_errClassERR - 10),
  nbt_errBadL1Value     = (nbt_errClassERR - 11),
  nbt_errInvalidLblLen  = (nbt_errClassERR - 12),
  nbt_errInvalidPacket  = (nbt_errClassERR - 13),
  nbt_errInvalidSSType  = (nbt_errClassERR - 14),
  nbt_errInvalidSSLen   = (nbt_errClassERR - 15),
  nbt_errBadCalledName  = (nbt_errClassERR - 16),
  nbt_errBadCallingName = (nbt_errClassERR - 17),
  nbt_errUnknownCommand = (nbt_errClassERR - 18),

  /* The End */
  nbt_errTheEnd         = -0xFFFF
  } nbt_error;


/* -------------------------------------------------------------------------- **
 * Macros:
 */

/**
 * @def     nbt_errISERROR( E )
 * @param   E - An #nbt_error code.
 * @brief   Determine whether an #nbt_error code is in the Error class.
 * @returns True if \p E is in the Error class, else false.
 * @hideinitializer
 */
#define nbt_errISERROR( E ) \
        ( -nbt_errClassERR  == (nbt_errClassMASK & -(E)) ? true : false )

/**
 * @def     nbt_errISWARN( W )
 * @param   W - An #nbt_error code.
 * @brief   Determine whether an #nbt_error code is in the Warning class.
 * @returns True if \p W is in the Warning class, else false.
 * @hideinitializer
 */
#define nbt_errISWARN( W )  \
        ( -nbt_errClassWARN == (nbt_errClassMASK & -(W)) ? true : false )

/**
 * @def     nbt_errISINFO( I )\
 * @param   I - An #nbt_error code.
 * @brief   Determine whether an #nbt_error code is in the Info class.
 * @returns True if \p I is in the Information class, else false.
 * @hideinitializer
 */
#define nbt_errISINFO( I )  \
        ( -nbt_errClassINFO == (nbt_errClassMASK & -(I)) ? true : false )

/**
 * @def     nbt_errClass( E )
 * @param   E - An #nbt_error code.
 * @brief   Returns the #nbt_errorClass of the given error code.
 * @returns The error #nbt_errorClass of error code \p E.
 * @details
 *  This macro allows you to do things like this:
 *  \code{.c}
 *    switch( nbt_errClass(err) )
 *      {
 *      case nbt_errClassERR:  return( "Error: " );
 *      case nbt_errClassWARN: return( "Warning: " );
 *      case nbt_errClassINFO: return( "Info: " );
 *      }
 *    return( "Unknown Error Class: " );
 * \endcode
 * @hideinitializer
 */
#define nbt_errClass( E ) (-(nbt_errClassMASK & -(E)))

/* -------------------------------------------------------------------------- **
 * Inline Functions
 *
 * Read and write NBT 16- and 32-bit integers.
 *  This set of functions is copied verbatim from smb/smb_endian.h in the
 *  `smb` subdirectory of the Zambezi source tree.  We could (should?) just
 *  include that header, but we (I) want the nbt subsystem to be able to
 *  stand alone.
 */

static inline uint16_t nbtget16( const uint8_t *const ptr )
  /** Read a 16-bit unsigned integer from a buffer in network byte order.
   *
   * @param[in] ptr A pointer to an array of (at least) two bytes.
   *                These two bytes will be read as a 16-bit unsigned
   *                integer in network byte order.
   *
   * @returns   An unsigned 16-bit integer in host byte order.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   *  - In NBT, integers are transferred in network byte order.
   */
  {
  return( (uint16_t)(ptr[0] << 8 | ptr[1]) );
  } /* nbtget16 */

static inline uint32_t nbtget32( const uint8_t *const ptr )
  /** Read a 32-bit unsigned integer from a buffer in network byte order.
   *
   * @param[in] ptr A pointer to an array of (at least) four bytes.
   *                The four bytes will be read as a 32-bit unsigned
   *                integer in network byte order.
   *
   * @returns   An unsigned 32-bit integer in host byte order.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   *  - In NBT, integers are transferred in network byte order.
   *  - In SMB, only the framing header is sent in network byte order
   *    (big endian order).  This is a hold-over from the NBT Session
   *    Service, defined in RFC1001/1002.
   */
  {
  return( (uint32_t)(ptr[0] << 24 | ptr[1] << 16 | ptr[2] << 8 | ptr[3]) );
  } /* nbtget32 */

static inline uint8_t *nbtset16( uint8_t *ptr, const uint16_t val )
  /** Write a 16-bit unsigned integer to a buffer in network byte order.
   *
   * @param[in,out] ptr A pointer to an array of (at least) two bytes,
   *                    into which the integer value will be written.
   * @param[in]     val An integer value, in host byte order, to be
   *                    written to \p ptr in network byte order.
   *
   * @returns   A pointer to the location in \p ptr immediately following
   *            the two bytes that were written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  ptr[0] = (uint8_t)((val >>  8) & 0xFF);
  ptr[1] = (uint8_t)( val        & 0xFF);
  return( ptr + 2 );
  } /* nbtset16 */

static inline uint8_t *nbtset32( uint8_t *ptr, const uint32_t val )
  /** Write a 32-bit unsigned integer to a buffer in network byte order.
   *
   * @param[in,out] ptr A pointer to an array of (at least) four bytes,
   *                    into which the integer value will be written.
   * @param[in]     val An integer value, in host byte order, to be
   *                    written to \p ptr in network byte order.
   *
   * @returns   A pointer to the location in \p ptr immediately following
   *            the four bytes that were written.
   *
   * \b Notes
   *  - This function is defined as both \c static and \c inline.
   */
  {
  ptr[0] = (uint8_t)((val >> 24) & 0xFF);
  ptr[1] = (uint8_t)((val >> 16) & 0xFF);
  ptr[2] = (uint8_t)((val >>  8) & 0xFF);
  ptr[3] = (uint8_t)( val        & 0xFF);
  return( ptr + 4 );
  } /* nbtset32 */

static inline uint8_t *nbtcpmem( void *dst, const void *src, size_t len )
  /** Copy the contents of one memory area to another memory area.
   *
   * @param[in] dst A pointer indicating the destination buffer into which
   *                the copy will be made.
   * @param[in] src A pointer to the source buffer from which the copy will
   *                be made.
   * @param[in] len The number of bytes to copy.
   *
   * @returns   A pointer to the byte position immediately following the
   *            copied data.  That is, \p dst + \p len.
   *
   * \b Notes
   *  - The copy is performed using the \c memcpy(3) function.  Therefore,
   *    as with \c memcpy(3), the \p dst and \p src memory areas must not
   *    overlap.
   *  - This function is essentially the same as the GNU \c mempcpy(3)
   *    function, except that it returns a <tt>uint8_t *</tt> pointer.
   *    This function was written only because \c mempcpy(3) is not standard.
   *
   * @see <a href="@posix/functions/memcpy.html">memcpy(3)</a>
   * @see <a href=
   *   "https://www.gnu.org/software/gnulib/manual/html_node/mempcpy.html">GNU
   *   mempcpy(3)</a>
   */
  {
  return( (uint8_t *)memcpy( dst, src, len ) + len );
  } /* nbtcpmem */


/* ============================== nbt_common.h ============================== */
#endif /* NBT_COMMON_H */
