/* ========================================================================== **
 *                                 nbt_nbns.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description:
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_nbns.c; 2020-11-02 19:48:48 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>     /* For debugging; see assert(3).  */

#include "nbt_names.h"  /* NBT Name Manipulations.        */
#include "nbt_nbns.h"   /* Module Header.                 */


/* -------------------------------------------------------------------------- **
 * Static Functions:
 */

static int PackNsHdr( const nbt_nsMsg *const nsMsg,
                      uint8_t         *const bufr,
                      const size_t           bSize )
  /* ------------------------------------------------------------------------ **
   * Build an NBT Name Service message header from parts.
   *
   *  Input:  nsMsg - Pointer to an <nbt_nsMsg> structure which contains
   *                  the <tid>, <flags>, and <rmap> values that will be
   *                  used in composing the message header.
   *          bufr  - Destination buffer; an array of octets into which the
   *                  NBT Name Service message header will be written.
   *          bSize - Size of destination buffer (in octets).  The <bufr>
   *                  must be at least <nbt_nsHEADER_LEN> octets long.
   *
   *  Output: On success, <nbt_nsHEADER_LEN> is returned.  On error, an
   *          error code value is returned.
   *
   *  Errors: nbt_errBufrTooSmall - Indicates that <bufr> is not large
   *                                enough to fit an NBT name service header
   *                                (<bSize> is less than
   *                                <nbt_nsHEADER_LEN>).
   *
   *  Notes:  The following <nsMsg> fields are used to compose the header:
   *          tid   - A 16-bit Transaction Id (TID) which will be written to
   *                  the zero offset of <bufr> in network byte order.
   *          flags - NBT Header flags.  This value can be constructed by
   *                  'OR'ing flag field constants including the Response
   *                  Flag, OpCodes, NM_Flags, and RCode values.
   *          rmap  - Indicates which of the Query/Resource records will
   *                  follow the header.  Construct this parameter by
   *                  'OR'ing a subset of:
   *                  + nbt_nsQUERYREC = Query Record.
   *                  + nbt_nsANSREC   = Answer Record.
   *                  + nbt_nsNSREC    = Name Server Record (never used).
   *                  + nbt_nsADDREC   = Additional Record.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int      i;
  uint8_t *p;
  static const uint8_t rmap_array[4] =
    {
    nbt_nsQUERYREC,
    nbt_nsANSREC,
    nbt_nsNSREC,
    nbt_nsADDREC
    };

  /* Check our personal boundaries. */
  if( bSize < nbt_nsHEADER_LEN )
    return( nbt_errBufrTooSmall );

  /* 1. Set the TID.
   * 2. Cauterize the flags value and write it to the flags field.
   * 3. Set each count field to 0 or 1, depending on the rmap value.
   * z. Done.
   */
  p = nbtset16( bufr, nsMsg->tid );
  p = nbtset16( &bufr[2], (nsMsg->flags & nbt_nsHEADER_FLAGMASK) );
  for( i = 0; i < 4; i++ )
    p = nbtset16( p, ((nsMsg->rmap & rmap_array[i]) ? 1 : 0) );
  return( nbt_nsHEADER_LEN );
  } /* PackNsHdr */

static int ParseQR( uint8_t   *const msg,
                    size_t     const msgLen,
                    int              offset,
                    nbt_nsMsg *const nsMsg )
  /* ------------------------------------------------------------------------ **
   * Rip apart a Question Record.
   *
   *  Input:  msg     - Pointer to a buffer containing a wire-format
   *                    Name Service message.
   *          msgLen  - The number of octets in the buffer indicated by
   *                    <msg>.
   *          offset  - The offset, relative to the start of <msg>, at which
   *                    the Question Record begins.
   *          nsMsg   - A pointer to an <nbt_nsMsg> structure, into which
   *                    the Question Record fields will be unpacked.
   *
   *  Output: The offset at which the next data may be read.  This is the
   *          given <offset> plus number of octets consumed by unpacking the
   *          Question Record.
   *
   *  Errors: nbt_errInvalidLblLen  - The starting label was invalid.  The
   *                                  first label should always be 32 octets
   *                                  in length.
   *          nbt_errBadLblFlag     - A non-zero label flag was found.
   *                                  (Could be a label string pointer.)
   *          nbt_errOutOfBounds    - The given <offset> indicates a position
   *                                  at or beyond the end of the buffer.
   *          nbt_errTruncatedBufr  - Ran out of buffer before we found all
   *                                  of the data.
   *          nbt_errNameTooLong    - NBT name is greater than 255 octets.
   *
   *  Notes:  This function will return the <nbt_errBadLblFlag> error even
   *          if there is a label string pointer in the name field.  STD19
   *          does not allow a label string pointer in the Question Name
   *          field.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int rslt;

  /* Find the name and its length, and do a lot of error detection. */
  if( (rslt = nbt_CheckL2Name( msg, offset, (int)msgLen )) < 0 )
    return( rslt );
  /* We survived, so we have a valid name with a valid length (<=255).  */
  nsMsg->Q_name     = msg;
  nsMsg->Q_name_len = (uint8_t)(0xFF & rslt);

  /* Make sure that the buffer has enough room to hold the Type and Class. */
  offset += rslt;
  if( msgLen < (offset + 4) )
    return( nbt_errTruncatedBufr );

  /* Read the QUESTION_TYPE.
   *  We don't bother reading the Class since it must always be 0x0001 (IN).
   */
  nsMsg->Q_type = nbtget16( &msg[offset] );

  /* Add four octets; two for QUESTION_TYPE and two for QUESTION_CLASS.
   */
  return( offset + 4 );
  } /* ParseQR */

static int PackQR( nbt_nsMsg *const nsMsg,
                   uint8_t   *const bufr,
                   size_t     const bSize )
  /* ------------------------------------------------------------------------ **
   * Pack the Question Record (QR) portion of an NBT Name Service Message.
   *
   *  Input:  nsMsg - An <nbt_nsMsg> structure containing the Question Name
   *                  and Question Type.
   *          bufr  - A pointer to a memory buffer into which the QR will be
   *                  written.  This parameter must point to the location of
   *                  the Question Record.
   *          bSize - Number of bytes available in <bufr>.
   *
   *  Output: On error, an error code is returned (error codes are always
   *          negative values).  On success, the number of bytes written
   *          to <bufr> are returned.  This will be 4 plus the length of
   *          the QUESTION_NAME.
   *
   *  Errors: nbt_errBufrTooSmall - Indicates that <bufr> is not large
   *                                enough to to hold the Question Record.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint8_t *p;
  int      qr_len = 4 + nsMsg->Q_name_len;

  assert( NULL != bufr );
  assert( NULL != nsMsg );

  if( qr_len > bSize )
    return( nbt_errBufrTooSmall );

  p = nbtcpmem( bufr, nsMsg->Q_name, nsMsg->Q_name_len );
  p = nbtset16( p, nsMsg->Q_type );
  p = nbtset16( p, nbt_nsQCLASS_IN );
  return( qr_len );
  } /* PackQR */

static int ParseRR( uint8_t   *const msg,
                    size_t     const msgLen,
                    int              offset,
                    nbt_nsMsg *const nsMsg )
  /* ------------------------------------------------------------------------ **
   * Rip apart a Resource Record.
   *
   *  Input:  msg     - Pointer to a buffer containing a wire-format
   *                    Name Service message.
   *          msgLen  - The number of octets in the buffer indicated by
   *                    <msg>.
   *          offset  - The offset, relative to the start of <msg>, at which
   *                    the Resource Record begins.
   *          nsMsg   - A pointer to an <nbt_nsMsg> structure, into which
   *                    the Resource Record fields will be unpacked.
   *
   *  Output: On error, a negative value is returned as an error code.
   *          On success, the return value is the offset indicating the next
   *          octet beyond the end of the Resource Record.
   *
   *  Errors: nbt_errInvalidLblLen  - The starting label was invalid.  The
   *                                  first label should always be 32 octets
   *                                  in length.
   *          nbt_errBadLblFlag     - A non-zero label flag was found
   *                                  indicating a Label String Pointer (LSP).
   *                                  Either the LSP is invalid, or
   *                                  <nsMsg->Q_name> is NULL.
   *          nbt_errOutOfBounds    - The given <offset> indicates a position
   *                                  at or beyond the end of the buffer.
   *          nbt_errTruncatedBufr  - Ran out of buffer before we found all
   *                                  of the data.
   *          nbt_errNameTooLong    - NBT name is greater than 255 octets.
   *
   *  Notes:
   *    - If <nsMsg->rdata_len> is zero, then the message does not contain
   *      RDATA, and <nsMsg->rdata> will be set to NULL.  Otherwise,
   *      <nsMsg->rdata> will point to the start of the RDATA blob within
   *      <msg>
   *    - The returned value should be the size of the complete message,
   *      relative to <msg>.  It is the sum of the original <offset> and the
   *      size of the Resource Record (including the RDATA blob).
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int rslt;

  /* Find the name and its length.
   */
  rslt = nbt_CheckL2Name( msg, offset, (int)msgLen );
  if( nbt_errBadLblFlag == rslt )
    {
    /* Special case: We have a Label String Pointer (LSP).
     *  LSPs are okay in RR_names *if* they're correct LSPs and *if* they
     *  point to the right place.  We will return an error if the <Q_name>
     *  in <msg> is NULL or if the LSP is incorrect.
     */
    if( (NULL == nsMsg->Q_name) || (nbt_nsLSP != nbtget16( &msg[offset] )) )
      return( nbt_errBadLblFlag );
    nsMsg->lsp         = true;
    nsMsg->RR_name     = nsMsg->Q_name;
    nsMsg->RR_name_len = nsMsg->Q_name_len;
    offset += 2;
    }
  else
    {
    /* We're not dealing with an LSP (correct or otherwise).
     * If we got an error in the name check, return that result.
     * Otherwise, we've got a valid L2 name.
     */
    if( rslt < 0 )
      return( rslt );
    nsMsg->lsp         = false;
    nsMsg->RR_name     = &msg[offset];
    nsMsg->RR_name_len = (uint8_t)(0xFF & rslt);
    offset += rslt;
    }

  /* Make sure that we have room for the Type[2], Class[2], TTL[4],
   * and RDLENGTH[2] fields before we try to read them.  (2+2+4+2==10)
   */
  if( msgLen < (offset + 10) )
    return( nbt_errTruncatedBufr );

  /* Parse the remaining fields. */
  nsMsg->RR_type   = nbtget16( &msg[offset] );
  offset += 4;     /* Type (and Class) */
  nsMsg->ttl       = nbtget32( &msg[offset] );
  offset += 4;     /* TTL */
  nsMsg->rdata_len = nbtget16( &msg[offset] );
  offset += 2;     /* RDLENGTH */

  /* There must be RDLENGTH octets left in the buffer.
   *  If so, set a pointer to the RDATA block, and return the offset.
   */
  rslt = offset + nsMsg->rdata_len;
  if( msgLen < rslt )
    return( nbt_errTruncatedBufr );
  nsMsg->rdata = (nsMsg->rdata_len > 0) ? &msg[offset] : NULL;
  return( offset );
  } /* ParseRR */

static int PackRR( nbt_nsMsg *const nsMsg,
                   uint8_t   *const bufr,
                   size_t     const bSize )
  /* ------------------------------------------------------------------------ **
   * Pack the Resource Record (RR) portion of an NBT Name Service Message.
   *
   *  Input:  nsMsg - An <nbt_nsMsg> structure containing or indicating the
   *                  Resource Record data.
   *          bufr  - A pointer to a memory buffer into which the RR will be
   *                  written.  This parameter must point to the location of
   *                  the Resource Record.
   *          bSize - Number of bytes available in <bufr>.
   *
   *  Output: On error, an error code is returned (error codes are always
   *          negative values).  On success, the number of bytes written
   *          to <bufr> are returned.
   *
   *  Errors: nbt_errBufrTooSmall - Indicates that <bufr> is not large
   *                                enough to fit the Resource Record.
   *
   *  Notes:  The RDLENGTH is copied into the buffer, but the RDATA blob
   *          is not.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint8_t *p;
  int      nameLen;

  assert( NULL != bufr );
  assert( NULL != nsMsg );

  if( nsMsg->lsp )
    {
    nameLen = 2;
    if( bSize < 12 )
      return( nbt_errBufrTooSmall );
    p = nbtset16( bufr, nbt_nsLSP );
    }
  else
    {
    nameLen = nsMsg->RR_name_len;
    if( bSize < (nameLen + 10) )
      return( nbt_errBufrTooSmall );
    p = nbtcpmem( bufr, nsMsg->RR_name, (size_t)nameLen );
    }
  p = nbtset16( p, nsMsg->RR_type );
  p = nbtset16( p, nbt_nsRRCLASS_IN );
  p = nbtset32( p, nsMsg->ttl );
  p = nbtset16( p, nsMsg->rdata_len );
  return( nameLen + 10 );
  } /* PackRR */


/* -------------------------------------------------------------------------- **
 * Functions
 */

int nbt_nsParseMsg( uint8_t   *const msg,
                    size_t     const msgLen,
                    nbt_nsMsg *const nsMsg )
  /** Carve up the contents of a received NBT message.
   *
   * @param[in]   msg     A pointer to a buffer containing the NBT Name
   *                      Service message to be parsed.
   * @param[in]   msgLen  The length, in octets, of the \p msg buffer.
   * @param[out]  nsMsg   A pointer to an \c #nbt_nsMsg into which \p msg
   *                      will be parsed.
   *
   * @returns On error, a negative error code is returned.  On success, the
   *          number of octets of \p msg that were parsed.
   *
   * \b Errors
   *  - \c #nbt_errInvalidLblLen  - While checking an NBT Name, the starting
   *                                label was found to be invalid.  The
   *                                first label should always be 32 octets
   *                                in length.
   *  - \c #nbt_errBadLblFlag     - A non-zero label flag was found
   *  - \c #nbt_errTruncatedBufr  - Ran out of buffer before all of the
   *                                required data was found.
   *  - \c #nbt_errNameTooLong    - The NBT Name being parsed is greater than
   *                                the maximum 255 octets in length.
   *  - \c #nbt_errInvalidPacket  - Conflicting data discovered within the
   *                                packet during parsing (invalid packet).
   *  - \c #nbt_errUnknownCommand - Unrecognized NBT Name Service `OpCode`.
   *
   * \b Notes
   *  - The RDATA blob, if there is one, is \e not parsed by this function.
   *    A pointer to the RDATA blob, and its length, are returned via the
   *    \c #nbt_nsMsg structure indicated by \p nsMsg.
   *  - Several checks are done to ensure that the message is properly
   *    parsed.  Few are done to ensure that the parsed data is correct.
   *    This is left as an exercise for the caller.
   *  - There is no good reason to support the Redirect Name Query Response
   *    (\c #nbt_nsNAME_QUERY_REPLY_REDIR).  However, this function will
   *    detect this (unused) message type and handle it correctly.
   *  - A Name Registration Request always has the `RD` (Recursion Desired)
   *    bit set.  The same message with `RD` clear is either a Name
   *    Overwrite Demand or a Name Update Request depending upon on the
   *    value of the Broadcast (`B`) bit.  See:
   *    + <a href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.1.1">[IMPCIFS;
   *      1.4.3.1.1]: Broadcast Name Registration</a>
   *    + <a href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.1.2">[IMPCIFS;
   *      1.4.3.1.2]: Unicast (NBNS) Name Registration</a>
   */
  {
  uint16_t opcode;
  int      rslt;

  /* This function should not be called if either pointer is NULL. */
  assert( NULL != msg );
  assert( NULL != nsMsg );
  /* We must, at least, have a message header. */
  if( msgLen <= nbt_nsHEADER_LEN )
    return( nbt_errTruncatedBufr );

  /* Initial parse. */
  nsMsg->tid   = nbtget16( msg );
  nsMsg->flags = nbtget16( &msg[2] );
  nsMsg->rmap  = nbt_nsGetRmap( msg );
  /* ...and subparse. */
  opcode = (nbt_nsOPCODE_MASK & nsMsg->flags);

  /* Split Requests from Responses. */
  if( 0 == (nbt_nsR_BIT & nsMsg->flags) )
    {
    /* Requests... */
    switch( opcode )
      {
      case nbt_nsOPCODE_WACK:
        /* WACKs are only supposed to be sent as replies. */
        return( nbt_errInvalidPacket );
      case nbt_nsOPCODE_QUERY:
        {
        /* Name and Status query Requests.
         * These have only a Query Record, no Resource Record.
         */
        if( (rslt = ParseQR( msg, msgLen, nbt_nsHEADER_LEN, nsMsg )) < 0 )
          return( rslt );
        /* Ensure that the RR portion is clean. */
        nsMsg->RR_name     = NULL;
        nsMsg->RR_name_len = 0;
        nsMsg->rdata_len   = 0;
        /* Determine the query type. */
        nsMsg->type = (nbt_nsQTYPE_NBSTAT == nsMsg->Q_type)
                    ? nbt_nsNODE_STATUS_REQST : nbt_nsNAME_QUERY_REQST;
        return( rslt );
        }
      /* The remainder all have both a query and a resource record. */
      case nbt_nsOPCODE_REGISTER:
        nsMsg->type = (nbt_nsRD_BIT & nsMsg->flags)
                    ? nbt_nsNAME_REG_REQST
                    : (nbt_nsB_BIT & nsMsg->flags)
                    ? nbt_nsNAME_OVERWRITE_DEMAND : nbt_nsNAME_UPDATE_REQST;
        break;
      case nbt_nsOPCODE_REFRESH:
      case nbt_nsOPCODE_ALTREFRESH:
        nsMsg->type = nbt_nsNAME_REFRESH_REQST;
        break;
      case nbt_nsOPCODE_MULTIHOMED:
        nsMsg->type = nbt_nsMULTI_REG_REQST;
        break;
      case nbt_nsOPCODE_RELEASE:
        nsMsg->type = nbt_nsNAME_RELEASE_REQST;
        break;
      default:
        return( nbt_errUnknownCommand );
      }
    /* Reach this point if we have a message with both Q and RR sections. */
    if( (rslt = ParseQR( msg, msgLen, nbt_nsHEADER_LEN, nsMsg )) < 0 )
      return( rslt );
    return( ParseRR( msg, msgLen, rslt, nsMsg ) );
    }

  /* Responses...
   *  ...do not contain query records, just resource records.
   *  We clear out the <Q_name> and <Q_name_len> fields to indicate that
   *  these are (intentionally left) blank.
   */
  nsMsg->Q_name     = NULL;
  nsMsg->Q_name_len = 0;

  switch( opcode )
    {
    case nbt_nsOPCODE_QUERY:
      {
      /* Query Response: Node Status, Negative/Positive Name Query. */
      if( nbt_nsQTYPE_NBSTAT == nsMsg->RR_type )
        nsMsg->type = nbt_nsNODE_STATUS_REPLY;
      else
        if( nbt_nsRCODE_MASK & nsMsg->flags )
          nsMsg->type = nbt_nsNAME_QUERY_REPLY_NEG;
        else
          {
          nsMsg->type = nbt_nsNAME_QUERY_REPLY_POS;
          /* We should never get a Redirect Name Query Response. */
          if( 0 == (nsMsg->flags & (nbt_nsAA_BIT | nbt_nsRA_BIT)) )
            nsMsg->type = nbt_nsNAME_QUERY_REPLY_REDIR;
          }
      break;
      }
    case nbt_nsOPCODE_REGISTER:
      {
      /* Registraton Response:
       *  Name Conflict Demand, Neg/Pos Registration Response,
       *  Registration Challenge.
       */
      switch( nsMsg->flags & nbt_nsRCODE_MASK )
        {
        case nbt_nsRCODE_POS_RSP:
          nsMsg->type = (nbt_nsRA_BIT & nsMsg->flags)
                      ? nbt_nsNAME_REG_REPLY_POS : nbt_nsNAME_REG_CHALLENGE;
          break;
        case nbt_nsRCODE_CFT_ERR:
          nsMsg->type = nbt_nsNAME_CONFLICT_DEMAND; break;
        default:
          nsMsg->type = nbt_nsNAME_REG_REPLY_NEG;   break;
        }
      break;
      }
    case nbt_nsOPCODE_RELEASE:
      {
      /* Release Response: Positive/Negative Name Release. */
      nsMsg->type = (nbt_nsRCODE_MASK & nsMsg->flags)
                  ? nbt_nsNAME_RELEASE_REPLY_NEG
                  : nbt_nsNAME_RELEASE_REPLY_POS;
      break;
      }
    case nbt_nsOPCODE_WACK:
      {
      nsMsg->type = nbt_nsWACK_REPLY;
      /* Special case: [RFC1002; 4.2.16] says a WACK may have a NULL name.
       *  Not sure if this is true in practice, but we'll go with it.
       */
      if( 0 == msg[nbt_nsHEADER_LEN] )
        {
        if( msgLen < 25 )
          return( nbt_errTruncatedBufr );
        /* Unpack the remaining 12 octets. */
        nsMsg->RR_name     = NULL;
        nsMsg->RR_name_len = 0;
        nsMsg->RR_type     = nbtget16( &msg[13] );  /* Skip Class; 2 octets.  */
        nsMsg->ttl         = nbtget32( &msg[17] );
        nsMsg->rdata_len   = nbtget16( &msg[21] );
        nsMsg->rdata       = &msg[23];
        return( 25 );
        }
      break;
      }

    /* Should not see these in a Response.  */
    case nbt_nsOPCODE_REFRESH:
    case nbt_nsOPCODE_ALTREFRESH:
    case nbt_nsOPCODE_MULTIHOMED:
      return( nbt_errInvalidPacket );
    default:
      return( nbt_errUnknownCommand );
    }

  /* We fall through to this point if we know the message type,
   *  and know that it has a standard Resource Record.
   */
  return( ParseRR( msg, msgLen, nbt_nsHEADER_LEN, nsMsg ) );
  } /* nbt_nsParseMsg */

int nbt_nsParseAddrEntry( uint8_t         *const rdata,
                          const uint16_t         rdata_len,
                          nbt_nsAddrEntry *const nsAddrEntry )
  /** Read six \c RDATA octets into NB_FLAGS and NB_ADDRESS values.
   *
   * @param[in]   rdata       Pointer to a buffer containing the Address
   *                          Entry.
   * @param[in]   rdata_len   The size, in octets, of \p rdata.
   * @param[out]  nsAddrEntry Pointer to an \c #nbt_nsAddrEntry structure
   *                          to receive the unpacked data.
   *
   * @returns
   *  On error, \c #nbt_errTruncatedBufr to indicate that \p rdata is not
   *  large enough for a full Address Entry.  Six (6) octets are required.
   *  On success, the return value represents the number of octets that
   *  were read...which will always be six (6).
   *
   * \b Notes
   *  - The following message types include a single Address Entry as the
   *    content of the RDATA subfield:
   *    + `NAME REGISTRATION REQUEST`
   *    + `MULTI-HOMED REGISTRATION REQUEST`
   *    + `NAME OVERWRITE DEMAND`
   *    + `NAME UPDATE REQUEST`
   *    + `NAME REFRESH REQUEST`
   *    + `POSITIVE NAME REGISTRATION RESPONSE`
   *    + `NEGATIVE NAME REGISTRATION RESPONSE`
   *    + `END-NODE CHALLENGE REGISTRATION RESPONSE`
   *    + `NAME CONFLICT DEMAND` (see notes below)
   *    + `NAME RELEASE REQUEST and DEMAND`
   *    + `POSITIVE NAME RELEASE RESPONSE`
   *    + `NEGATIVE NAME RELEASE RESPONSE`
   *  - The \c RDATA field of the `POSITIVE NAME QUERY RESPONSE` will
   *    contain an array of one or more Address Entries.
   *  - In the `NAME CONFLICT DEMAND message`, the \c TTL and \p NB_ADDRESS
   *    fields must be zero.  In \c NB_FLAGS, the \c G (Group) bit will be
   *    zero (clear) because only unique names can be in conflict.  That
   *    leaves only the \c ONT subfield to be filled in.
   *  - The `NAME OVERWRITE DEMAND` and `NAME UPDATE REQUEST` are almost the
   *    same message.  Both terms are used in [STD19].  The difference is in
   *    the way in which they are used.  The `NAME OVERWRITE DEMAND` is
   *    broadcast (and has the `B` bit set), while the `NAME UPDATE REQUEST`
   *    is unicast (with `B` clear) to an unsecured NBNS.  See: <a
   *    href="http://ubiqx.org/cifs/NetBIOS.html#NBT.4.3.1.2">[IMPCIFS;
   *    1.4.3.1.2]: Unicast (NBNS) Name Registration</a>.
   *
   * @see \c #nbt_nsRDataFlags
   */
  {
  assert( NULL != rdata );
  assert( NULL != nsAddrEntry );

  if( rdata_len < 6 )
    return( nbt_errTruncatedBufr );
  nsAddrEntry->NB_flags = nbtget16( rdata );
  nsAddrEntry->NB_addr  = nbtget32( &rdata[2] );
  return( 6 );
  } /* nbt_nsParseAddrEntry */

int nbt_nsParseWACK( uint8_t  *const rdata,
                     const uint16_t  rdata_len,
                     uint16_t *const rdFlags )
  /** Read the two \c RDATA octets into a 16-bit flags value.
   *
   * @param[in]   rdata     Pointer to a buffer containing the additional
   *                        data.
   * @param[in]   rdata_len The size, in octets, of \p rdata, which should
   *                        always be 2 in this case.
   * @param[out]  rdFlags   Pointer to an unsigned 16-bit integer.  The
   *                        RDATA returned in a WACK Response should be an
   *                        echo of the header FLAGS field in the request.
   *
   * @returns   On success, the number of bytes read, which must be 2.
   *            On error, a negative number is returned.  The only error
   *            code returned is \c #nbt_errTruncatedBufr, to indicate that
   *            \p rdata_len was less than 2.
   *
   * \b Notes
   *  - The Resource Record portion of the WACK message is parsed by
   *    \c #nbt_nsParseMsg().  This is consistent with other message types.
   *    This function focuses on the \c RDATA blob, which is unique per
   *    message type.
   */
  {
  assert( NULL != rdata );
  assert( NULL != rdFlags );

  if( rdata_len < 2 )
    return( nbt_errTruncatedBufr );
  *rdFlags = nbtget16( rdata );
  return( 2 );
  } /* nbt_nsParseWACK */

int nbt_nsParseStatus( uint8_t         *const rdata,
                       const uint16_t         rdata_len,
                       nbt_nsNodeStats *const nodeStat )
  /** Quick parse of the Node Status Response \c RDATA block.
   *
   * @param[in]   rdata     Pointer to a buffer containing the Node Status
   *                        data.
   * @param[in]   rdata_len The size, in octets, of \p rdata.  This must be
   *                        the Resource Record \c RDLENGTH from the message.
   * @param[out]  nodeStat  Pointer to a \c #nbt_nsNodeStats structure.
   *                        In this first pass, the \c NODE_NAME Array
   *                        and \c STATISTICS portions of the \c RDATA
   *                        blob are broken out so that they can be
   *                        further processed.
   *
   * @returns On success, the value of \p rdata_len is returned.  On error,
   *          \c #nbt_errTruncatedBufr is returned.  If \p rdata_len is
   *          zero (0), then \p nodeStat is cleared and zero is returned.
   *
   * \b Notes
   *  - The number of names and/or the size of the statistics subfield may
   *    be zero.  If so, then the respective pointer in \p nodeStat will be
   *    NULL.
   */
  {
  uint16_t tmpLen;

  /* Paranoia. */
  assert( NULL != nodeStat );
  /* Check for empty RDATA. */
  if( (0 == rdata_len) || (NULL == rdata) )
    return( 0 );

  /* Clear everything at the outset. */
  nodeStat->num_names  = 0;
  nodeStat->name_array = NULL;
  nodeStat->stat_size  = 0;
  nodeStat->statistics = NULL;

  /* Get the number of names; give up if we can't possibly have that many.  */
  nodeStat->num_names = *rdata;
  tmpLen = (uint16_t)((18 * nodeStat->num_names) + 1);  /* +1 for NUM_NAMES */
  if( rdata_len < tmpLen )
    return( nbt_errTruncatedBufr );

  /* Parse the rest. */
  nodeStat->name_array = &rdata[1];
  if( rdata_len > tmpLen )
    {
    nodeStat->stat_size  = (uint16_t)(rdata_len - tmpLen);
    nodeStat->statistics = &rdata[tmpLen];
    }
  return( rdata_len );
  } /* nbt_nsParseStatus */

int nbt_nsParseNameEntry( uint8_t             *const entry,
                          nbt_nsNodeNameEntry *const nodeName )
  /** Break out the subfields of a \c NODE_NAME Entry block.
   *
   * @param[in]   entry     Pointer to the start of a \c NODE_NAME entry
   *                        within the \c NODE_NAME array.
   * @param[out]  nodeName  Pointer to a \c #nbt_nsNodeNameEntry structure
   *                        into which the \c NODE_NAME entry will be
   *                        disemboweled.
   *
   * @returns   This function always returns 18, which is the fixed
   *            wire-format size of a \c NODE_NAME structure.
   *
   * \b Notes
   *  - It is \b assumed that a test has already been done to confirm that
   *    there are at least 18 octets in the buffer indicated by \p entry.
   *    Such a test is implictly performed in \c #nbt_nsParseStatus().
   *  - The trailing suffix byte is copied into the \c type field of
   *    \p nodeName, which means that it has been preserved.  That means
   *    that \c nodeName->name[15] could be overwritten with a NUL octet,
   *    making it easier to validate and otherwise manipulate.
   *
   * @see \c #nbt_CheckNbName()
   */
  {
  assert( NULL != entry );
  assert( NULL != nodeName );

  nodeName->name  = entry;
  nodeName->type  = entry[15];
  nodeName->flags = nbtget16( &entry[16] );
  return( 18 );
  } /* nbt_nsParseNameEntry */

int nbt_nsPackMsg( nbt_nsMsg *const nsMsg,
                   uint8_t   *const bufr,
                   size_t     const bSize )
  /** Compose a Name Service message.
   *
   * @param[in]   nsMsg A pointer to an \c #nbt_nsMsg structure from which
   *                    the message will be built.
   * @param[out]  bufr  A pointer to a buffer within which the message will
   *                    be constructed.
   * @param[in]   bSize The number of bytes available in \p bufr.
   *
   * @returns   On success, the number of octets of \p bufr that were filled
   *            with message data.
   *
   * \b Errors
   *  - nbt_errNullInput    - The \c nsMsg->rmap field indicates that a name
   *                          is required in either a Question Record or a
   *                          Resource record (or both), but the name was not
   *                          provided via \p nsMsg.
   *  - nbt_errBufrTooSmall - Based on \p bSize, \p bufr is not large enough
   *                          for the composed message.
   *
   * \b Notes
   *  - This function goes through the fields provided via \p nsMsg and
   *    selectively copies them into \p bufr.
   *    + The \c #nbt_nsMsg.type field is not consulted.  This function
   *      ignores what the message \e should be and just does what it's
   *      told to do.
   *    + The Question Record and Resource Record are written depending
   *      upon the flags in the \c #nbt_nsMsg.rmap field.
   *  - If there is a Resource Record present, the \c RDLENGTH
   *    (\c #nbt_nsMsg.rdata_len) is copied into \p bufr, but the
   *    \c RDATA (\c #nbt_nsMsg.rdata) is \b not.  The caller may copy
   *    the \c RDATA blob into the \p bufr, but may be better served by
   *    using an I/O vector to transmit the message instead.  See
   *    <a href="@posix/functions/sendmsg.html">sendmsg(3)</a>
   */
  {
  int rslt;   /* Return value of a call.         */
  int offset; /* Writing position within <bufr>. */

  assert( NULL != bufr );
  assert( NULL != nsMsg );

  /* Write the header. */
  if( (rslt = PackNsHdr( nsMsg, bufr, bSize )) < 0 )
    return( rslt );
  offset = rslt;

  /* Is there a Question Record?  If so, write it. */
  if( nbt_nsQUERYREC & nsMsg->rmap )
    {
    /* There *should be* a Query Name in the <nsMsg> structure. */
    if( (NULL == nsMsg->Q_name) || (0 == nsMsg->Q_name_len) )
      return( nbt_errNullInput );
    if( (rslt = (int)bSize - offset) < 0 )
      return( nbt_errBufrTooSmall );  /* Should be impossible at this stage.  */
    if( (rslt = PackQR( nsMsg, &bufr[offset], (size_t)rslt )) < 0 )
      return( rslt );
    offset += rslt;
    }

  /* Is there a Resource Record (of any type)?  If so, append it. */
  if( 0 != (nsMsg->rmap & (nbt_nsANSREC | nbt_nsNSREC | nbt_nsADDREC)) )
    {
    if( !(nsMsg->lsp)
     && ((NULL == nsMsg->RR_name) || (0 == nsMsg->RR_name_len)) )
      return( nbt_errNullInput );
    if( (rslt = (int)bSize - offset) < 0 )
      return( nbt_errBufrTooSmall );  /* Should be impossible at this stage.  */
    if( (rslt = PackRR( nsMsg, &bufr[offset], (size_t)rslt )) < 0 )
      return( rslt );
    offset += rslt;
    }

  return( offset );
  } /* nbt_nsPackMsg */

int nbt_nsPackAddrEntry( nbt_nsAddrEntry *const nsAddrEntry,
                         uint8_t         *const bufr,
                         size_t           const bSize )
  /** Pack a set of NBT flags and an IPv4 address into an \c RDATA blob.
   *
   * @param[in]   nsAddrEntry   Pointer to a structure that holds the NBT
   *                            flags and IPv4 address.
   * @param[out]  bufr          Pointer to the buffer into which the \c RDATA
   *                            will be written.
   * @param[in]   bSize         Size, in octets, of \p bufr.
   *
   * @returns   On error, a negative value is returned.  Otherwise, the
   *            return value indicates the number of octets written to
   *            \p bufr (always 6).
   *
   * \b Errors
   *  - \c #nbt_errBufrTooSmall - Returned if \p bufr is less than 6 octets
   *                              in size, as indicated by \p bSize.
   */
  {
  assert( NULL != bufr );
  assert( NULL != nsAddrEntry );

  if( bSize < 6 )
    return( nbt_errBufrTooSmall );
  (void)nbtset16(  bufr,    nsAddrEntry->NB_flags );
  (void)nbtset32( &bufr[2], nsAddrEntry->NB_addr );
  return( 6 );
  } /* nbt_nsPackAddrEntry */

int nbt_nsPackWACK( uint16_t const rdFlags,
                    uint8_t *const bufr,
                    size_t   const bSize )
  /** Write a 16-bit flags value into the output buffer.
   *
   * @param[in]   rdFlags   A 16-bit flags value that contains the \c OPCODE
   *                        and \c NM_FLAGS from the request.
   * @param[out]  bufr      Pointer to the buffer into which the \c RDATA
   *                        will be written.
   * @param[in]   bSize     Size, in octets, of \p bufr.
   *
   * @returns   On error, a negative value is returned.  Otherwise, the
   *            return value indicates the number of octets written to
   *            \p bufr (always 2).
   *
   * \b Errors
   *  - \c #nbt_errBufrTooSmall - Returned if \p bufr is less than 2 octets
   *                              in size, as indicated by \p bSize.
   */
  {
  assert( NULL != bufr );

  if( bSize < 2 )
    return( nbt_errBufrTooSmall );
  (void)nbtset16( bufr, rdFlags );
  return( 2 );
  } /* nbt_nsPackWACK */

int nbt_nsPackNameEntry( nbt_nsNodeNameEntry *const nodeName,
                         uint8_t             *const bufr,
                         size_t               const bSize )
  /** Pack a Node Name Entry into its 18-octet wire format.
   *
   * @param[in]   nodeName  Pointer to a single Node Name Entry structure.
   * @param[out]  bufr      Pointer to the buffer into which the Name Entry
   *                        will be written.
   * @param[in]   bSize     Size, in octets, of \p bufr.
   *
   * @returns   18 (success) or a negative value to indicate an error.
   *
   * \b Errors
   *  - \c #nbt_errBufrTooSmall - Returned if \p bufr is less than 18 octets
   *                              in size, as indicated by \p bSize.
   */
  {
  assert( NULL != bufr );
  assert( NULL != nodeName );

  if( bSize < 18 )
    return( nbt_errBufrTooSmall );
  (void)nbtcpmem( bufr, nodeName->name, 15 );
  bufr[15] = nodeName->type;
  (void)nbtset16( &bufr[16], nodeName->flags );
  return( 18 );
  } /* nbt_nsPackNameEntry */


/* ============================== Like, Totally ============================= */
