/* ========================================================================== **
 *                                 nbt_nbss.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description:
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_nbss.c; 2020-11-04 09:25:21 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>     /* For debugging.             */
#include <string.h>     /* For memcmp(3) & memcpy(3). */

#include "nbt_nbss.h"   /* Module Header.             */


/* -------------------------------------------------------------------------- **
 * Macros
 *
 *  L2Okay( CN )  - Quick check to partially validate a Level-2 Encoded NBT
 *                  name.  This macro is only to be used for debugging.  For
 *                  a more thorough validation test, see nbt_CheckL2Name()
 *                  in the nbt_names module.
 */

#define L2Okay( CN ) ((32 == (CN)[0]) && (0 == (CN)[33])) ? true : false


/* -------------------------------------------------------------------------- **
 * Functions
 */

int nbt_ssParseMsg( const uint8_t *const msg, nbt_ssMsg *const ssMsg )
  /** Parse the 4-byte Session Service message prefix.
   *
   * @param[in]   msg     Pointer to the input buffer, which \b must be at
   *                      least 4 bytes in length.
   * @param[out]  ssMsg   Pointer to a \c #nbt_ssMsg structure into which
   *                      the message prefix will be parsed.  Only the
   *                      \c Type and \c Length fields are assigned.
   *
   * @returns   On success, 4 is returned to indicate the number of bytes
   *            that have been processed.  On error, a negative value is
   *            returned.
   *
   * \b Errors
   *  - \c #nbt_errInvalidPacket  - The \c Flags subfield was not zero,
   *                                indicating a malformed packet.
   *
   * \b Notes
   *  - The \c Type and \c Length subfields are not validated by this
   *    function.  \b See: \c #nbt_ssParseTrailer().
   */
  {
  assert( NULL != msg );
  assert( NULL != ssMsg );

  /* The first 4 bytes are a 32-bit field in network byte order.
   *  We parse out the subfields, and verify that the undefined
   *  bits are all zeros (as they should be).
   */
  ssMsg->Length  = nbtget32( msg );
  ssMsg->Type    = (uint8_t)(0xFF & (ssMsg->Length >> 24));
  ssMsg->Length &= 0x00FFFFFF;
  if( ssMsg->Length & 0x00FE0000 )
    return( nbt_errInvalidPacket );
  return( 4 );
  } /* nbt_ssParseMsg */

int nbt_ssParseTrailer( uint8_t   *const msg,
                        size_t     const msgLen,
                        nbt_ssMsg *const ssMsg )
  /** Parse additional data in a Session Service message.
   *
   * @param[in]     msg     A pointer to the start of the Trailer.  This may
   *                        be NULL for Session Messages or any message that
   *                        has no Trailer data.
   * @param[in]     msgLen  The number of bytes in \p msg.
   * @param[in,out] ssMsg   A (constant) pointer to an \c #nbt_ssMsg
   *                        structure.  The \c Type and \c Length fields
   *                        must have valid values.  The message Trailer
   *                        fields, if any, will be parsed into the \p ssMsg.
   *
   * @returns On error, a negative value is returned.  Otherwise the return
   *          value is the number of bytes of Trailer data that were parsed.
   *          For Session Messages, the return value is always zero (0).
   *
   * \b Errors
   *  - \c #nbt_errNullInput -
   *        \p msg was NULL, but the message type requires additional data.
   *  - \c #nbt_errBufrTooSmall -
   *        Based on \p msgLen, \p msg is not large enough to contain the
   *        Trailer required by the message type.
   *  - \c #nbt_errInvalidSSLen -
   *        The Trailer length given in \c ssMsg->Length does not match
   *        required value given in RFC1002.
   *  - \c #nbt_errInvalidSSType -
   *        The \c ssMsg->Type value is not a recognized Session Service
   *        message type.
   *
   * \b Notes
   *  - If the message is a Session Message, this function returns zero and
   *    does not inspect \p msg.
   *  - If not NULL, \p msg must point to the Trailer data, not the initial
   *    4 byte prefix.
   *  - \p msg may be NULL for Session Messages, Positive Session Response
   *    messages, and Keepalive messages.  \p msgLen is also ignored for
   *    these message types.
   *  - On input, the values of \c ssMsg->Type and \c ssMsg->Length must be
   *    the results of parsing the 4-byte prefix by calling
   *    \c #nbt_ssParseMsg().  These two values are used to determine if/how
   *    to parse the message Trailer.
   *  - Data in the message Trailer is not validated by this function.
   *    - Use \c #nbt_CheckL2Name() to validate the Called and Calling Names
   *      in a Session Request message.
   *    - Use \c #nbt_ssCheckEcode() to validate the Error Code in a
   *      Negative Session Response.
   *    - Testing the IPv4 address and Port values returned in a Retarget
   *      Response is left as an exercise for the reader.
   */
  {
  /* Quick sanity check. */
  assert( NULL != ssMsg );

  /* Session message payloads are passed through to upper layers. */
  if( NBT_SS_SESS_MSG == ssMsg->Type )
    return( 0 );

  /* Validate <msg> and <msgLen> only for those messages that need them.
   *  For the rest, <ssMsg->Type> must be valid, and <ssMsg->Length> must
   *  be zero.
   */
  switch( ssMsg->Type )
    {
    case NBT_SS_SESS_REQ:
    case NBT_SS_SESS_NEG_RESP:
    case NBT_SS_SESS_RETARGET:
      if( NULL == msg )
        return( nbt_errNullInput );
      if( msgLen < ssMsg->Length )
        return( nbt_errBufrTooSmall );
      break;    /* The message has a trailer. */
    case NBT_SS_SESS_POS_RESP:
    case NBT_SS_SESS_KEEPALIVE:
      if( ssMsg->Length != 0 )
        return( nbt_errInvalidSSLen );
      return( 0 );
    default:
      return( nbt_errInvalidSSType );
    }

  /* There's a Trailer to unload. */
  switch( ssMsg->Type )
    {
    case NBT_SS_SESS_REQ:
      if( ssMsg->Length != (2 * NBT_SS_CNAME_SIZE) )
        return( nbt_errInvalidSSLen );
      ssMsg->Request.CalledName  = msg;
      ssMsg->Request.CallingName = &msg[NBT_SS_CNAME_SIZE];
      break;
    case NBT_SS_SESS_NEG_RESP:
      if( ssMsg->Length != 1 )
        return( nbt_errInvalidSSLen );
      ssMsg->ErrCode = *msg;
      break;
    case NBT_SS_SESS_RETARGET:
      if( ssMsg->Length != 6 )
        return( nbt_errInvalidSSLen );
      ssMsg->Retarget.IPv4 = nbtget32( msg );
      ssMsg->Retarget.Port = nbtget16( &msg[4] );
      break;
    }
  return( (int)ssMsg->Length );
  } /* nbt_ssParseTrailer */

int nbt_ssPackMsg( const nbt_ssMsg *const ssMsg,
                   uint8_t         *const bufr,
                   size_t           const bSize )
  /** Compose an NBT Session Service message.
   *
   * @param[in]   ssMsg   A pointer to an \c #nbt_ssMsg structure that must
   *                      contain the data used to compose the outgoing
   *                      message.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      wire-format message will be written.
   * @param[in]   bSize   The number of available bytes in \p bufr.
   *                      There must be at least 4 bytes available.
   *                      Some messages require more.
   *
   * @returns   The number of bytes of \p bufr that were written (see the
   *            Notes, below), or a negative value to indicate an error.
   *
   * \b Errors
   *  - \c #nbt_errBufrTooSmall - Based on \p bSize, \p bufr is too small to
   *                              compose the wire-format message.
   *  - \c #nbt_errInvalidSSLen - The wire format of the Length field is a
   *                              17-bit unsigned integer, but the value
   *                              provided in \c ssMsg->Length is greater
   *                              than 0x0001FFFF (131071).
   *
   * \b Notes
   *  - The value of \c ssMsg->Length is checked prior to writing the prefix
   *    for a Session Message.  It it is valid, it is copied to \p bufr in
   *    network byte order (as you would expect).  For all other message
   *    types, the value of \c ssMsg->Length is ignored and the value
   *    mandated by RFC1002 is used instead.
   *  - Special handling of Session Request Messages.\n
   *    - The size of a Session Request Message, which includes both a
   *      Called and Calling name, is fixed at 72 bytes.  If the size of
   *      \p bufr is at least 72 (per \p bSize), then the Called and Calling
   *      names will be copied into \p bufr and the return value will be 72.
   *    - If \p bSize is less than 72, then only the 4-byte prefix will be
   *      written to \p bufr, thus avoiding unnecessary copying.  The return
   *      value will be 4 in this case.
   */
  {
  uint32_t tmpPre;

  /* Quickies. */
  assert( NULL != ssMsg );
  assert( NULL != bufr  );
  if( bSize < 4 )
    return( nbt_errBufrTooSmall );

  /* Quickly handle Session Messages. */
  if( NBT_SS_SESS_MSG == ssMsg->Type )
    {
    if( 0xFFFE0000 && ssMsg->Length )
      return( nbt_errInvalidSSLen );
    (void)nbtset32( bufr, ssMsg->Length );
    return( 4 );
    }

  tmpPre = ((uint32_t)(ssMsg->Type) << 24);   /* Initialize...  */

  /* Build the message, based on the message type. */
  switch( ssMsg->Type )
    {
    case NBT_SS_SESS_POS_RESP:
    case NBT_SS_SESS_KEEPALIVE:
      (void)nbtset32( bufr, tmpPre );
      return( 4 );
    case NBT_SS_SESS_REQ:
      (void)nbtset32( bufr, (tmpPre | 68ul) );
      /* If we were given a large enough buffer, copy the names.  */
      if( bSize >= 72 )
        {
        assert( L2Okay( ssMsg->Request.CalledName ) );
        assert( L2Okay( ssMsg->Request.CallingName ) );
        (void)memcpy(  &bufr[4], ssMsg->Request.CalledName, 34 );
        (void)memcpy( &bufr[38], ssMsg->Request.CallingName, 34 );
        return( 72 );
        }
      return( 4 );
    case NBT_SS_SESS_NEG_RESP:
      if( bSize < 5 )
        return( nbt_errBufrTooSmall );
      (void)nbtset32( bufr, (tmpPre | 1ul) );
      bufr[4] = ssMsg->ErrCode;
      return( 5 );
    case NBT_SS_SESS_RETARGET:
      if( bSize < 10 )
        return( nbt_errBufrTooSmall );
      (void)nbtset32( bufr, (tmpPre | 6ul) );
      (void)nbtset32( &bufr[4], ssMsg->Retarget.IPv4 );
      return( 10 );
    }

  return( nbt_errInvalidSSType );
  } /* nbt_ssPackMsg */


/* ================================ All Done ================================ */
