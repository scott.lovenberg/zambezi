#ifndef NBT_NBSS_H
#define NBT_NBSS_H
/* ========================================================================== **
 *                                 nbt_nbss.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description:
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_nbss.h; 2020-11-04 09:27:19 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *  - See the Doxyfile for an explanation of the `unnamed{union}` macro used
 *    in this file.
 *
 * ========================================================================== **
 *//**
 * @file    nbt_nbss.h
 * @author  Christopher R. Hertel
 * @brief   NetBIOS Session Service message handling.
 * @date    2 Oct 2020
 * @version \$Id: nbt_nbss.h; 2020-11-04 09:27:19 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  These are the basic tools needed for marshalling and unmarshalling NBT
 *  Session Service (nbss) messages.  The Session Service defines six
 *  message types:
 *  - Session Message \c (0x00)
 *  - Session Request \c (0x81)
 *  - Positive Session Response \c (0x82)
 *  - Negative Session Response \c (0x83)
 *  - Retarget Session Response \c (0x84)
 *  - Session Keepalive \c (0x85)
 *
 *  Of those, the Session Message is the most commonly used.  It is the
 *  message type that carries...messages!  In particular, messages that
 *  are to be delivered up the stack.  The format of the NBT Session Message
 *  is also carried over into Naket TCP Transport (aka. TCP Direct, on TCP
 *  port 445).
 *
 *  The Session Request is used to initiate an NBT transport session.  There
 *  are three possible responses: yes, no, and go talk to your mom.  The
 *  third response, the retarget, is intended to redirect the client to a
 *  different server and/or port number.  Most clients do not implement
 *  support for the Retarget Session Response and treat it instead as a
 *  Negative Session Response.  (At least one older client will retry the
 *  connection if it receives a Retarget Session Response.)
 *
 *  The Session Keepalive is meant to be discarded by the receiver.  It is
 *  used to force the underlying TCP layer to send a message, which should
 *  generate an ACK response from the peer node.  If the ACK isn't received,
 *  the TCP layer will notify the higher layers that the connection has been
 *  lost.  This is covered in [RFC1001; B-3].
 *
 *  <b>Terminology Notes</b>
 *
 *  [STD19] does not coin the acronym \b NBSS.  There is (technically) no
 *  such thing.  As is true of much of the terminology surrounding NetBIOS
 *  and NBT, the use of NBSS comes from a general lack of understanding of
 *  the details.  [STD19] does define two similar looking acronyms:  \b NBNS
 *  and \b NBDD.  These refer to \em servers that are defined as components
 *  of broader \em services.
 *
 *  These terms are defined in [RFC1001], and are also covered in detail in
 *  [IMPCIFS; 1].  Being pedantic, we will stick to using \b NBNS and
 *  \b NBDD as defined in the RFCs.  Bowing to convention, however, we will
 *  use the lower case acroynms \b nbns, \b nbds, and \b nbss to refer to
 *  the broader \em services that are defined in th the RFCs, like so:
 *
 *  - \b nbns:  [RFC1001; 15] NetBIOS Name Service
 *  - \b nbds:  [RFC1001; 17] NetBIOS Datagram Service
 *  - \b nbss:  [RFC1001; 16] NetBIOS Session Service
 *  - \b NBNS:  [RFC1001; 11.1] NetBIOS Name Server
 *  - \b NBDD:  [RFC1001; 11.2] NetBIOS Datagram Distribution Server
 *
 * @see <a href="http://ubiqx.org/cifs/NetBIOS.html">[IMPCIFS; 1] NBT:
 *  NetBIOS over TCP/IP</a>
 * @see <a href="http://www.ietf.org/rfc/rfc1001.txt">[RFC1001]: Protocol
 *  Standard for a NetBIOS Service on a TCP/UDP Transport: Concepts
 *  and Methods</a>
 * @see <a href="http://www.ietf.org/rfc/rfc1002.txt">[RFC1002]: Protocol
 *  Standard for a NetBIOS Service on a TCP/UDP Transport: Detailed
 *  Specifications</a>
 * @see
 *  <a href="https://www.rfc-editor.org/info/std19">[STD19]:
 *  [RFC1001] and [RFC1002] together comprise IETF STD #19</a>
 */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "nbt_common.h"


/* -------------------------------------------------------------------------- **
 * Defined Constants
 */

/**
 * @def   NBT_SS_CNAME_SIZE
 * @brief Fixed byte length of Called and Calling names.
 * @details
 *  The Called and Calling names in a Session Request packet are Level-2
 *  encoded NetBIOS names with no Scope appended.  (Scope is enforced by
 *  the Name Service.)  An L2-Encoded name with no Scope is a fixed 34
 *  bytes in length.
 *
 * @see <a href="http://www.ubiqx.org/cifs/NetBIOS.html#NBT.6.2">[IMPCIFS;
 *         1.6.2]: Creating an NBT Session</a>
 */
#define NBT_SS_CNAME_SIZE 34


/* -------------------------------------------------------------------------- **
 * Macros
 */

/**
 * @def     nbt_ssSessMsg( M )
 * @param   M   \c uint32_t; The first 32-bits of the incoming message.
 * @brief   Quick check to see if the message is a Session Message.
 * @returns \c true if the message is a Session Message, else \c false.
 * @details
 *  Session Messages are the most common and most important Session Service
 *  message type.  Typically, when processing NBT messages, the first four
 *  bytes are read (converting to host byte order) to determine how to
 *  process the rest of the message (if any).  This macro provides a quick
 *  check to see if the message is a Session Message, which will then be
 *  sent up the stack.  Other message types are processed by the NBT layer.
 *
 *  The input must be a 32-bit unsigned integer in host byte order.
 *
 * @hideinitializer
 */
#define nbt_ssSessMsg( M ) ((0 == ((M) & 0xFFFE0000)) ? true : false )


/* -------------------------------------------------------------------------- **
 * Enumerated Types
 */

/**
 * @enum  nbt_ssMsgType
 *  Session Service Message Type codes.
 *
 * @var NBT_SS_SESS_MSG
 *  The payload of the message, as indicated by the \c Length, is to be
 *  passed up the connection stack.  For example, if the NBT Session is
 *  transporting SMB, then the payload of this NBT Session Message is an
 *  SMB message to be passed up to the SMB layer.
 * @var NBT_SS_SESS_REQ
 *  A request for a new NBT Session with the Called node, initiated by the
 *  Calling node.
 * @var NBT_SS_SESS_POS_RESP
 *  The associated Session Request was successful.
 * @var NBT_SS_SESS_NEG_RESP
 *  The associated Session Request was unsuccessful; an error code is
 *  returned.
 * @var NBT_SS_SESS_RETARGET
 *  The Called node is directing the Calling Node to try another IPv4
 *  address and/or TCP port number.  The Session Request is denied.
 * @var NBT_SS_SESS_KEEPALIVE
 *  This message type triggers a TCP connection check.  The Keepalive
 *  message itself is simply discarded.
 */
typedef enum
  {
  NBT_SS_SESS_MSG       = 0x00,   /**< Session Message           */
  NBT_SS_SESS_REQ       = 0x81,   /**< Session Request           */
  NBT_SS_SESS_POS_RESP  = 0x82,   /**< Positive Session Response */
  NBT_SS_SESS_NEG_RESP  = 0x83,   /**< Negative Session Response */
  NBT_SS_SESS_RETARGET  = 0x84,   /**< Retarget Session Response */
  NBT_SS_SESS_KEEPALIVE = 0x85    /**< Session Keepalive         */
  } nbt_ssMsgType;

/**
 * @enum  nbt_ssError
 *  Error codes that may be returned in a Negative Session Response.
 *
 * @see <a href="http://www.ubiqx.org/cifs/NetBIOS.html#NBT.6.2">[IMPCIFS;
 *         1.6.2]: Creating an NBT Session</a>
 *
 * @var NBT_SS_ERR_NL_CALLED
 *  The remote node has registered the CALLED NAME, but no application or
 *  service is listening for Session Request messages on the given name.
 *
 * @var NBT_SS_ERR_NL_CALLING
 *  The remote node has registered the CALLED NAME and is listening for
 *  connections, but it doesn't want to talk to the Caller.  It is
 *  expecting a call from some other CALLING NAME.
 *
 * @var NBT_SS_ERR_CALLED_NOT_PRES
 *  The remote node has not registered the CALLED NAME.
 *
 * @var NBT_SS_ERR_INS_RESOURCES
 *  The remote node is busy and cannot take the call at this time.
 *
 * @var NBT_SS_ERR_UNSPECIFIED
 *  Something went wrong.
 */
typedef enum
  {
  NBT_SS_ERR_NL_CALLED       = 0x80,  /**< Not Listening on Called Name.    */
  NBT_SS_ERR_NL_CALLING      = 0x81,  /**< Not listening for Calling Name.  */
  NBT_SS_ERR_CALLED_NOT_PRES = 0x82,  /**< Called Name Not Present.         */
  NBT_SS_ERR_INS_RESOURCES   = 0x83,  /**< Insufficient Resources.          */
  NBT_SS_ERR_UNSPECIFIED     = 0x8F   /**< Unspecified Error.               */
  } nbt_ssError;


/* -------------------------------------------------------------------------- **
 * Static Inline Functions
 */

/* Tell Doxygen to produce documentation for static inline functions.  */
#ifdef DOXY_TEXT
bool nbt_ssCheckEcode( const uint8_t eCode );
uint32_t nbt_ssTrailBytes( const uint8_t tCode );
#endif /* DOXY_TEXT */

static inline bool nbt_ssCheckEcode( const uint8_t eCode )
  /** Test whether the given value is a valid Session Service error code.
   *
   * @param[in] eCode   A one-byte error code, as used in a Negative
   *                    Session Response message.
   *
   * @returns   \c true if \p eCode is a defined Session Service error code,
   *            else \c false.
   *
   * \b Notes
   *  - See \c #nbt_ssError for a list of valid error codes.
   *  - This function is defined in the header as a static inline function.
   *    It is quite small, and was written as a function instead of a macro
   *    just because the former looks cleaner.
   */
  {
  switch( eCode )
    {
    case NBT_SS_ERR_NL_CALLED:
    case NBT_SS_ERR_NL_CALLING:
    case NBT_SS_ERR_CALLED_NOT_PRES:
    case NBT_SS_ERR_INS_RESOURCES:
    case NBT_SS_ERR_UNSPECIFIED:
    return( true );
    }
  return( false );
  } /* nbt_ssCheckEcode */

static inline uint32_t nbt_ssTrailBytes( const uint8_t tCode )
  /** Return the number of Trailer bytes required for the given message type.
   *
   * @param[in] tCode   The \c Type code of the message.
   *
   * @returns   The length, in bytes, of the Trailer required for the given
   *            message type, as defined in RFC1002.  This will be zero for
   *            all message types except:
   *            - Session Request messages \c (68).
   *            - Negative Session Response messages \c (1).
   *            - Session Retarget messages \c (6).
   *
   * \b Notes
   *  - No test is done to validate \p tCode.  An invalid message type code
   *    will result in a return a value of 0.
   *  - See \c #nbt_ssMsgType for a list of valid type codes.
   *  - This function is defined in the header as a static inline function.
   */
  {
  uint32_t tmp32 = 0;

  switch( tCode )
    {
    case NBT_SS_SESS_REQ:
      tmp32 = 68;
      break;
    case NBT_SS_SESS_NEG_RESP:
      tmp32 = 1;
      break;
    case NBT_SS_SESS_RETARGET:
      tmp32 = 6;
      break;
    }
  return( tmp32 );
  } /* nbt_ssMinTrailBytes */


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  nbt_ssMsg
 * @brief   NetBIOS Name Service message structure.
 * @details
 *  Every Session Service message begins with a 32-bit unsigned integer
 *  value in network byte order.  This initial value is subdivided into
 *  three fields: \c Type, \c Flags, and \c Length.  Additional data may
 *  follow, depending upon the value of the \c Type subfield.
 *
 *  This structure does not record the \c Flags field.  RFC1002 was written
 *  in 1987 and new new \c Flags bits have been defined since then.  This
 *  module only checks for non-zero bits to determine whether or not the
 *  message is valid.  RFC1002 references an \c E (Extension) bit in the
 *  \c Flags field.  This module includes the \c E bit in the \c Length
 *  field as described in [IMPCIFS; 1.6.1].
 *
 * @see \c #nbt_ssParseMsg()
 * @see \c #nbt_ssParseTrailer()
 * @see \c #nbt_ssPackMsg()
 * @see <a href="http://www.ubiqx.org/cifs/NetBIOS.html#NBT.6.1">[IMPCIFS;
 *         1.6.1]: Session Service Header</a>
 *
 * @var nbt_ssMsg::Type
 *  The one-byte message type code.
 * @var nbt_ssMsg::Length
 *  The length of the *subsequent data* (aka. "Trailer").  In the case of a
 *  Session Message, this will be the size of the message that is passed up
 *  the stack.  Otherwise, see \c #nbt_ssTrailBytes().
 * @var nbt_ssMsg::Request.CalledName
 *  Level 2 Encoded NetBIOS name of the requested service.
 * @var nbt_ssMsg::Request.CallingName
 *  Level 2 Encoded NetBIOS name of the source of the request.
 * @var nbt_ssMsg::Retarget.IPv4
 *  The IPv4 Address to which the connection is being redirected.
 *  The IPv4 address is represented on the wire as a 32-bit integer value
 *  in network byte order.  It is stored in this field as a 32-bit integer
 *  in host byte order.
 * @var nbt_ssMsg::Retarget.Port
 *  The TCP port to which the connection is being redirected.
 *  This field is in host byte order.  Its value is converted to network
 *  byte order when sent on the wire.
 * @var nbt_ssMsg::ErrCode
 *  A one-byte error code indicating the reason that the connection failed.
 *  See \c #nbt_ssError.
 */
typedef struct
  {
  uint8_t  Type;
  uint32_t Length;
  union
    {
    uint8_t ErrCode;          /**< Negative Session Response Trailer. */
    struct
      {
      uint8_t *CalledName;    /**< Session Request Target name.       */
      uint8_t *CallingName;   /**< Session Request Source name.       */
      } Request;
    struct
      {
      uint32_t  IPv4;         /**< Session Retarget IPv4 address.     */
      uint16_t  Port;         /**< Session Retarget TCP port number.  */
      } Retarget;
    };
  } nbt_ssMsg;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int nbt_ssParseMsg( const uint8_t *const msg, nbt_ssMsg *const ssMsg );

int nbt_ssParseTrailer( uint8_t   *const msg,
                        size_t     const msgLen,
                        nbt_ssMsg *const ssMsg );

int nbt_ssPackMsg( const nbt_ssMsg *const ssMsg,
                   uint8_t         *const bufr,
                   size_t           const bSize );


/* =============================== nbt_nbss.h =============================== */
#endif /* NBT_NBSS_H */
